
## Déployment dans Yunohost

Le site est déployé automatiquement à chaque push sur une instance yunhost. Voir `gitlab-ci.yml` pour la syntaxe précise, mais l'idée est simplement de pousser tous les fichiers en SFTP (en utilisant les [variables de CI](https://gitlab.utc.fr/licolomb/odyssee-du-web/-/settings/ci_cd)). Le serveur yunohost peut alors servir les fichiers reçus.

Pour installer correctement l'application yunohost :

```sh
yunohost app install --label "Déprise" --args "domain=VOTREDOMAINE&path=/&with_sftp=yes&password=VOTREMOTDEPASSE&init_main_permission=visitors&phpversion=none&nodeversion=none&database=none" my_webapp
```

en remplaçant `VOTREDOMAINE` et `VOTREMOTDEPASSE`.
