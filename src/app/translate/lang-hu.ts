// lang-en.ts

export const LANG_HU_NAME = 'hu';

export const LANG_HU_TRANS = {
    'hello world': 'bonjour Monde',
    "Accueil":
        "Főoldal",
    "Présentation":
        "A műről",
    "Crédits":
        "Készítette",
    "Prix":
        "Díjak",
    "Scènes":
        "Jelenetek",
    "Scène":
        "jelenet",
    "Version Flash":
        "Flash verzió",
    "Version application smartphone":
        "Mobiltelefon alkalmazás",
    "Version Angular":
        "JavaScript Verzió",
    "Réalisation":
        "Alkotók",
    "Langues":
        "Nyelvek",
    "Traduction":
        "Fordította",
    "Voix":
        "Hang",
    "Épouse":
        "Feleség",
    "Musique":
        "Zene",
    "Vidéo":
        "Videó",
    "Téléphone":
        "Telefon",
    "Narrateur":
        "Narrátor",
    "ado":
        "Kamasz",
    "Ambiance":
        "Hangulat",
    "Manipulation":
        "Manipuláció",
    "Saxophone":
        "Szaxofon",
    "Guitare électrique":
        "Elektromos gitár",
    "Batterie/Montage":
        "Dob/Hangmérnök",
    "Extrait du disque":
        "Részlet,",

    "Déprise a gagné le prix New Media Writing Prize en 2011.":
        "A Fogásvesztés elnyerte a 2011-es New Media Writing Prize-t",
    "Voir le site":
        "A honlap megtekintése",

    "Déprise est également disponible sous forme d'application mobile.":
        "A Fogásvesztés mobiltelefon alkalmazás formájában is hozzáférhető",
    "Application sur Google Play":
        "Az alkalmazás a Google Playen",
    "Application sur l'Apple Store":
        "Az alkalmazás az Apple Store-ban",

    "Toute ma vie, j'ai cru avoir devant moi un champ des possibles infini.":
        "Egész életemben azt hittem, a lehetőségek végtelen tárháza áll előttem.",
    "déprise": "fogásvesztés",
    "Déprise": "Fogásvesztés",
    "Déprise est une création sur les notions de prise et perte de contrôle. Quand a-t-on l'impression d'être en situation de prise ou de perte de prise dans la vie? Six scènes racontent l'histoire d'un personnage en pleine déprise. Parallèlement, ce jeu sur prise et déprise permet de mettre en scène la situation du lecteur d'une oeuvre interactive.":
        "A Fogásvesztés a dolgok megragadhatóságáról és irányíthatóságáról szóló mű. Mikor érezzük úgy, hogy kezünkben tarjuk az életünket, illetve mikor tűnik úgy, hogy minden kisiklik az ujjaink közül? Hat jelenet meséli el a főhős történetét, aki elveszteni látszik az események fonalát a saját életében. Ez az ingadozás kontroll és fogásvesztés között egyúttal az olvasó helyzetét is bemutatja az interaktív művel szemben.",
    "Avez-vous pensé à allumer vos enceintes ?":
        "Bekapcsolta a hangszórót?",
    "Appuyez sur n'importe quelle touche": "Üssön le egy billentyűt!",
    "\"L'univers entier m'appartient\", pensais-je.": "„Az egész világegyetem az enyém\”, gondoltam.",
    "J'ai le choix.": "Van választásom.",
    "Je suis maître de mon destin.":
        "Kezemben a sorsom.",
    "Je peux prendre ce qui me plaît.":
        "Bármit megragadhatok, ami tetszik.",
    "Je deviendrai ce que je veux.":
        "Az leszek, ami akarok.",
    "J'ai tracé mon propre chemin.":
        "Én alakítom az utamat.",
    "J'ai parcouru de magnifiques paysages.":
        "Csodás tájakat jártam be.",
    "Quoi de plus naturel, je les avais choisis.":
        "Mi sem természetesebb, magam választottam őket.",
    "Mais depuis un moment, j'ai des doutes.":
        "De egy ideje kételyeim vannak.",
    "Comment avoir prise sur ce qui m'arrive ?":
        "Hogyan tartsam kézben mindazt, ami velem történik?",
    "Tout s'échappe.":
        "Minden tovacsúszik.",
    "Me glisse entre les doigts.":
        "Kisiklik az ujjaim közül.",
    "Les objets, les personnes.":
        "A tárgyak, az emberek.",
    "J'ai l'impression de ne plus rien contrôler.":
        "Úgy érzem, semmi felett sincs többé befolyásom.",
    "Depuis quelques temps maintenant,":
        "Már egy ideje,",
    "Je n'attends qu'une chose.":
        "Csak egy dologra várok?",
    "La suite.":
        "A folytatásra.",

    "Et le rendez-vous est arrivé.":
        "És a randevú ideje elérkezett.",

    "Mais le rendez-vous était biaisé.":
        "De a találkát megtrükközték.",
    "Je ne m'en suis rendu compte que beaucoup plus tard.":
        "Csak jóval később jöttem rá.",
    "La femme en face de moi, qui paraissait si parfaite, me laissait bouche bée.":
        "A nő, akivel szemben találtam magam, tökéletesnek tűnk, tátva maradt a szám.",
    "Impossible de prononcer quelque chose de cohérent.":
        "Egy értelmes szót sem tudtam kifacsarni magamból.",
    "Sa présence me bouleversait...":
        "Felkavart a jelenléte.",
    "Il fallait que je pose des questions pour la mettre à jour.":
        "Muszáj volt kérdeznem, hogy megtudjam, kicsoda.",
    "Sans que je m'en aperçoive, cette inconnue devenait ma femme.":
        "Anélkül, hogy észrevettem volna, ez az ismeretlen a feleségem lett.",
    "On a tout partagé.":
        "Mindent megosztottunk.",
    "Mais jamais je ne suis parvenu à vraiment la connaître.":
        "De sohasem sikerült valóban megismernem.",
    "Aujourd'hui encore je me pose des questions.":
        "Még ma is kételkedem.",
    "Qui d'elle ou moi suit l'autre ?":
        "Ki követi a másikat, én vagy ő?",
    "Quand je l'aime, elle me sème.":
        "Mikor szeretem, eltávolít.",

    "Qui êtes-vous ?":
        "Ki maga?",
    "Vous aimez...":
        "Mit szeret?",
    "Que pensez-vous...":
        "Mit gondol…",
    "D'où venez-vous ?":
        "Honnan jön?",
    "Où allez-vous ?":
        "Hová megy?",
    "Vous pensez quoi de...":
        "Mit gondol arról, hogy…",
    "Vous habitez la région depuis longtemps ?":
        "Régóta lakik a régióban?",
    "Vous évitez la légion depuis longtemps ?":
        "Rágót alakít a régi óra?",
    "Et vous travaillez dans quoi ?":
        "És mi a foglalkozása?",
    "Et vous travaillez l'envoi ?":
        "És mi a fogadkozása?",
    "Je vous trouve vraiment très jolie !":
        "Nagyon csinosnak találom!",
    "Chevaux, brousse, bêlement... près jolis":
        "Nagyon csíkos a hátam!",
    "J'ai l'impression qu'on a beaucoup de points communs":
        "Úgy érzem, sok közös pontunk van",
    "Chemins pression en Allemagne point comme un...":
        "Ugye vérzésem, sokat köpött a torma.",
    "Vous avez des yeux somptueux":
        "Csodálatos szemei vannak!",
    "Vous avouez des notions de tueurs":
        "Családos eszement!",
    "Vous venez souvent ici ?":
        "Gyakran jár ide?",
    "Vous avez l'absent acquis":
        "Gyatra nyáridő?",
    "Puis-je vous offrir un autre verre ?":
        "Meghívhatom még egy pohárra?",
    "Pigeon ouïr un Notre Père ?":
        "Meghízhat még így pofára?",
    "J'aime votre façon de sourire":
        "Tetszik a mosolya.",
    "Gêne, votre garçon mourir":
        "Fekszik a mostoha.",
    "Vous voulez marcher un peu ?":
        "Akar sétálni egy kicsit?",
    "Nouveaux-nés barges et il pleut.":
        "Se kar, se tál, egy csikk?",

    "Vingt ans se sont écoulés depuis notre rencontre.":
        "Húsz év telt el az első találkozásunk óta.",
    "Ce matin, je me perds dans un mot qu'elle m'a laissé.":
        "Ma reggel felkavar egy üzenet, amit nekem hagyott.",
    "Tout se brouille dans mon esprit.":
        "Minden összezavarodik a fejemben.",
    "Je ne sais comment l'interpréter.":
        "Nem tudom, hogy értelmezzem.",
    "Mot d'amour ou de rupture ?":
        "Szerelmes levél vagy szakító szó?",
    "Le fait-elle exprès ?":
        "Szándékosan csinálja?",


    "Je sais que c'est pour toi un choc":
        "Tudom, hogy ez számodra sokkoló",
    "Je n'ai que de l'amour pour toi":
        "„Csak szerelmet érzek irántad”",
    "Est un mensonge, et":
        "Színtiszta hazugság, és",
    "Dans un couple, il y en a un qui souffre et un qui s'ennuie":
        "„minden párkapcsolatban az egyik fél szenved, a másik unatkozik”",
    "Je veux que tous nos amis sachent que":
        "Azt akarom, hogy minden ismerősünk tudja, hogy",
    "Je ne veux pas rester avec toi,":
        "Nem akarok veled maradni,",
    "Depuis le premier jour, je ne sais pas comment tu peux croire que":
        "Az első pillanattól fogva, nem tudom, hogy gondolhatod, hogy",
    "Je t'aime":
        "Szeretlek",
    "Mon amour":
        "Szerelmem",
    "A disparu":
        "Szertefoszlott",
    "L'indifférence":
        "A közöny",
    "Est plus vivace que jamais":
        "Elevenebb, mint",
    "Le charme de notre rencontre":
        "A találkozásunk varázsa",
    "S'est dissipé à présent":
        "Mely eloszlott immár",
    "Et le moindre malentendu":
        "Legapróbb félreértésünk",
    "A vaincu":
        "Legyőzte",
    "Notre amour":
        "Szerelmünk",
    "Je n'ai pas de héros. D'aussi loin qu'il m'en souvienne, et même après une longue réflexion, je n'ai jamais eu de héros. La figure du héros ne me séduit pas. Sans doute parce que je ne préfère pas une qualité à une autre, une valeur morale à une autre. Les héros, je les connais, je les reconnais, mais je n'éprouve ni adoration, ni idolâtrie pour eux. À dire vrai, le fanatisme me rend fou. Si l'on considère que le héros obtient son titre par ses actions, son statut est donc une forme de récompense pour la prouesse, la hardiesse, l'originalité. Mais que reste-t-il à la personne, une fois l'acte terminé ? Rien, sinon le titre. On peut présumer que le héros retire de son action une aura : l'action brille à travers lui. J'ai tendance à croire, que l'oeuvre — l'action selon les domaines — doit quitter son créateur pour pouvoir vivre pleinement. Les enfants-livres des auteurs vont eux-mêmes tracer leur chemin, se cognant à l'occasion à quelques Zoïles.":
        "Nincs hősöm. Amennyire emlékszem, hosszas gondolkodás után is úgy tűnik, hogy sosem volt hősöm. Nem vonz a hős figurája. Valószínűleg azért, mert nem tartok többre egy jó tulajdonságot vagy emberi értéket a többinél. Ismerem és felismerem a hősöket, de nem érzek irántuk sem imádatot, sem csodálatot. Őszintén szólva megőrít a fanatizmus. Ha úgy tekintjük, hogy a hős a tetteivel érdemli ki a címét, a státusza a bátorságának, merészségének, eredetiségének jutalma. De mi marad mindebből a tett véghezvitele után? Semmi, magán a tituluson kívül. Feltételezhetjük, hogy a hős átveszi tetteinek auráját: az ő alakján át ragyog a tett. Én úgy gondolom, hogy a munkának – illetve a tettnek, a területtől függően – el kell szakadnia kivitelezőjétől ahhoz, hogy teljes életet élhessen. Az írók könyvutódai is a saját útjukat járják, itt-ott esetleg rosszindulatú kritikusokba ütközve.",
    "Je ne t'aime pas.":
        "Nem szeretlek.",
    "Tu ne me connais pas.":
        "Nem ismersz.",
    "Nous n'avons rien en commun.":
        "Nincs közös pontunk.",
    "Je ne veux rien de toi.":
        "Semmit sem akarok tőled.",
    "Tu n'es pas un modèle pour moi.":
        "Nem vagy a példaképem.",
    "Je veux voler de mes propres ailes.":
        "A saját szárnyaimmal akarok repülni.",
    "Bientot je partirai.":
        "Nemsoká elmegyek.",

    "Si ce n'était qu'elle, je pourrais l'accepter.":
        "Ha csak róla lenne szó, még el tudnám fogadni.",
    "Mais mon fils dispose des mêmes armes.":
        "De a fiam is hasonló eszközöket vet be.",
    "Il voudrait mon avis sur sa rédaction. ":
        "A véleményemet kéri egy fogalmazásáról. ",
    "Mais je ne parviens pas à me concentrer sur le texte.":
        "De nehezemre esik a szövegrre koncentrálni.",
    "Étrange impression de ne pouvoir lire qu'entre les lignes…":
        "Furcsa benyomás, mintha csak a sorok között tudnék olvasni…",


    "Suis-je si peu présent ?":
        "Ennyire nem lennék jelen?",
    "Si modelable ?":
        "Ennyire képlékeny lennék?",
    "Ma propre image semble me fuir.":
        "A saját képem is menekülni tűnik előlem.",
    "Elle m'échappe.":
        "Nem tudom megragadni.",
    "Je me sens manipulé.":
        "Úgy érzem, manipulálnak.",



    "Il est temps de reprendre le contrôle.":
        "Ideje kézbe venni a dolgokat.",
    "Arrêter de tourner en rond.":
        "Véget vetni a tehetetlenségnek.",

    "Retrouver une direction.":
        "Irányt találni.",
    "Plier le cours des événements.":
        "Megváltoztatni a dolgok menetét.",
    "Donner un sens à mes actions.":
        "Értelmet adni a tetteimnek.",


    "Je fais tout pour maîtriser de nouveau le cours de ma vie.":
        "Mindent megteszek azért, hogy én irányítsam az életem.",
    "Je choisis.":
        "Én választok.",
    "Mes émotions.":
        "Az érzéseim.",
    "Le sens à donner aux choses.":
        "A dolgok jelentését.",
    "Enfin, je me suis repr":
        "Végre ismét…",

    "fin":
        "Vége",

    // fichiers sonores
    // chp1

    "assets/sound/chapitre1/BienvenueAppuyez.mp3":
        "assets/sound/chapitre1/BienvenueAppuyez-hu.mp3",
    "assets/sound/chapitre1/BipEtBravo.mp3":
        "assets/sound/chapitre1/BipEtBravo-hu.mp3",
    "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous.mp3":
        "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous-hu.mp3",
    "assets/sound/chapitre1/BravoVotreRendezVousEstArrive.mp3":
        "assets/sound/chapitre1/BravoVotreRendezVousEstArrive-hu.mp3",

    // chp2
    "assets/sound/chapitre2/julien1.mp3":
        "assets/sound/chapitre2/HU_man_1.mp3",
    "assets/sound/chapitre2/julien2.mp3":
        "assets/sound/chapitre2/HU_man_2.mp3",
    "assets/sound/chapitre2/julien3.mp3":
        "assets/sound/chapitre2/HU_man_3.mp3",
    "assets/sound/chapitre2/julien4.mp3":
        "assets/sound/chapitre2/HU_man_4.mp3",
    "assets/sound/chapitre2/julien5.mp3":
        "assets/sound/chapitre2/HU_man_5.mp3",
    "assets/sound/chapitre2/julien6.mp3":
        "assets/sound/chapitre2/HU_man_6.mp3",
    "assets/sound/chapitre2/julien7.mp3":
        "assets/sound/chapitre2/HU_man_7.mp3",
    "assets/sound/chapitre2/julien8.mp3":
        "assets/sound/chapitre2/HU_man_8.mp3",
    "assets/sound/chapitre2/julien9.mp3":
        "assets/sound/chapitre2/HU_man_9.mp3",

    // chp4


    "assets/sound/chapitre4/maximilienEssay.mp3":
        "assets/sound/chapitre4/maximilienEssay-hu.mp3",
    "assets/sound/chapitre4/max-jenetaimepas.mp3":
        "assets/sound/chapitre4/jenetaimepas-hu.mp3",
    "assets/sound/chapitre4/max-tunemeconnaispas.mp3":
        "assets/sound/chapitre4/tunemeconnaispas-hu.mp3",
    "assets/sound/chapitre4/max-nousnavonsrienencommun.mp3":
        "assets/sound/chapitre4/nousnavonsrienencommun-hu.mp3",
    "assets/sound/chapitre4/max-jeneveuxriendetoi.mp3":
        "assets/sound/chapitre4/jeneveuxriendetoi-hu.mp3",
    "assets/sound/chapitre4/max-tunespasunmodelepourmoi.mp3":
        "assets/sound/chapitre4/tunespasunmodelepourmoi-hu.mp3",
    "assets/sound/chapitre4/max-jeveuxvolerdemespropresailes.mp3":
        "assets/sound/chapitre4/jeveuxvolerdemespropresailes-hu.mp3",
    "assets/sound/chapitre4/max-bientotjepartirai.mp3":
        "assets/sound/chapitre4/bientotjepartirai-hu.mp3",

};
