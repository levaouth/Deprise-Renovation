// lang-zh.ts

export const LANG_ZH_NAME = 'zh';

export const LANG_ZH_TRANS = {
    "Accueil":
        "首页",
    "Présentation":
        "简介",
    "Crédits":
        "制作人员",
    "Prix":
        "奖项",
    "Scènes":
        "章节",
    "Scène":
        "章节",
    "Version Flash":
        "Flash版本",
    "Version application smartphone":
        "手机App",
    "Version Angular":
        "JavaScript版本",
    "Langues":
        "语言",
    "Réalisation":
        "指导",
    //制作
    "Traduction":
        "翻译",
    "Voix":
        "配音",
    "Épouse":
        "妻子",
    "Musique":
        "音乐",
    "Vidéo":
        "视频",
    "Téléphone":
        "电话语音",
    "Narrateur":
        "旁白",
    "ado":
        "少年",
    "Ambiance":
        "氛围",
    "Manipulation":
        "操纵",
    "Saxophone":
        "萨克斯",
    "Guitare électrique":
        "电吉他",
    "Batterie/Montage":
        "鼓/装配",
    "Extrait du disque":
        "音乐节选",
    "Déprise est également disponible sous forme d'application mobile.":
        "本作品也有手机应用版本",
    "Application sur Google Play":
        "Google Play",
    "Application sur l'Apple Store":
        "App Store",

    "Déprise a gagné le prix New Media Writing Prize en 2011.":
        "本作品获得了2011年的新媒体创作大奖",
    "Voir le site":
        "查看网站",

    'hello world': '你好，世界',
    "Toute ma vie, j'ai cru avoir devant moi un champ des possibles infini.":
        "从小到大，我一直以为我面前陈列着无数的可能性",
    "déprise":
        "失控",
    "Déprise":
        "失控",
    "Déprise est une création sur les notions de prise et perte de contrôle. Quand a-t-on l'impression d'être en situation de prise ou de perte de prise dans la vie? Six scènes racontent l'histoire d'un personnage en pleine déprise. Parallèlement, ce jeu sur prise et déprise permet de mettre en scène la situation du lecteur d'une oeuvre interactive.":
        "《失控》是一件关于掌控与失控的概念性电子文学作品，讨论在何时我们会感到对人生失去掌控。本作呈现六个不同场景中人逐渐失去控制的故事，同时通过媒体交互，反映读者的实时体验。",
    "Avez-vous pensé à allumer vos enceintes ?":
        "您开启电脑的声音了吗？",
    "(Appuyez sur n'importe quelle touche pour continuer)":
        "(按任意键继续)",
    "\"L'univers entier m'appartient\", pensais-je.":
        "\"整个宇宙都是属于我的\"，我想。",
    "J'ai le choix.": "我能作出选择。",
    // 我可以选择
    "Je suis maître de mon destin.":
        "我能掌控命运。",
    "Je peux prendre ce qui me plaît.":
        "我是世界之主。",
    "Je deviendrai ce que je veux.":
        "我会心想事成。",
    "J'ai tracé mon propre chemin.":
        "我沿着自己的路，",
    "J'ai parcouru de magnifiques paysages.":
        "饱览了壮美的风光。",
    "Quoi de plus naturel, je les avais choisis.":
        "这理所应当，因为，这都是我的选择",
    "Mais depuis un moment, j'ai des doutes.":
        "但不知从什么时候起，我有了一丝怀疑",
    "Comment avoir prise sur ce qui m'arrive ?":
        "我是如何，控制所发生之事的？",
    "Tout s'échappe.":
        "一切都离我而去。",
    "Me glisse entre les doigts.":
        "从我指缝中划过",
    "Les objets, les personnes.":
        "事，物，人，",
    "J'ai l'impression de ne plus rien contrôler.":
        "我感觉自己失控了。",
    "Depuis quelques temps maintenant,":
        "最近一段时间以来，",
    "Je n'attends qu'une chose.":
        "只有一件事，是我所期待的：",
    "La suite.":
        "接下来，会发生什么。",

    "Et le rendez-vous est arrivé.":
        "见面时间到了",

    "Mais le rendez-vous était biaisé.":
        "可是，约会与我设想的完全不一样，",
    "Je ne m'en suis rendu compte que beaucoup plus tard.":
        "我很久之后才意识到。",
    "La femme en face de moi, qui paraissait si parfaite, me laissait bouche bée.":
        "我面前的女人完美得让我难以自持。",
    "Impossible de prononcer quelque chose de cohérent.":
        "我完全吐不出正常的字句。",
    "Sa présence me bouleversait...":
        "她让我情迷意乱了。",
    "Il fallait que je pose des questions pour la mettre à jour.":
        "关于她，我有太多的疑惑。",
    "Sans que je m'en aperçoive, cette inconnue devenait ma femme.":
        "不知不觉，这个陌生人成为了我的妻子。",
    "On a tout partagé.":
        "我们分享一切。",
    "Mais jamais je ne suis parvenu à vraiment la connaître.":
        "但我从来没能真正了解他。",
    "Aujourd'hui encore je me pose des questions.":
        "直到今天，我仍然在疑惑着，",
    "Qui d'elle ou moi suit l'autre ?":
        "是谁在追随着谁？",
    "Quand je l'aime, elle me sème.":
        "若我爱她，她便失去了自我",

    "Qui êtes-vous ?":
        "你是谁？",
    "Vous aimez...":
        "你想要...",
    "Que pensez-vous...":
        "你怎么看...",
    "D'où venez-vous ?":
        "你从哪里来?",
    "Où allez-vous ?":
        "你要去哪里？",
    "Vous pensez quoi de...":
        "你是否觉得...",


    "Vous habitez la région depuis longtemps ?":
        "你在附近住了很久了吗",
    "Vous évitez la légion depuis longtemps ?":
        "你在福建住了很久吗？",
        // 你宰福建很久了吗？
    "Et vous travaillez dans quoi ?":
        "那你做什么工作呢？",
    "Et vous travaillez l'envoi ?":
        "那你坐什么空座呢？",
    "Je vous trouve vraiment très jolie !":
        "你真的很漂亮！",
    "Chevaux, brousse, bêlement... près jolis":
        "你装得很漂亮！",
    "J'ai l'impression qu'on a beaucoup de points communs":
        "我觉得我们有很多共同点",
    "Chemins pression en Allemagne point comme un...":
        "我觉得我没有很多共通点",
    "Vous avez des yeux somptueux":
        "你的眼睛真美",
    "Vous avouez des notions de tueurs":
        "你的盐晶蒸没了",
    "Vous venez souvent ici ?":
        "你经常来这儿吗？",
    "Vous avez l'absent acquis":
        "你警察来这儿吗？",
    "Puis-je vous offrir un autre verre ?":
        "我能再请你喝点什么吗？",
    "Pigeon ouïr un Notre Père ?":
        "我们在清理河道是吗？",
    "J'aime votre façon de sourire":
        "我喜欢你笑的样子",
    "Gêne, votre garçon mourir":
        "我喜欢你下的秧子",
    "Vous voulez marcher un peu ?":
        "我们出去散个步吧？",
    "Nouveaux-nés barges et il pleut.":
        "我们除去伞把儿吧？",
    "Vingt ans se sont écoulés depuis notre rencontre.":
        "自从我们见面，已经二十年过去了。",
    "Ce matin, je me perds dans un mot qu'elle m'a laissé.":
        "今天早上，我拿着她留给我的信，不知所措。",
    "Tout se brouille dans mon esprit.":
        "我的大脑一片浆糊，",
    "Je ne sais comment l'interpréter.":
        "完全不知道该如何解读。",
    "Mot d'amour ou de rupture ?":
        "这是首情诗，还是分手信？",
    "Le fait-elle exprès ?":
        "她是故意的吗？",


    "Je sais que c'est pour toi un choc":
        "我知道这对你来说很难接受",
    "Je n'ai que de l'amour pour toi":
        "我对你只有爱，",
    "Est un mensonge, et":
        "只是个谎言。",
    "Dans un couple, il y en a un qui souffre et un qui s'ennuie":
        "\"两个人之间，总是一个委屈，一个倦怠。\"",
    "Je veux que tous nos amis sachent que":
        "我想让我们所有的朋友都知道，",
    "Je ne veux pas rester avec toi,":
        "我不想和你在一起，",
    "Depuis le premier jour, je ne sais pas comment tu peux croire que":
        "从第一天起，我就一直在思考，怎样才能让你明白，",
    // 从第一天起，我就一直在思考，你是怎么相信
    "Je t'aime":
        "我爱你",
    "Mon amour":
        "我的爱",
    "A disparu":
        "消失了",
    "L'indifférence":
        "冷漠",
    "Est plus vivace que jamais":
        "从未如此真切",
    "Le charme de notre rencontre":
        "初次见面的魔力",
    "S'est dissipé à présent":
        "也消散了",
    "Et le moindre malentendu":
        "最细小的误会",
    "A vaincu":
        "打败了",
    "Notre amour":
        "我们的爱",
    "Je n'ai pas de héros. D'aussi loin qu'il m'en souvienne, et même après une longue réflexion, je n'ai jamais eu de héros. La figure du héros ne me séduit pas. Sans doute parce que je ne préfère pas une qualité à une autre, une valeur morale à une autre. Les héros, je les connais, je les reconnais, mais je n'éprouve ni adoration, ni idolâtrie pour eux. À dire vrai, le fanatisme me rend fou. Si l'on considère que le héros obtient son titre par ses actions, son statut est donc une forme de récompense pour la prouesse, la hardiesse, l'originalité. Mais que reste-t-il à la personne, une fois l'acte terminé ? Rien, sinon le titre. On peut présumer que le héros retire de son action une aura : l'action brille à travers lui. J'ai tendance à croire, que l'oeuvre — l'action selon les domaines — doit quitter son créateur pour pouvoir vivre pleinement. Les enfants-livres des auteurs vont eux-mêmes tracer leur chemin, se cognant à l'occasion à quelques Zoïles.":
        "我心里没有英雄。自打记事起，就算绞尽脑汁，我也想不出心中有过。我对英雄式的人物并不感冒。没错，因为我并不认为某种特定精神品质的道德价值高于另一种。我知道有英雄，我能认出他们，但我对他们从没有爱戴，或是崇拜。说实话，我讨厌那些拥趸。如果人们认为，英雄之所以为英雄，是因为他们的所作所为，英雄之称只是对他们言行勇义和特殊品质的褒奖，那么一旦战役终了，英雄脱下战袍，离开战场，他还剩下什么呢？什么都不剩，除了一个头衔。可以设想，英雄继续生活在他们英勇事迹的余晖之中。我倒是觉得，文艺作品得摆脱它的创作者，去发挥自己真正的生命力。文艺作品作为作者的后嗣，得去走出自己的路，找到自己的听众，亲自叩开毒舌评论家的门。",
    "Je ne t'aime pas.":
        "我不爱你。",
    "Tu ne me connais pas.":
        "你不了解我。",
    "Nous n'avons rien en commun.":
        "我们完全没有共同点。",
    "Je ne veux rien de toi.":
        "我不要你的东西。",
    "Tu n'es pas un modèle pour moi.":
        "你不是我的榜样。",
    "Je veux voler de mes propres ailes.":
        "我想靠自己去闯。",
    "Bientot je partirai.":
        "我很快就会离开。",

    "Si ce n'était qu'elle, je pourrais l'accepter.":
        "如果只是她这样，我还能承受。",
    "Mais mon fils dispose des mêmes armes.":
        "但我儿子也做了一样的事。",
    "Il voudrait mon avis sur sa rédaction. ":
        "他想让我评价一下他的文章，",
    "Mais je ne parviens pas à me concentrer sur le texte.":
        "但我完全无法将注意力集中在文字上。",
    "Étrange impression de ne pouvoir lire qu'entre les lignes…":
        "我所能看到的只有字里行间透露出的话语…",


    "Suis-je si peu présent ?":
        "我的存在如此微弱吗？",
    "Si modelable ?":
        "那么不成样子？",
    "Ma propre image semble me fuir.":
        "我自己的存在似乎都要离我而去。",
    "Elle m'échappe.":
        "她挣脱了我。",
    "Je me sens manipulé.":
        "我觉得自己被操纵着。",



    "Il est temps de reprendre le contrôle.":
        "是时候夺回掌控权了。",
    "Arrêter de tourner en rond.":
        "不能再继续原地打转。",

    "Retrouver une direction.":
        "重新找回方向。",
    "Plier le cours des événements.":
        "让事情的发展遂我所愿。",
    "Donner un sens à mes actions.":
        "让我的一举一动再次拥有意义。",


    "Je fais tout pour maîtriser de nouveau le cours de ma vie.":
        "我正在尽我所能地试着重新掌控我的生活。",
    "Je choisis.":
        "作出选择，",
    "Mes émotions.":
        "控制自己的情绪。",
    "Le sens à donner aux choses.":
        "控制对事物的看法。",
    "Enfin, je me suis repr":
        "最终，我重获了控..",

    "fin":
        "完",

    // fichiers sonores
    // chp1

    "assets/sound/chapitre1/BienvenueAppuyez.mp3":
        "assets/sound/chapitre1/BienvenueAppuyez-zh.mp3",
    "assets/sound/chapitre1/BipEtBravo.mp3":
        "assets/sound/chapitre1/BipEtBravo-zh.mp3",
    "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous.mp3":
        "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous-zh.mp3",
    "assets/sound/chapitre1/BravoVotreRendezVousEstArrive.mp3":
        "assets/sound/chapitre1/BravoVotreRendezVousEstArrive-zh.mp3",

    // chp2
    "assets/sound/chapitre2/julien1.mp3":
        "assets/sound/chapitre2/ZH_man_1.mp3",
    "assets/sound/chapitre2/julien2.mp3":
        "assets/sound/chapitre2/ZH_man_2.mp3",
    "assets/sound/chapitre2/julien3.mp3":
        "assets/sound/chapitre2/ZH_man_3.mp3",
    "assets/sound/chapitre2/julien4.mp3":
        "assets/sound/chapitre2/ZH_man_4.mp3",
    "assets/sound/chapitre2/julien5.mp3":
        "assets/sound/chapitre2/ZH_man_5.mp3",
    "assets/sound/chapitre2/julien6.mp3":
        "assets/sound/chapitre2/ZH_man_6.mp3",
    "assets/sound/chapitre2/julien7.mp3":
        "assets/sound/chapitre2/ZH_man_7.mp3",
    "assets/sound/chapitre2/julien8.mp3":
        "assets/sound/chapitre2/ZH_man_8.mp3",
    "assets/sound/chapitre2/julien9.mp3":
        "assets/sound/chapitre2/ZH_man_9.mp3",

    // chp4


    "assets/sound/chapitre4/maximilienEssay.mp3":
        "assets/sound/chapitre4/maximilienEssay-zh.mp3",
    "assets/sound/chapitre4/max-jenetaimepas.mp3":
        "assets/sound/chapitre4/jenetaimepas-zh.mp3",
    "assets/sound/chapitre4/max-tunemeconnaispas.mp3":
        "assets/sound/chapitre4/tunemeconnaispas-zh.mp3",
    "assets/sound/chapitre4/max-nousnavonsrienencommun.mp3":
        "assets/sound/chapitre4/nousnavonsrienencommun-zh.mp3",
    "assets/sound/chapitre4/max-jeneveuxriendetoi.mp3":
        "assets/sound/chapitre4/jeneveuxriendetoi-zh.mp3",
    "assets/sound/chapitre4/max-tunespasunmodelepourmoi.mp3":
        "assets/sound/chapitre4/tunespasunmodelepourmoi-zh.mp3",
    "assets/sound/chapitre4/max-jeveuxvolerdemespropresailes.mp3":
        "assets/sound/chapitre4/jeveuxvolerdemespropresailes-zh.mp3",
    "assets/sound/chapitre4/max-bientotjepartirai.mp3":
        "assets/sound/chapitre4/bientotjepartirai-zh.mp3"
};