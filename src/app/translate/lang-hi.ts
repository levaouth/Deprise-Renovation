// lang-en.ts

export const LANG_HI_NAME = 'hi';

export const LANG_HI_TRANS = {

  "Accueil":
  "घर",
  "Présentation":
  "प्रस्तुति",
  "Crédits":
  "श्रेय",
  "Prix":
  "पुरस्कार",
  "Scènes":
  "दृश्य",
  "Scène":
  "दृश्य",
  "Version Flash":
  "फ्लैश संस्करण",
  "Version application smartphone":
  "स्मार्टफोन्स के लिए ऐप",
  "Version":
  "संस्करण",
  "Langues":
  "भाषाएं",
  "Réalisation":
  "दिशा",
  "Traduction":
  "अनुवाद",
  "Voix":
  "आवाज़",
  "Épouse":
  "पत्नी",
  "Musique":
  "संगीत",
  "Vidéo":
  "वीडियो",
  "Téléphone":
  "फोन",
  "Narrateur":
  "कथावाचक",
  "ado":
  "किशोर",
  "Ambiance":
  "वातावरण",
  "Manipulation":
  "हेरफेर",
  "Saxophone":
  "सैक्सोफोन",
  "Guitare électrique":
  "इलेक्ट्रिक गिटार",
  "Batterie/Montage":
  "ड्रम/मॉन्टाज",
  "Extrait du disque":
  "डिस्क का अंश",

  "Déprise est également disponible sous forme d'application mobile." :
  "अस्तित्व की ढलान रचना मोबाइल उपकरणों के लिए एक ऐप के रूप में भी उपलब्ध है।",
  "Application sur Google Play":
  "गूगल प्ले",
  "Application sur l'Apple Store":
  "ऐप स्टोर",
"Déprise est une création sur les notions de prise et perte de contrôle. Quand a-t-on l'impression d'être en situation de prise ou de perte de prise dans la vie? Six scènes racontent l'histoire d'un personnage en pleine déprise. Parallèlement, ce jeu sur prise et déprise permet de mettre en scène la situation du lecteur d'une oeuvre interactive.":
        "नियंत्रण की हानि एक डिजिटल रचना है जो (नियंत्रण की) हानि की अवधारणा पर आधारित है। किन परिस्थितियों में हमें लगता है कि हमारे जीवन पर हमारा नियंत्रण है (या नहीं है)? छह दृश्य एक पात्र को पूर्ण अस्तित्व संकट में प्रस्तुत करते हैं। साथ ही, नियंत्रण पाने और खोने के इस खेल में एक इंटरैक्टिव डिजिटल कार्य के साथ पाठक के अनुभव को प्रतिबिंबित किया गया है।",
  "Déprise a gagné le prix New Media Writing Prize en 2011.":
  "अस्तित्व की ढलान रचना ने न्यू मीडिया राइटिंग पुरस्कार 2011 में जीता।",
  "Voir le site":
  "वेबसाइट देखें।",

    'hello world': 'नमस्ते दुनिया।',
    "Toute ma vie, j'ai cru avoir devant moi un champ des possibles infini.": "अपने पूरे जीवन में मैंने हमेशा यह विश्वास किया कि मेरे सामने असीमित संभावनाएं हैं।",
	"Déprise":"अस्तित्व की ढलान",
	"Déprise est une création sur les notions de prise et de contrôle. Quand a-t-on l'impression d'être en situation de prise ou de perte de prise dans la vie ? Six scènes racontent l'histoire d'un personnage en pleine déprise. Parallèlement, ce jeu sur prise et déprise permet de mettre en scène la situation du lecteur d'une oeuvre interactive.": "अस्तित्व की ढलान एक डिजिटल रचना है, जो नियंत्रण और पकड़ के विचारों को उजागर करती है। किन परिस्थितियों में हमें अपने जीवन पर नियंत्रण या उसकी कमी महसूस होती है? इस रचना में छह दृश्य हैं, जिनमें एक पात्र अपनी पकड़ खोता जा रहा है। साथ ही, पकड़ और पकड़ खोने का यह खेल पाठक के एक इंटरैक्टिव डिजिटल कार्य के अनुभव को प्रतिबिंबित करता है।",
	"Avez-vous pensé à allumer vos enceintes ?":"क्या आपके कंप्यूटर की ध्वनि चालू है?",
  "(Appuyez sur n'importe quelle touche pour continuer)":"(जारी रखने के लिए कोई भी कुंजी दबाएँ)",
		"\"L'univers entier m'appartient\", pensais-je.":"\"पूरा ब्रह्मांड मेरा है।\", मैंने सोचा.",
		"J'ai le choix.":"मेरे पास विकल्प है।",

		"Je suis maître de mon destin.":
		"मैं अपने भाग्य को नियंत्रित करता हूँ।",
		"Je peux prendre ce qui me plaît.":
		"मैं दुनिया का राजा हूँ।",
		"Je deviendrai ce que je veux.":
		"मैं वही बनूंगा जो मैं बनना चाहता हूँ।",
		"J'ai tracé mon propre chemin.":
		"मैंने अपना रास्ता खुद चुना।",
		"J'ai parcouru de magnifiques paysages.":
		"मैंने सुंदर द्रश्य का आनंद लिया।",
		"Quoi de plus naturel, je les avais choisis.":
		"कोई आश्चर्य नहीं, क्योंकि उन्हें मैंने ही चुना था।",
		"Mais depuis un moment, j'ai des doutes.":
		"लेकिन कुछ समय से, मुझे संदेह हो रहा है।",
		"Comment avoir prise sur ce qui m'arrive ?":
		"मैं उस पर कैसे नियंत्रण रख सकता हूँ जो मेरे साथ होता है?",
		"Tout s'échappe.":
		"सब कुछ मुझसे दूर होता जा रहा है।",
		"Me glisse entre les doigts.":
		"मेरे हाथों से फिसल जाता है।",
		"Les objets, les personnes.":
		"वस्तुएं, लोग।",
		"J'ai l'impression de ne plus rien contrôler.":
		"मुझे लगता है कि मैंने नियंत्रण खो दिया है।",
		"Depuis quelques temps maintenant,":
		"कुछ समय से,",
		"Je n'attends qu'une chose.":
		"मुझे अब केवल एक ही चीज़ की आशा है।",
		"La suite.":
		"आगे क्या आता है।",

	  "Et le rendez-vous est arrivé.":
    "मिलाप का समय आ गया है।",

		"Mais le rendez-vous était biaisé.":
		"लेकिन मिलाप रद्द हो गया।",
		"Je ne m'en suis rendu compte que beaucoup plus tard.":
		"मुझे यह बात बाद में ही समझ में आई।",
		"La femme en face de moi, qui paraissait si parfaite, me laissait bouche bée.":
		"मेरे सामने खड़ी महिला इतनी परिपूर्ण लगी कि मैं हैरान रह गया।",
		"Impossible de prononcer quelque chose de cohérent.":
		"मैं कुछ भी स्पष्ट रूप से नहीं कह पाया।",
		"Sa présence me bouleversait...":
		"मैं परेशान था।",
		"Il fallait que je pose des questions pour la mettre à jour.":
		"मुझे उसे समझने के लिए सवाल पूछने पड़े।",
		"Sans que je m'en aperçoive, cette inconnue devenait ma femme.":
		"मेरे जाने बिना ही, यह अजनबी मेरी पत्नी बन गई।",
		"On a tout partagé.":
		"हमने सब कुछ साझा किया।",
		"Mais jamais je ne suis parvenu à vraiment la connaître.":
		"लेकिन मैं उसे कभी वास्तव में जान नहीं पाया।",
		"Aujourd'hui encore je me pose des questions.":
		"आज भी, मैं सोचता हूँ।",
		"Qui d'elle ou moi suit l'autre ?":
		"कौन किसका पीछा कर रहा है?",
		"Quand je l'aime, elle me sème.":
		"जब मैं उससे प्यार करता हूँ, तो वह मुझे खो देती है।",

		"Qui êtes-vous ?":
		"तुम कौन हो?",
		"Vous aimez...":
		"क्या तुम्हें पसंद है...",
		"Que pensez-vous...":
		"इस बारे में तुम्हारा क्या सोचना है...",
		"D'où venez-vous ?":
		"तुम कहाँ से हो?",
		"Où allez-vous ?":
		"तुम कहाँ जा रहे हो?",
		"Vous pensez quoi de...":
		"क्या तुम्हें लगता है...",


		"Vous habitez la région depuis longtemps ?":
    "क्या तुम यहाँ बहुत समय से रह रही हो?",
    "Vous évitez la légion depuis longtemps ?":
    "क्या तुम बहुत सहम सी रही हो?",
		"Et vous travaillez dans quoi ?":
		"तुम क्या काम करती हो?",
    "Et vous travaillez l'envoi ?":
    "तुम क्या आराम करती हो?",
		"Je vous trouve vraiment très jolie !":
		"तुम काफी सुंदर हो!",
    "Chevaux, brousse, bêlement... près jolis":
    "तुम काफी अंदर हो।",
		"J'ai l'impression qu'on a beaucoup de points communs":
		"मुझे लगता है कि हमारे बीच बहुत सारी समानताएँ हैं।",
    "Chemins pression en Allemagne point comme un...":
    "मुझे लगता है हमारे पीछे बहुत सारे सामान आए हैं।",
		"Vous avez des yeux somptueux":
    "तुम्हारी भव्य आँखें हैं।",
    "Vous avouez des notions de tueurs":
    "तुम्हारी सभ्य आहें हैं।",
		"Vous venez souvent ici ?":
		"क्या तुम यहाँ प्रतिदिन आती हो?",
    "Vous avez l'absent acquis":
    "क्या तुम यहाँ प्रथा के दिन गाती हो?",
		"Puis-je vous offrir un autre verre ?":
		"क्या तुम और पीना चाहोगी?",
    "Pigeon ouïr un Notre Père ?":
    "क्या तुम और जीना चाहोगी?",
		"J'aime votre façon de sourire":
		"मुझे तुम्हारी मुस्कुराहट पसंद है।",
    "Gêne, votre garçon mourir":
    "मुझे तुमसे हरारत पसंद है।",
		"Vous voulez marcher un peu ?":
		"क्या टहलने चलोगी मेरे साथ?",
    "Nouveaux-nés barges et il pleut.":
    "क्या ताल में लगोगी मेरे साथ?",

		"Vingt ans se sont écoulés depuis notre rencontre.":
		"हमारे मिलाप को बीस साल हो गए हैं।",
		"Ce matin, je me perds dans un mot qu'elle m'a laissé.":
		"आज सुबह, मैं उसका छोड़ा हुआ एक नोट पढ़ रहा हूँ।",
		"Tout se brouille dans mon esprit.":
		"मैं उलझन में हूँ।",
		"Je ne sais comment l'interpréter.":
		"मैं समझ नहीं पा रहा हूँ कि इसका अर्थ क्या है।",
		"Mot d'amour ou de rupture ?":
		"यह एक प्रेम कविता है या फिर अलगाव का संदेश?",
		"Le fait-elle exprès ?":
		"क्या वह ऐसा जानबूझकर कर रही है?",


		"Je sais que c'est pour toi un choc":
		"मुझे पता है, यह तुम्हारे लिए एक झटका है",
		"Je n'ai que de l'amour pour toi":
		"जो भी मैं तुम्हारे लिए अनुभव करता हूँ, वह प्यार है,",
		"Est un mensonge, et":
		"यह एक झूठ है, और",
		"Dans un couple, il y en a un qui souffre et un qui s'ennuie":
		"\"एक जोड़े में हमेशा एक होता है जो पीड़ित होता है और दूसरा जो ऊब जाता है\"",
		"Je veux que tous nos amis sachent que":
		"मैं चाहता हूँ कि हमारे सभी दोस्त यह जानें कि",
		"Je ne veux pas rester avec toi,":
		"मैं तुम्हारे साथ नहीं रहना चाहता,",
		"Depuis le premier jour, je ne sais pas comment tu peux croire que":
		"पहले दिन से ही, मैं यह सोच रहा था कि तुम कैसे मान सकती हो कि",
		"Je t'aime":
		"मैं तुमसे प्यार करता हूँ",
		"Mon amour":
		"मेरा प्यार",
		"A disparu":
		"गायब हो गया है",
		"L'indifférence":
		"उदासीनता।",
		"Est plus vivace que jamais":
		"पहले से भी अधिक स्पष्ट है",
		"Le charme de notre rencontre":
		"हमारे मिलन का आकर्षण",
		"S'est dissipé à présent":
		"खत्म हो गया है",
		"Et le moindre malentendu":
		"और जरा सी भी गलतफहमी",
		"A vaincu":
		"ने हमारे प्यार को",
		"Notre amour":
		"समाप्त कर दिया है।",
	  "Je n'ai pas de héros. D'aussi loin qu'il m'en souvienne, et même après une longue réflexion, je n'ai jamais eu de héros. La figure du héros ne me séduit pas. Sans doute parce que je ne préfère pas une qualité à une autre, une valeur morale à une autre. Les héros, je les connais, je les reconnais, mais je n'éprouve ni adoration, ni idolâtrie pour eux. À dire vrai, le fanatisme me rend fou. Si l'on considère que le héros obtient son titre par ses actions, son statut est donc une forme de récompense pour la prouesse, la hardiesse, l'originalité. Mais que reste-t-il à la personne, une fois l'acte terminé ? Rien, sinon le titre. On peut présumer que le héros retire de son action une aura : l'action brille à travers lui. J'ai tendance à croire, que l'oeuvre — l'action selon les domaines — doit quitter son créateur pour pouvoir vivre pleinement. Les enfants-livres des auteurs vont eux-mêmes tracer leur chemin, se cognant à l'occasion à quelques Zoïles.":
    "मेरे पास कोई हीरो नहीं है। जब से मुझे याद है और गहराई से सोचने के बाद भी, मेरे पास कभी कोई हीरो नहीं था। हीरो की छवि मुझे आकर्षित नहीं करती। शायद इसलिए कि मैं किसी एक गुण या नैतिक मूल्य को दूसरे से अधिक महत्व नहीं देता। मैं हीरो को जानता हूँ, उन्हें पहचान सकता हूँ, लेकिन न तो मैं उन्हें प्यार करता हूँ और न ही उनकी पूजा करता हूँ। सच कहूँ तो, मैं कट्टरपंथियों से घृणा करता हूँ। अगर किसी को हीरो माना जाता है, तो वह उसकी उपलब्धियों, उसके वीरतापूर्ण कार्यों और उसकी विशिष्टता के कारण होता है। लेकिन उसके वीरतापूर्ण कार्य खत्म होने के बाद उसके पास क्या बचता है? सिर्फ उसकी उपाधि । यह माना जा सकता है कि हीरो एक आभा बनाए रखता है: उसके कार्य उससे चमकते हैं। मैं मानता हूँ कि कार्य को अपने रचयिता से मुक्त होकर अपना जीवन जीना चाहिए। रचनाकार की संतानें अपनी ही दुनिया खोज लेंगी, और कभी-कभी उन्हें अपने मार्ग में कुछ सख्त और ईर्ष्यालु समीक्षक मिलेंगे।",
		"Je ne t'aime pas.":
		"मैं तुमसे प्यार नहीं करता।",
		"Tu ne me connais pas.":
		"तुम मुझे नहीं जानती।",
		"Nous n'avons rien en commun.":
		"हमारे बीच कुछ भी सामान्य नहीं है।",
		"Je ne veux rien de toi.":
		"मुझे तुमसे कुछ नहीं चाहिए।",
		"Tu n'es pas un modèle pour moi.":
		"तुम मेरे लिए आदर्श नहीं हो।",
		"Je veux voler de mes propres ailes.":
		"मैं अपना मार्ग खुद बनाना चाहता हूँ।",
		"Bientot je partirai.":
		"जल्द ही, मैं चला जाऊँगा।",

		"Si ce n'était qu'elle, je pourrais l'accepter.":
		"मैं ऐसा अपनी पत्नी से सह सकता हूँ,",
		"Mais mon fils dispose des mêmes armes.":
		"लेकिन मेरा बेटा ऐसा मेरे साथ कैसे कर सकता है?",
		"Il voudrait mon avis sur sa rédaction. ":
		"वह चाहता है कि मैं उसकी लेखनी पढ़ूं,",
		"Mais je ne parviens pas à me concentrer sur le texte.":
		"लेकिन मैं शब्दों पर ध्यान नहीं दे पा रहा हूँ,",
		"Étrange impression de ne pouvoir lire qu'entre les lignes…":
		"ऐसा क्यों है कि मैं केवल पंक्तियों के बीच का ही अर्थ समझ पा रहा हूँ?",


		"Suis-je si peu présent ?":
		"क्या मैं यहाँ इतना कम हूँ?",
		"Si modelable ?":
		"क्या इतना ढलने योग्य हूँ?",
		"Ma propre image semble me fuir.":
		"मेरा अपना प्रतिबिंब मुझसे दूर जाता प्रतीत होता है।",
		"Elle m'échappe.":
		"यह मुझसे छूट जाता है।",
		"Je me sens manipulé.":
		"मुझे लगता है कि मुझसे खेला जा रहा है।.",



		"Il est temps de reprendre le contrôle.":
		"अब फिर से नियंत्रण अपने हाथ में लेने का समय है।",
		"Arrêter de tourner en rond.":
		"बेकार में गोल-गोल घूमना बंद करना है।",

		"Retrouver une direction.":
		"कुछ दिशानिर्देश ढूंढने हैं।",
		"Plier le cours des événements.":
		"घटनाओं को आकार देना है।",
		"Donner un sens à mes actions.":
		"अपनी क्रियाओं को अर्थ देना है।.",

		"Je fais tout pour maîtriser de nouveau le cours de ma vie.":
		"मैं फिर से अपने जीवन पर नियंत्रण पाने की पूरी कोशिश कर रहा हूँ",
		"Je choisis.":
		"मैं निर्णय लेता हूँ",
		"Mes émotions.":
		"मैं अपनी भावनाओं को नियंत्रित करता हूँ",
		"Le sens à donner aux choses.":
		"चीजों का अर्थ समझता हूँ",
		"Enfin, je me suis repr":
		"अंततः, मेरे अस्ति...",

		
    "fin":
    "समाप्त।",


	// fichiers sonores
    // chp1

    "assets/sound/chapitre1/BienvenueAppuyez.mp3":
        "assets/sound/chapitre1/BienvenueAppuyez-hi.mp3",
    "assets/sound/chapitre1/BipEtBravo.mp3":
        "assets/sound/chapitre1/BipEtBravo-hi.mp3",
    "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous.mp3":
        "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous-hi.mp3",
    "assets/sound/chapitre1/BravoVotreRendezVousEstArrive.mp3":
        "assets/sound/chapitre1/BravoVotreRendezVousEstArrive-hi.mp3",

    // chp2
    "assets/sound/chapitre2/julien1.mp3":
        "assets/sound/chapitre2/HI_man_1.mp3",
    "assets/sound/chapitre2/julien2.mp3":
        "assets/sound/chapitre2/HI_man_2.mp3",
    "assets/sound/chapitre2/julien3.mp3":
        "assets/sound/chapitre2/HI_man_3.mp3",
    "assets/sound/chapitre2/julien4.mp3":
        "assets/sound/chapitre2/HI_man_4.mp3",
    "assets/sound/chapitre2/julien5.mp3":
        "assets/sound/chapitre2/HI_man_5.mp3",
    "assets/sound/chapitre2/julien6.mp3":
        "assets/sound/chapitre2/HI_man_6.mp3",
    "assets/sound/chapitre2/julien7.mp3":
        "assets/sound/chapitre2/HI_man_7.mp3",
    "assets/sound/chapitre2/julien8.mp3":
        "assets/sound/chapitre2/HI_man_8.mp3",
    "assets/sound/chapitre2/julien9.mp3":
        "assets/sound/chapitre2/HI_man_9.mp3",

    // chp4


    "assets/sound/chapitre4/maximilienEssay.mp3":
        "assets/sound/chapitre4/maximilienEssay-hi.mp3",
    "assets/sound/chapitre4/max-jenetaimepas.mp3":
        "assets/sound/chapitre4/jenetaimepas-hi.mp3",
    "assets/sound/chapitre4/max-tunemeconnaispas.mp3":
        "assets/sound/chapitre4/tunemeconnaispas-hi.mp3",
    "assets/sound/chapitre4/max-nousnavonsrienencommun.mp3":
        "assets/sound/chapitre4/nousnavonsrienencommun-hi.mp3",
    "assets/sound/chapitre4/max-jeneveuxriendetoi.mp3":
        "assets/sound/chapitre4/jeneveuxriendetoi-hi.mp3",
    "assets/sound/chapitre4/max-tunespasunmodelepourmoi.mp3":
        "assets/sound/chapitre4/tunespasunmodelepourmoi-hi.mp3",
    "assets/sound/chapitre4/max-jeveuxvolerdemespropresailes.mp3":
        "assets/sound/chapitre4/jeveuxvolerdemespropresailes-hi.mp3",
    "assets/sound/chapitre4/max-bientotjepartirai.mp3":
        "assets/sound/chapitre4/bientotjepartirai-hi.mp3"
};
