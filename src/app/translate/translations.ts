// app/translate/translation.ts

import { InjectionToken } from  '@angular/core';

// import translations
import { LANG_EN_NAME, LANG_EN_TRANS } from './lang-en';
import { LANG_ES_NAME, LANG_ES_TRANS } from './lang-es';
import { LANG_FR_NAME, LANG_FR_TRANS } from './lang-fr';
import { LANG_HI_NAME, LANG_HI_TRANS } from './lang-hi';
import { LANG_PT_NAME, LANG_PT_TRANS } from './lang-pt';
import { LANG_IT_NAME, LANG_IT_TRANS } from './lang-it';
import { LANG_DE_NAME, LANG_DE_TRANS } from './lang-de';
import { LANG_PL_NAME, LANG_PL_TRANS } from './lang-pl';
import { LANG_HU_NAME, LANG_HU_TRANS } from './lang-hu';
import { LANG_ZH_NAME, LANG_ZH_TRANS } from './lang-zh';
import { LANG_AR_NAME, LANG_AR_TRANS } from './lang-ar';
import { LANG_RU_NAME, LANG_RU_TRANS } from './lang-ru';

// translation token
export const TRANSLATIONS = new InjectionToken('translations');

// all traslations
export const dictionary = {
	[LANG_EN_NAME]: LANG_EN_TRANS,
	[LANG_ES_NAME]: LANG_ES_TRANS,
	[LANG_FR_NAME]: LANG_FR_TRANS,
	[LANG_HI_NAME]: LANG_HI_TRANS,
	[LANG_PT_NAME]: LANG_PT_TRANS,
	[LANG_IT_NAME]: LANG_IT_TRANS,
	[LANG_DE_NAME]: LANG_DE_TRANS,
	[LANG_PL_NAME]: LANG_PL_TRANS,
    [LANG_HU_NAME]: LANG_HU_TRANS,
    [LANG_ZH_NAME]: LANG_ZH_TRANS,
    [LANG_AR_NAME]: LANG_AR_TRANS,
    [LANG_RU_NAME]: LANG_RU_TRANS,
};

// providers
export const TRANSLATION_PROVIDERS = [
	{ provide: TRANSLATIONS, useValue: dictionary },
];
