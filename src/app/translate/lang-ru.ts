// lang-en.ts

export const LANG_RU_NAME = 'ru';

export const LANG_RU_TRANS = {

    "Accueil":
        "Главная",
    "Présentation":
        "Описание",
    "Crédits":
        "Титры",
    "Prix":
        "Награды",
    "Scènes":
        "Сцены",
    "Scène":
        "Сцена",
    "Version Flash":
        "Flash версия",
    "Version Angular":
        "JavaScript версия",
    "Version application smartphone":
        "Версия для мобильного приложения",
    "Version":
        "Версия",
    "Langues":
        "Языки",
    "Réalisation":
        "Замысел и реализация",
    "Traduction":
        "Перевод",
    "Voix":
        "Голос",
    "Épouse":
        "Супруга",
    "Musique":
        "Музыка",
    "Vidéo":
        "Видео",
    "Téléphone":
        "Телефон",
    "Narrateur":
        "Рассказчик",
    "ado":
        "Подросток",
    "Ambiance":
        "Эмбиент",
    "Manipulation":
        "Обработка",
    "Saxophone":
        "Саксофон",
    "Guitare électrique":
        "Электрическая гитара",
    "Batterie/Montage":
        "Ударные/Монтаж",
    "Extrait du disque":
        "Отрывок из диска",

    "Déprise est également disponible sous forme d'application mobile.":
        "«Утрата контроля» также доступна в формате мобильного приложения:",
    "Application sur Google Play":
        "Приложение в Google Play",
    "Application sur l'Apple Store":
        "Приложение в App Store",

    "Déprise a gagné le prix New Media Writing Prize en 2011.":
        "«Утрата контроля» была удостоена премии New Media Writing Prize 2011.",
    "Voir le site":
        "Перейти на сайт",

    'hello world': 'hello world',
    "Toute ma vie, j'ai cru avoir devant moi un champ des possibles infini.": "Всю жизнь я верил, что передо мной бескрайнее море возможностей.",
    "Déprise": "Утрата контроля",
    "Déprise est une création sur les notions de prise et perte de contrôle. Quand a-t-on l'impression d'être en situation de prise ou de perte de prise dans la vie? Six scènes racontent l'histoire d'un personnage en pleine déprise. Parallèlement, ce jeu sur prise et déprise permet de mettre en scène la situation du lecteur d'une oeuvre interactive.": "«Утрата контроля» — это произведение о понятиях обретения и утраты контроля. При каких обстоятельствах мы чувствуем, что обретаем или теряем контроль над своей жизнью? Шесть сцен рассказывают историю героя, который переживает экзистенциальный кризис. В то же время, эта игра с понятиями обретения и утраты контроля отражает опыт читателя интерактивного произведения цифровой литературы.",
    "Avez-vous pensé à allumer vos enceintes ?": "Вы включили динамики?",
    "(Appuyez sur n'importe quelle touche pour continuer)": "(Чтобы продолжить, нажмите на любую клавишу)",
    "\"L'univers entier m'appartient\", pensais-je.": "\«Вся вселенная принадлежит мне\», — думал я.",
    "J'ai le choix.": "У меня есть выбор.",

    "Je suis maître de mon destin.":
        "Я господин своей судьбы.",
    "Je peux prendre ce qui me plaît.":
        "Я могу взять то, что мне нравится.",
    "Je deviendrai ce que je veux.":
        "Я стану кем захочу.",
    "J'ai tracé mon propre chemin.":
        "Я создал собственный путь.",
    "J'ai parcouru de magnifiques paysages.":
        "Мой путь проходил по великолепным пейзажам.",
    "Quoi de plus naturel, je les avais choisis.":
        "Неудивительно, ведь я их выбрал.",
    "Mais depuis un moment, j'ai des doutes.":
        "Но с некоторого времени меня терзают сомнения.",
    "Comment avoir prise sur ce qui m'arrive ?":
        "Как обрести контроль над происходящим со мной?",
    "Tout s'échappe.":
        "От меня всё ускользает.",
    "Me glisse entre les doigts.":
        "Уплывает сквозь пальцы.",
    "Les objets, les personnes.":
        "Предметы, люди.",
    "J'ai l'impression de ne plus rien contrôler.":
        "Чувствую, что больше ничего не контролирую.",
    "Depuis quelques temps maintenant,":
        "Уже некоторое время",
    "Je n'attends qu'une chose.":
        "Я жду лишь одну вещь.",
    "La suite.":
        "Продолжение.",

    "Et le rendez-vous est arrivé.":
        "Время встречи настало.",

    "Mais le rendez-vous était biaisé.":
        "Но во встрече был подвох.",
    "Je ne m'en suis rendu compte que beaucoup plus tard.":
        "Понял я это лишь намного позже.",
    "La femme en face de moi, qui paraissait si parfaite, me laissait bouche bée.":
        "Женщина передо мной казалась такой идеальной, что я смотрел, разинув рот.",
    "Impossible de prononcer quelque chose de cohérent.":
        "Не мог произнести ни слова.",
    "Sa présence me bouleversait...":
        "От ее присутствия у меня путались мысли.",
    "Il fallait que je pose des questions pour la mettre à jour.":
        "Я должен был задавать вопросы, чтобы по-настоящему ее увидеть.",
    "Sans que je m'en aperçoive, cette inconnue devenait ma femme.":
        "Незаметно для меня, эта незнакомка становилась моей женой.",
    "On a tout partagé.":
        "Мы разделяли всё.",
    "Mais jamais je ne suis parvenu à vraiment la connaître.":
        "Но я так и не смог по-настоящему ее узнать.",
    "Aujourd'hui encore je me pose des questions.":
        "И по сей день я задаюсь вопросами.",
    "Qui d'elle ou moi suit l'autre ?":
        "Кто следует за кем: она за мной или я за ней?",
    "Quand je l'aime, elle me sème.":
        "Когда я люблю ее, она от меня ускользает.",

    "Qui êtes-vous ?":
        "Кто вы?",
    "Vous aimez...":
        "Вам нравится...",
    "Que pensez-vous...":
        "Что вы думаете о...",
    "D'où venez-vous ?":
        "Откуда вы?",
    "Où allez-vous ?":
        "Куда вы идете?",
    "Vous pensez quoi de...":
        "Какого вы мнения о...",


    "Vous habitez la région depuis longtemps ?":
        "Давно вы живете в этом регионе?",
    "Vous évitez la légion depuis longtemps ?":
        "Давно вы служите в этом легионе?",
    "Et vous travaillez dans quoi ?":
        "Кем вы работаете?",
    "Et vous travaillez l'envoi ?":
        "Тем вы рабов таите?",
    "Je vous trouve vraiment très jolie !":
        "Вы очень красивы!",
    "Chevaux, brousse, bêlement... près jolis":
        "Вымочили крапивы!",
    "J'ai l'impression qu'on a beaucoup de points communs":
        "Мне кажется, у нас много общего.",
    "Chemins pression en Allemagne point comme un...":
        "Мин гаджеты — унос миног общего",
    "Vous avez des yeux somptueux":
        "У вас прекрасные глаза.",
    "Vous avouez des notions de tueurs":
        "Анфас покрашена бирюза",
    "Vous venez souvent ici ?":
        "Вы часто здесь бываете?",
    "Vous avez l'absent acquis":
        "Вы часом снедь кидаете?",
    "Puis-je vous offrir un autre verre ?":
        "Могу я пригласить вас на еще один бокал?",
    "Pigeon ouïr un Notre Père ?":
        "Могучая при гласе весна, еще один вокал?",
    "J'aime votre façon de sourire":
        "Мне нравится ваша улыбка.",
    "Gêne, votre garçon mourir":
        "Мне нравы отца ваша опалубка",
    "Vous voulez marcher un peu ?":
        "Прогуляемся?",
    "Nouveaux-nés barges et il pleut.":
        "Прогул ямса?",

    "Vingt ans se sont écoulés depuis notre rencontre.":
        "С момента нашей встречи прошло двадцать лет.",
    "Ce matin, je me perds dans un mot qu'elle m'a laissé.":
        "Этим утром я в растерянности из-за оставленной ею записки.",
    "Tout se brouille dans mon esprit.":
        "В моей голове всё смешалось.",
    "Je ne sais comment l'interpréter.":
        "Я не знаю, как ее истолковать.",
    "Mot d'amour ou de rupture ?":
        "Любовное письмо или же прощальное?",
    "Le fait-elle exprès ?":
        "Она это специально?",


    "Je sais que c'est pour toi un choc":
        "Знаю, для тебя это будет шоком",
    "Je n'ai que de l'amour pour toi":
        "К тебе я испытываю лишь любовь",
    "Est un mensonge, et":
        "Это ложь, и",
    "Dans un couple, il y en a un qui souffre et un qui s'ennuie":
        "\"В паре всегда есть тот, кто страдает, и тот, кто скучает\"",
    "Je veux que tous nos amis sachent que":
        "Я хочу, чтобы все наши друзья знали, что",
    "Je ne veux pas rester avec toi,":
        "Я не хочу быть с тобой,",
    "Depuis le premier jour, je ne sais pas comment tu peux croire que":
        "С самого первого дня я не понимаю, как ты можешь думать, что ",
    "Je t'aime":
        "Я тебя люблю",
    "Mon amour":
        "Любовь моя",
    "A disparu":
        "Пропала",
    "L'indifférence":
        "Холодность",
    "Est plus vivace que jamais":
        "Сильна как никогда",
    "Le charme de notre rencontre":
        "Прелесть нашей встречи",
    "S'est dissipé à présent":
        "Уже рассеялась",
    "Et le moindre malentendu":
        "И любая размолвка",
    "A vaincu":
        "Победит",
    "Notre amour":
        "Любовь между нами",
    "Je n'ai pas de héros. D'aussi loin qu'il m'en souvienne, et même après une longue réflexion, je n'ai jamais eu de héros. La figure du héros ne me séduit pas. Sans doute parce que je ne préfère pas une qualité à une autre, une valeur morale à une autre. Les héros, je les connais, je les reconnais, mais je n'éprouve ni adoration, ni idolâtrie pour eux. À dire vrai, le fanatisme me rend fou. Si l'on considère que le héros obtient son titre par ses actions, son statut est donc une forme de récompense pour la prouesse, la hardiesse, l'originalité. Mais que reste-t-il à la personne, une fois l'acte terminé ? Rien, sinon le titre. On peut présumer que le héros retire de son action une aura : l'action brille à travers lui. J'ai tendance à croire, que l'oeuvre — l'action selon les domaines — doit quitter son créateur pour pouvoir vivre pleinement. Les enfants-livres des auteurs vont eux-mêmes tracer leur chemin, se cognant à l'occasion à quelques Zoïles.":
        "У меня нет героя. Насколько я себя помню, даже хорошо подумав, я не могу вспомнить, чтобы у меня когда-либо был герой. Фигура героя не привлекает меня. Несомненно, потому что я не ставлю одно качество выше другого, одну моральную ценность выше другой. Я знаю героев, я их признаю, но они не вызывают у меня ни восхищения, ни преклонения. Честно говоря, я терпеть не могу фанатизм. Если считать, что герой становится таковым благодаря своим действиям, тогда его статус — это некая награда за достижение, смелость, оригинальность. Но что остается человеку, когда действие завершено? Лишь статус. Можно предположить, что действие придает герою некую ауру: действие светится в нём. Я склоняюсь к мысли о том, что творение — деяние в некоторых областях — должно освободиться от своего создателя, чтобы обрести полноценную жизнь. Книги-дети авторов сами проложат свой путь, наталкиваясь время от времени на отдельных Зоилов.",
    "Je ne t'aime pas.":
        "Я тебя не люблю.",
    "Tu ne me connais pas.":
        "Ты меня не знаешь.",
    "Nous n'avons rien en commun.":
        "У нас нет ничего общего.",
    "Je ne veux rien de toi.":
        "Мне от тебя ничего не надо.",
    "Tu n'es pas un modèle pour moi.":
        "Ты для меня не пример.",
    "Je veux voler de mes propres ailes.":
        "Я хочу быть самостоятельным.",
    "Bientot je partirai.":
        "Я скоро уйду.",

    "Si ce n'était qu'elle, je pourrais l'accepter.":
        "Будь это только она, я бы мог это принять.",
    "Mais mon fils dispose des mêmes armes.":
        "Но у моего сына то же оружие.",
    "Il voudrait mon avis sur sa rédaction. ":
        "Он спрашивает мое мнение о его эссе.",
    "Mais je ne parviens pas à me concentrer sur le texte.":
        "Но у меня не получается сосредоточиться на тексте.",
    "Étrange impression de ne pouvoir lire qu'entre les lignes…":
        "У меня странное ощущение, что я могу читать лишь между строк…",


    "Suis-je si peu présent ?":
        "Неужели мое присутствие так незначительно?",
    "Si modelable ?":
        "Так подвержено искажению?",
    "Ma propre image semble me fuir.":
        "Кажется, мой собственный образ от меня ускользает.",
    "Elle m'échappe.":
        "Я его теряю.",
    "Je me sens manipulé.":
        "Я чувствую, что мной манипулируют.",



    "Il est temps de reprendre le contrôle.":
        "Пора вернуть себе контроль",
    "Arrêter de tourner en rond.":
        "Перестать ходить кругами.",

    "Retrouver une direction.":
        "Вновь обрести цель.",
    "Plier le cours des événements.":
        "Задать направление событий.",
    "Donner un sens à mes actions.":
        "Придать смысл моим действиям.",


    "Je fais tout pour maîtriser de nouveau le cours de ma vie.":
        "Я делаю всё возможное, чтобы вновь обрести контроль над течением моей жизни.",
    "Je choisis.":
        "Я выбираю.",
    "Mes émotions.":
        "Мои эмоции.",
    "Le sens à donner aux choses.":
        "Придаваемый вещам смысл.",
    "Enfin, je me suis repr":
        "Наконец, я вновь обрел конт…",

    "fin":
        "Конец",

    // fichiers sonores
    // chp1

    "assets/sound/chapitre1/BienvenueAppuyez.mp3":
        "assets/sound/chapitre1/BienvenueAppuyez-ru.mp3",
    "assets/sound/chapitre1/BipEtBravo.mp3":
        "assets/sound/chapitre1/BipEtBravo-ru.mp3",
    "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous.mp3":
        "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous-ru.mp3",
    "assets/sound/chapitre1/BravoVotreRendezVousEstArrive.mp3":
        "assets/sound/chapitre1/BravoVotreRendezVousEstArrive-ru.mp3",

    // chp2
    "assets/sound/chapitre2/julien1.mp3":
        "assets/sound/chapitre2/RU_man_1.mp3",
    "assets/sound/chapitre2/julien2.mp3":
        "assets/sound/chapitre2/RU_man_2.mp3",
    "assets/sound/chapitre2/julien3.mp3":
        "assets/sound/chapitre2/RU_man_3.mp3",
    "assets/sound/chapitre2/julien4.mp3":
        "assets/sound/chapitre2/RU_man_4.mp3",
    "assets/sound/chapitre2/julien5.mp3":
        "assets/sound/chapitre2/RU_man_5.mp3",
    "assets/sound/chapitre2/julien6.mp3":
        "assets/sound/chapitre2/RU_man_6.mp3",
    "assets/sound/chapitre2/julien7.mp3":
        "assets/sound/chapitre2/RU_man_7.mp3",
    "assets/sound/chapitre2/julien8.mp3":
        "assets/sound/chapitre2/RU_man_8.mp3",
    "assets/sound/chapitre2/julien9.mp3":
        "assets/sound/chapitre2/RU_man_9.mp3",

    // chp4


    "assets/sound/chapitre4/maximilienEssay.mp3":
        "assets/sound/chapitre4/maximilienEssay-ru.mp3",
    "assets/sound/chapitre4/max-jenetaimepas.mp3":
        "assets/sound/chapitre4/jenetaimepas-ru.mp3",
    "assets/sound/chapitre4/max-tunemeconnaispas.mp3":
        "assets/sound/chapitre4/tunemeconnaispas-ru.mp3",
    "assets/sound/chapitre4/max-nousnavonsrienencommun.mp3":
        "assets/sound/chapitre4/nousnavonsrienencommun-ru.mp3",
    "assets/sound/chapitre4/max-jeneveuxriendetoi.mp3":
        "assets/sound/chapitre4/jeneveuxriendetoi-ru.mp3",
    "assets/sound/chapitre4/max-tunespasunmodelepourmoi.mp3":
        "assets/sound/chapitre4/tunespasunmodelepourmoi-ru.mp3",
    "assets/sound/chapitre4/max-jeveuxvolerdemespropresailes.mp3":
        "assets/sound/chapitre4/jeveuxvolerdemespropresailes-ru.mp3",
    "assets/sound/chapitre4/max-bientotjepartirai.mp3":
        "assets/sound/chapitre4/bientotjepartirai-ru.mp3"
};