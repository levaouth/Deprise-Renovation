import {Injectable, Inject} from '@angular/core';
import { TRANSLATIONS } from './translations'; // import our opaque token
import { DOCUMENT } from '@angular/platform-browser';

@Injectable()
export class TranslateService {
	private _currentLang: string;
	public get currentLang() {
	  return this._currentLang;
	}

    // inject our translations
    constructor(@Inject(TRANSLATIONS) private _translations: any) {
        
		// set current language
	 	var langueUtil = JSON.parse(localStorage.getItem('currentLang') || "null");
	 	if (langueUtil!=null){
            this._currentLang = langueUtil;
          }
        this.setGlobalHtmlAttribut(this._currentLang);
	}

	public use(lang: string): void {
		console.log(lang);
        this._currentLang = lang;
        localStorage.setItem('currentLang', JSON.stringify(lang));
	}

	public translate(key: string): string {
		// private perform translation
		let translation = key;
            
    	if (this._translations[this.currentLang] && this._translations[this.currentLang][key]) {
			return this._translations[this.currentLang][key];
		}

		return translation;
    }
    
    public setGlobalHtmlAttribut(lang: string) {
        document.documentElement.lang = lang;
    }

    ngOnInit(){
    }

	public instant(key: string) {
		// public perform translation
		return this.translate(key);
	}
}
