// lang-pl.ts

export const LANG_PL_NAME = 'pl';

export const LANG_PL_TRANS = {
    "Accueil":  "Powitanie",
    "Présentation":  "O utworze",
    "Crédits":  "Podziękowania",
    "Prix":  "Nagrody",
    "Scènes":  "Sceny",
    "Scène":  "Scena",
    "Version Flash":  "Wersja Flash ",
    "Version Angular": "Wersja JavaScript",
    "Version application smartphone":
    "Version app",
    "Version Ionic":  "Wersja na urządzenia mobilne",
    "Version":  "Wersja",
    "Langues":  "Języki",
    "Réalisation":  "Reżyseria",
    "Traduction":  "Tłumaczenie",
    "Voix":  "Głosu użyczyli",
    "Épouse":  "Żona",
    "Musique":  "Muzyka",
    "Vidéo":  "Wideo",
    "Téléphone":  "Głos w telefonie",
    "Narrateur":  "Narrator",
    "ado":  "Nastolatek",
    "Ambiance":  "Dźwięki otoczenia",
    "Manipulation":  "Manipulacja",
    "Saxophone":  "Saksofon",
    "Guitare électrique":  "Gitara elektryczna",
    "Batterie/Montage":  "Perkusja/Montaż",
    "Extrait du disque":  "Fragment płyty",
    "Déprise a gagné le prix New Media Writing Prize en 2011.":
        "(Nie)panowanie zostało uhonorowane New Media Writing Prize 2011",
    "Voir le site":
        "Zobacz stronę www",

    "Déprise est également disponible sous forme d'application mobile.":
        "(Nie)panowanie jest dostępne również jako aplikacja na urządzenia mobilne.",
    "Application sur Google Play":
        "Aplikacja w Google Play",
    "Application sur l'Apple Store":
        "Aplikacja w Apple Store",

    "Toute ma vie, j'ai cru avoir devant moi un champ des possibles infini.": "Całe moje życie wierzyłem, że mam przed sobą nieograniczone możliwości.",
    "déprise": "(Nie)panowanie",
    "Déprise": "(Nie)panowanie",
    "Déprise est une création sur les notions de prise et perte de contrôle. Quand a-t-on l'impression d'être en situation de prise ou de perte de prise dans la vie? Six scènes racontent l'histoire d'un personnage en pleine déprise. Parallèlement, ce jeu sur prise et déprise permet de mettre en scène la situation du lecteur d'une oeuvre interactive.":
        "(Nie)panowanie to utwór elektroniczny o panowaniu, posiadaniu kontroli. W jakich okolicznościach czujemy, że trzymamy ster naszego życia w rękach, a w jakich nie? Sześć scen utworu ukazuje bohatera, który traci to poczucie. Równocześnie napięcie między momentami, w których ma się wrażenie pełnej kontroli, i chwilami, gdy ono ucieka, znajduje odbicie w czytelniczym doświadczeniu lektury interaktywnego tekstu.",
    "Avez-vous pensé à allumer vos enceintes ?": "Czy masz włączony dźwięk?",
    "(Appuyez sur n'importe quelle touche pour continuer)":
        "(Aby kontynuować, wciśnij dowolny klawisz)",
    "\"L'univers entier m'appartient\", pensais-je.":
        "„Cały wszechświat do mnie należy” - myślałem sobie.",
    "J'ai le choix.":
        "Mam wybór.",
    "Je suis maître de mon destin.":
        "Kontroluję moje przeznaczenie.",
    "Je peux prendre ce qui me plaît.":
        "Jestem panem świata.",
    "Je deviendrai ce que je veux.":
        "Stanę się tym, kim chcę.",
    "J'ai tracé mon propre chemin.":
        "Podążałem swoją własną ścieżką.",
    "J'ai parcouru de magnifiques paysages.":
        "Przemierzałem piękne krajobrazy.",
    "Quoi de plus naturel, je les avais choisis.":
        "Nic dziwnego – to ja wybrałem tę drogę.",
    "Mais depuis un moment, j'ai des doutes.":
        "Ale od pewnego czasu mam wątpliwości.",
    "Comment avoir prise sur ce qui m'arrive ?":
        "Jak mogę zapanować nad tym, co mi się przydarza?",
    "Tout s'échappe.":
        "Wszystko mi umyka.",
    "Me glisse entre les doigts.":
        "Przelatuje mi przez palce...",
    "Les objets, les personnes.":
        "przedmioty, ludzie...",
    "J'ai l'impression de ne plus rien contrôler.":
        "Czuję, że straciłem kontrolę.",
    "Depuis quelques temps maintenant,":
        "Od jakiegoś czasu",
    "Je n'attends qu'une chose.":
        "czekam tylko na jedno.",
    "La suite.":
        "Na to, co będzie dalej.",

    "Et le rendez-vous est arrivé.":
        "Nadszedł czas spotkania.",

    "Mais le rendez-vous était biaisé.":
        "Ale to spotkanie miało w sobie coś z pułapki.",
    "Je ne m'en suis rendu compte que beaucoup plus tard.":
        "Choć zdałem sobie z tego sprawę dopiero później.",
    "La femme en face de moi, qui paraissait si parfaite, me laissait bouche bée.":
        "Kobieta naprzeciw mnie wydawała się tak idealna, że stałem w osłupieniu.",
    "Impossible de prononcer quelque chose de cohérent.":
        "Nie dawałem rady powiedzieć nic sensownego.",
    "Sa présence me bouleversait...":
        "Byłem zdruzgotany.",
    "Il fallait que je pose des questions pour la mettre à jour.":
        "Chcąc nie chcąc zadawałem pytania, by ją poznać, odsłonić.",
    "Sans que je m'en aperçoive, cette inconnue devenait ma femme.":
        "Nie wiem, jak i kiedy ta obca osoba stała się moją żoną.",
    "On a tout partagé.":
        "Dzieliliśmy wszystko.",
    "Mais jamais je ne suis parvenu à vraiment la connaître.":
        "Ale nigdy nie poznałem jej tak naprawdę.",
    "Aujourd'hui encore je me pose des questions.":
        "I dziś wciąż wątpię, pytam:",
    "Qui d'elle ou moi suit l'autre ?":
        "które z nas podążą za którym?",
    "Quand je l'aime, elle me sème.":
        "Gdy ja ją kocham, ona mnie traci.",

    "Qui êtes-vous ?":
        "Kim jesteś?",
    "Vous aimez...":
        "Czy lubisz...",
    "Que pensez-vous...":
        "Co sądzisz o...",
    "D'où venez-vous ?":
        "Skąd jesteś?",
    "Où allez-vous ?":
        "Dokąd zmierzasz?",
    "Vous pensez quoi de...":
        "Myslisz, że...",
    "Vous habitez la région depuis longtemps ?":
        "Czy długo mieszkasz w tej okolicy?",
    "Vous évitez la légion depuis longtemps ?":
        "Czy długi masz w tej okolicy?",
    "Et vous travaillez dans quoi ?":
        "Czym się zajmujesz, by związać koniec z końcem?",
    "Et vous travaillez l'envoi ?":
        "Czym się szprycujesz, by związać koniec z końcem?",
    "Je vous trouve vraiment très jolie !":
        "Wyglądasz olśniewająco!",
    "Chevaux, brousse, bêlement... près jolis":
        "Wyglądasz ogłupiająco",
    "J'ai l'impression qu'on a beaucoup de points communs":
        "Czuję, że wiele nas łączy.",
    "Chemins pression en Allemagne point comme un...":
        "Czuję, że wiele tu kłączy",
    "Vous avez des yeux somptueux":
        "Masz cudne oczy...",
    "Vous avouez des notions de tueurs":
        "Masz złudne oczy...",
    "Vous venez souvent ici ?":
        "Często tu bywasz?",
    "Vous avez l'absent acquis":
        "Czym to zbywasz?",
    "Puis-je vous offrir un autre verre ?":
        "Mogę cię zaprosić na drinka?",
    "Pigeon ouïr un Notre Père ?":
        "Może zbyt ciemna ta szminka?",
    "J'aime votre façon de sourire":
        "Podoba mi się, jak się śmiejesz.",
    "Gêne, votre garçon mourir":
        "Podoba mi się, jak siwiejesz.",
    "Vous voulez marcher un peu ?":
        "Może się przejdziemy?",
    "Nouveaux-nés barges et il pleut.":
        "Może się przejmiemy?",

    "Vingt ans se sont écoulés depuis notre rencontre.":
        "Minęło dwadzieścia lat od czasu, gdy się poznaliśmy.",
    "Ce matin, je me perds dans un mot qu'elle m'a laissé.":
        "Czytam o poranku liścik, który mi zostawiła.",
    "Tout se brouille dans mon esprit.":
        "Jestem w rozterce.",
    "Je ne sais comment l'interpréter.":
        "Nie wiem, co o tym myśleć.",
    "Mot d'amour ou de rupture ?":
        "To wyznanie miłosne czy list, którym ze mną zrywa?",
    "Le fait-elle exprès ?":
        "Zrobiła to specjalnie?",


    "Je sais que c'est pour toi un choc":
        "Wiem, że to dla Ciebie szok",
    "Je n'ai que de l'amour pour toi":
        "Wszystko, co do Ciebie czuję, to miłość",
    "Est un mensonge, et":
        "To kłamstwo",
    "Dans un couple, il y en a un qui souffre et un qui s'ennuie":
        "\"Przecież w każdym związku jest ktoś, kto cierpi i ktoś, kto jest znudzony\"",
    "Je veux que tous nos amis sachent que":
        "Chciałabym, żeby wszyscy Twoi przyjaciele wiedzieli, że",
    "Je ne veux pas rester avec toi,":
        "Nie chcę z Tobą dłużej zostać,",
    "Depuis le premier jour, je ne sais pas comment tu peux croire que":
        "Od pierwszej chwili zastanawiałam się, jak możesz w wierzyć, że",
    "Je t'aime":
        "Ja Cię Kocham",
    "Mon amour":
        "Kochanie",
    "A disparu":
        "Zniknęło",
    "L'indifférence":
        "Poczucie obojętności",
    "Est plus vivace que jamais":
        "Jest żywsze niż kiedykolwiek wcześniej",
    "Le charme de notre rencontre":
        "Wzajemne oczarowanie",
    "S'est dissipé à présent":
        "Już nic nie znaczy",
    "Et le moindre malentendu":
        "Jakieś drobne nieporozumienie",
    "A vaincu":
        "Zwyciężyło",
    "Notre amour":
        "Nasze uczucie",
    "Je n'ai pas de héros. D'aussi loin qu'il m'en souvienne, et même après une longue réflexion, je n'ai jamais eu de héros. La figure du héros ne me séduit pas. Sans doute parce que je ne préfère pas une qualité à une autre, une valeur morale à une autre. Les héros, je les connais, je les reconnais, mais je n'éprouve ni adoration, ni idolâtrie pour eux. À dire vrai, le fanatisme me rend fou. Si l'on considère que le héros obtient son titre par ses actions, son statut est donc une forme de récompense pour la prouesse, la hardiesse, l'originalité. Mais que reste-t-il à la personne, une fois l'acte terminé ? Rien, sinon le titre. On peut présumer que le héros retire de son action une aura : l'action brille à travers lui. J'ai tendance à croire, que l'oeuvre — l'action selon les domaines — doit quitter son créateur pour pouvoir vivre pleinement. Les enfants-livres des auteurs vont eux-mêmes tracer leur chemin, se cognant à l'occasion à quelques Zoïles.":
        "Nikt nie jest dla mnie bohaterem. Jak daleko bym nie sięgał pamięcią, nawet kiedy bardzo intensywnie próbuję sobie przypomnieć, nikt nim nigdy dla mnie nie był. Figura bohatera do mnie nie przemawia. Pewnie dlatego, że nie cenię jednych cech charakteru czy wartości moralnych bardziej niż inne. Znam bohaterów, rozpoznaję ich, ale nie darzę ich miłością ani ich nie wielbię. Mówiąc prawdę – nie znoszę fanatyków. Jeśli uznamy, że człowieka czyni bohaterem to, co robi, musimy przyjąć, że ten tytuł wynagradza jego uczynki, heroiczne działania, jego wyjątkowość. Ale z czym taki ktoś zostanie, gdy jego bohaterskie przedsięwzięcia dobiegną końca? Z niczym, prócz tytułu. Przyjmuje się, że bohater ma jakąś aurę, jakby jego działania przeświecały przez niego. A ja jestem skłonny wierzyć, że czyny muszą żyć własnym życiem. Takie „dzieła” – uwolnione od ojców-autorów – będą miały własnych odbiorców, możliwe, że spotkają też na swej drodze jakiegoś Zoilę.",
    "Je ne t'aime pas.":
        "Nie kocham Cię.",
    "Tu ne me connais pas.":
        "Nie znasz mnie.",
    "Nous n'avons rien en commun.":
        "Nic nas nie łączy.",
    "Je ne veux rien de toi.":
        "Nic od Ciebie nie chcę.",
    "Tu n'es pas un modèle pour moi.":
        "Nie jesteś dla mnie żadnym wzorem.",
    "Je veux voler de mes propres ailes.":
        "Chcę iść własną drogą.",
    "Bientot je partirai.":
        "Niedługo się wyprowadzę.",

    "Si ce n'était qu'elle, je pourrais l'accepter.":
        "Jakoś radzę sobie z tym, co między mną a żoną.",
    "Mais mon fils dispose des mêmes armes.":
        "Ale mój syn...",
    "Il voudrait mon avis sur sa rédaction. ":
        "Chce, żebym przeczytał jego wypracowanie,",
    "Mais je ne parviens pas à me concentrer sur le texte.":
        "lecz ja nie potrafię skupić się na poszczególnych frazach.",
    "Étrange impression de ne pouvoir lire qu'entre les lignes…":
        "Jak to się stało, że czytam już tylko między wierszami?",


    "Suis-je si peu présent ?":
        "Jestem tak nieistotny?",
    "Si modelable ?":
        "Tak łatwo deformowalny?",
    "Ma propre image semble me fuir.":
        "Umyka mi mój własny obraz.",
    "Elle m'échappe.":
        "Upada.",
    "Je me sens manipulé.":
        "Czuję się zmanipulowany.",



    "Il est temps de reprendre le contrôle.":
        "Czas odzyskać kontrolę.",
    "Arrêter de tourner en rond.":
        "Przestać kręcić się w kółko.",

    "Retrouver une direction.":
        "Wiedzieć, dokąd zmierzam.",
    "Plier le cours des événements.":
        "Nadawać bieg zdarzeniom.",
    "Donner un sens à mes actions.":
        "Przypisać znaczenie moim działaniom.",


    "Je fais tout pour maîtriser de nouveau le cours de ma vie.":
        "Robię, co w mojej mocy, by znów wziąć życie w swoje ręce.",
    "Je choisis.":
        "Dokonuję wyborów.",
    "Mes émotions.":
        "Mam władzę nad emocjami",
    "Le sens à donner aux choses.":
        "i znaczeniem rzeczy.",
    "Enfin, je me suis repr":
        "W końcu nad wszystkim panuj",

    "fin":
        "Koniec",

    // fichiers sonores
    // chp1

    "assets/sound/chapitre1/BienvenueAppuyez.mp3":
        "assets/sound/chapitre1/BienvenueAppuyez-pl.mp3",
    "assets/sound/chapitre1/BipEtBravo.mp3":
        "assets/sound/chapitre1/BipEtBravo-pl.mp3",
    "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous.mp3":
        "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous-pl.mp3",
    "assets/sound/chapitre1/BravoVotreRendezVousEstArrive.mp3":
        "assets/sound/chapitre1/BravoVotreRendezVousEstArrive-pl.mp3",

    // chp2
    "assets/sound/chapitre2/julien1.mp3":
        "assets/sound/chapitre2/PL_man_1.mp3",
    "assets/sound/chapitre2/julien2.mp3":
        "assets/sound/chapitre2/PL_man_2.mp3",
    "assets/sound/chapitre2/julien3.mp3":
        "assets/sound/chapitre2/PL_man_3.mp3",
    "assets/sound/chapitre2/julien4.mp3":
        "assets/sound/chapitre2/PL_man_4.mp3",
    "assets/sound/chapitre2/julien5.mp3":
        "assets/sound/chapitre2/PL_man_5.mp3",
    "assets/sound/chapitre2/julien6.mp3":
        "assets/sound/chapitre2/PL_man_6.mp3",
    "assets/sound/chapitre2/julien7.mp3":
        "assets/sound/chapitre2/PL_man_7.mp3",
    "assets/sound/chapitre2/julien8.mp3":
        "assets/sound/chapitre2/PL_man_8.mp3",
    "assets/sound/chapitre2/julien9.mp3":
        "assets/sound/chapitre2/PL_man_9.mp3",

    // chp4


    "assets/sound/chapitre4/maximilienEssay.mp3":
        "assets/sound/chapitre4/maximilienEssay-pl.mp3",
    "assets/sound/chapitre4/max-jenetaimepas.mp3":
        "assets/sound/chapitre4/jenetaimepas-pl.mp3",
    "assets/sound/chapitre4/max-tunemeconnaispas.mp3":
        "assets/sound/chapitre4/tunemeconnaispas-pl.mp3",
    "assets/sound/chapitre4/max-nousnavonsrienencommun.mp3":
        "assets/sound/chapitre4/nousnavonsrienencommun-pl.mp3",
    "assets/sound/chapitre4/max-jeneveuxriendetoi.mp3":
        "assets/sound/chapitre4/jeneveuxriendetoi-pl.mp3",
    "assets/sound/chapitre4/max-tunespasunmodelepourmoi.mp3":
        "assets/sound/chapitre4/tunespasunmodelepourmoi-pl.mp3",
    "assets/sound/chapitre4/max-jeveuxvolerdemespropresailes.mp3":
        "assets/sound/chapitre4/jeveuxvolerdemespropresailes-pl.mp3",
    "assets/sound/chapitre4/max-bientotjepartirai.mp3":
        "assets/sound/chapitre4/bientotjepartirai-pl.mp3",




};
