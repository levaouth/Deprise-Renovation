// lang-en.ts

export const LANG_FR_NAME = 'fr';

export const LANG_FR_TRANS = {
	'hello world': 'bonjour Monde',
	"Accueil":
    "Accueil",
    "Présentation":
    "Présentation",
    "Crédits":
    "Crédits",
    "Prix":
    "Prix",
    "Scènes":
    "Scènes",
    "Scène":
    "Scène",
    "Version Flash":
    "Version Flash",
    "Version application smartphone":
    "Version application smartphone",
    "Version Angular":
    "Version JavaScript",
    "Réalisation":
    "Réalisation",
    "Traduction":
    "Traduction",
    "Voix":
    "Voix",
    "Épouse":
    "Épouse",
    "Musique":
    "Musique",
    "Vidéo":
    "Vidéo",
    "Téléphone":
    "Téléphone",
    "Narrateur":
    "Narrateur",
    "ado":
    "Adolescent",
    "Ambiance":
    "Ambiance",
    "Manipulation":
    "Manipulation",
    "Saxophone":
    "Saxophone",
    "Guitare électrique":
    "Guitare électrique",
    "Batterie/Montage":
    "Batterie/Montage",
    "Extrait du disque":
    "Extrait du disque",

    "Déprise a gagné le new Media Writing Prize 2011":
    "Déprise a gagné le new Media Writing Prize 2011",
    "Voir le site":
    "Voir le site",

    "Déprise est également disponible sous forme d'application mobile." :
    "Déprise est également disponible sous forme d'application mobile." ,
    "Application sur Google Play":
    "Application sur Google Play",
    "Application sur l'Apple Store":
    "Application sur l'Apple Store",

    "Toute ma vie, j'ai cru avoir devant moi un champ des possibles infini.": "Toute ma vie, j'ai cru avoir devant moi un champ des possibles infini.",
	"déprise":"déprise",
	"Déprise": "Déprise",
	"Déprise est une création sur les notions de prise et de contrôle. Quand a-t-on l'impression d'être en situation de prise ou de perte de prise dans la vie? Six scènes racontent l'histoire d'un personnage en pleine déprise. Parallèlement, ce jeu sur prise et déprise permet de mettre en scène la situation du lecteur d'une oeuvre interactive.": "Déprise est une création sur les notions de prise et de contrôle. Quand a-t-on l'impression d'être en situation de prise ou de perte de prise dans la vie ? Six scènes racontent l'histoire d'un personnage en pleine déprise. Parallèlement, ce jeu sur prise et déprise permet de mettre en scène la situation du lecteur d'une oeuvre interactive.",
	"Avez-vous pensé à allumer vos enceintes ?":"Avez-vous pensé à allumer vos enceintes ?",
  "Appuyez sur n'importe quelle touche":"Appuyez sur n'importe quelle touche",
		"\"L'univers entier m'appartient\", pensais-je.":"\"L'univers entier m'appartient\", pensais-je.",
		"J'ai le choix.":"J'ai le choix.",
		"Je suis maître de mon destin.":
		"Je suis maître de mon destin.",
		"Je peux prendre ce qui me plaît.":
		"Je peux prendre ce qui me plaît.",
		"Je deviendrai ce que je veux.":
		"Je deviendrai ce que je veux.",
		"J'ai tracé mon propre chemin.":
		"J'ai tracé mon propre chemin.",
		"J'ai parcouru de magnifiques paysages.":
		"J'ai parcouru de magnifiques paysages.",
		"Quoi de plus naturel, je les avais choisis.":
		"Quoi de plus naturel, je les avais choisis.",
		"Mais depuis un moment, j'ai des doutes.":
		"Mais depuis un moment, j'ai des doutes.",
		"Comment avoir prise sur ce qui m'arrive ?":
		"Comment avoir prise sur ce qui m'arrive ?",
		"Tout s'échappe.":
		"Tout s'échappe.",
		"Me glisse entre les doigts.":
		"Me glisse entre les doigts.",
		"Les objets, les personnes.":
		"Les objets, les personnes.",
		"J'ai l'impression de ne plus rien contrôler.":
		"J'ai l'impression de ne plus rien contrôler.",
		"Depuis quelques temps maintenant,":
		"Depuis quelques temps maintenant,",
		"Je n'attends qu'une chose.":
		"Je n'attends qu'une chose.",
		"La suite.":
		"La suite.",

	  "Et le rendez-vous est arrivé.":
	  "Et le rendez-vous est arrivé.",

		"Mais le rendez-vous était biaisé.":
		"Mais le rendez-vous était biaisé.",
		"Je ne m'en suis rendu compte que beaucoup plus tard.":
		"Je ne m'en suis rendu compte que beaucoup plus tard.",
		"La femme en face de moi, qui paraissait si parfaite, me laissait bouche bée.":
		"La femme en face de moi, qui paraissait si parfaite, me laissait bouche bée.",
		"Impossible de prononcer quelque chose de cohérent.":
		"Impossible de prononcer quelque chose de cohérent.",
		"Sa présence me bouleversait...":
		"Sa présence me bouleversait...",
		"Il fallait que je pose des questions pour la mettre à jour.":
		"Il fallait que je pose des questions pour la mettre à jour.",
		"Sans que je m'en aperçoive, cette inconnue devenait ma femme.":
		"Sans que je m'en aperçoive, cette inconnue devenait ma femme.",
		"On a tout partagé.":
		"On a tout partagé.",
		"Mais jamais je ne suis parvenu à vraiment la connaître.":
		"Mais jamais je ne suis parvenu à vraiment la connaître.",
		"Aujourd'hui encore je me pose des questions.":
		"Aujourd'hui encore je me pose des questions.",
		"Qui d'elle ou moi suit l'autre ?":
		"Qui d'elle ou moi suit l'autre ?",
		"Quand je l'aime, elle me sème.":
		"Quand je l'aime, elle me sème.",

		"Qui êtes-vous ?":
		"Qui êtes-vous ?",
		"Vous aimez...":
		"Vous aimez...",
		"Que pensez-vous...":
		"Que pensez-vous...",
		"D'où venez-vous ?":
		"D'où venez-vous ?",
		"Où allez-vous ?":
		"Où allez-vous ?",
		"Vous pensez quoi de...":
		"Vous pensez quoi de...",
		"Vous habitez la région depuis longtemps ?":
		"Vous habitez la région depuis longtemps ?",
    "Vous évitez la légion depuis longtemps ?":
    "Vous évitez la légion depuis longtemps ?",
		"Et vous travaillez dans quoi ?":
		"Et vous travaillez dans quoi ?",
    "Et vous travaillez l'envoi ?":
    "Et vous travaillez l'envoi ?",
		"Je vous trouve vraiment très jolie !":
		"Je vous trouve vraiment très jolie !",
    "Chevaux, brousse, bêlement... près jolis":
    "Chevaux, brousse, bêlement... prés jolis",
		"J'ai l'impression qu'on a beaucoup de points communs":
		"J'ai l'impression qu'on a beaucoup de points communs",
    "Chemins pression en Allemagne point comme un...":
    "J'ai la pression et la pinte en commun",
		"Vous avez des yeux somptueux":
		"Vous avez des yeux somptueux",
    "Vous avouez des notions de tueurs":
    "Vous avez des désirs soyeux",
		"Vous venez souvent ici ?":
		"Vous venez souvent ici ?",
    "Vous avez l'absent acquis":
    "Vous venez sous le vent aussi ?",
		"Puis-je vous offrir un autre verre ?":
		"Puis-je vous offrir un autre verre ?",
    "Pigeon ouïr un Notre Père ?":
    "Puis-je ouïr un Notre Père ?",
		"J'aime votre façon de sourire":
		"J'aime votre façon de sourire",
    "Gêne, votre garçon mourir":
    "Gêne, votre face a des soupirs",
		"Vous voulez marcher un peu ?":
		"Vous voulez marcher un peu ?",
    "Nouveaux-nés barges et il pleut.":
    "Les nouveau-nés marchent et il pleut.",

		"Vingt ans se sont écoulés depuis notre rencontre.":
		"Vingt ans se sont écoulés depuis notre rencontre.",
		"Ce matin, je me perds dans un mot qu'elle m'a laissé.":
		"Ce matin, je me perds dans un mot qu'elle m'a laissé.",
		"Tout se brouille dans mon esprit.":
		"Tout se brouille dans mon esprit.",
		"Je ne sais comment l'interpréter.":
		"Je ne sais comment l'interpréter.",
		"Mot d'amour ou de rupture ?":
		"Mot d'amour ou de rupture ?",
		"Le fait-elle exprès ?":
		"Que puis-je faire ?",


		"Je sais que c'est pour toi un choc":
		"Je sais que c'est pour toi un choc",
		"Je n'ai que de l'amour pour toi":
		"Je n'ai que de l'amour pour toi",
		"Est un mensonge, et":
		"Est un mensonge, et",
		"Dans un couple, il y en a un qui souffre et un qui s'ennuie":
		"\"Dans un couple, il y en a un qui souffre et un qui s'ennuie\"",
		"Je veux que tous nos amis sachent que":
		"Je veux que tous nos amis sachent que",
		"Je ne veux pas rester avec toi,":
		"Je ne veux pas rester avec toi,",
		"Depuis le premier jour, je ne sais pas comment tu peux croire que":
		"Depuis le premier jour, je ne sais pas comment tu peux croire que",
		"Je t'aime":
		"Je t'aime",
		"Mon amour":
		"Mon amour",
		"A disparu":
		"A disparu",
		"L'indifférence":
		"L'indifférence",
		"Est plus vivace que jamais":
		"Est plus vivace que jamais",
		"Le charme de notre rencontre":
		"Le charme de notre rencontre",
		"S'est dissipé à présent":
		"S'est dissipé à présent",
		"Et le moindre malentendu":
		"Et le moindre malentendu",
		"A vaincu":
		"A vaincu",
		"Notre amour":
		"Notre amour",
	  "Je n'ai pas de héros. D'aussi loin qu'il m'en souvienne, et même après une longue réflexion, je n'ai jamais eu de héros. La figure du héros ne me séduit pas. Sans doute parce que je ne préfère pas une qualité à une autre, une valeur morale à une autre. Les héros, je les connais, je les reconnais, mais je n'éprouve ni adoration, ni idolâtrie pour eux. À dire vrai, le fanatisme me rend fou. Si l'on considère que le héros obtient son titre par ses actions, son statut est donc une forme de récompense pour la prouesse, la hardiesse, l'originalité. Mais que reste-t-il à la personne, une fois l'acte terminé ? Rien, sinon le titre. On peut présumer que le héros retire de son action une aura : l'action brille à travers lui. J'ai tendance à croire, que l'oeuvre — l'action selon les domaines — doit quitter son créateur pour pouvoir vivre pleinement. Les enfants-livres des auteurs vont eux-mêmes tracer leur chemin, se cognant à l'occasion à quelques Zoïles.":
	  "Je n'ai pas de héros. D'aussi loin qu'il m'en souvienne, et même après une longue réflexion, je n'ai jamais eu de héros. La figure du héros ne me séduit pas. Sans doute parce que je ne préfère pas une qualité à une autre, une valeur morale à une autre. Les héros, je les connais, je les reconnais, mais je n'éprouve ni adoration, ni idolâtrie pour eux. À vrai dire, le fanatisme me rend fou. Si l'on considère que le héros obtient son titre par ses actions, son statut est donc une forme de récompense pour la prouesse, la hardiesse, l'originalité. Mais que reste-t-il à la personne, une fois l'acte terminé ? Rien, sinon le titre. On peut présumer que le héros retire de son action une aura : l'action brille à travers lui. J'ai tendance à croire, que l'oeuvre — l'action selon les domaines — doit quitter son créateur pour pouvoir vivre pleinement. Les enfants-livres des auteurs vont eux-mêmes tracer leur chemin, se cognant à l'occasion à quelques Zoïles.",
		"Je ne t'aime pas.":
		"Je ne t'aime pas.",
		"Tu ne me connais pas.":
		"Tu ne me connais pas.",
		"Nous n'avons rien en commun.":
		"Nous n'avons rien en commun.",
		"Je ne veux rien de toi.":
		"Je ne veux rien de toi.",
		"Tu n'es pas un modèle pour moi.":
		"Tu n'es pas un modèle pour moi.",
		"Je veux voler de mes propres ailes.":
		"Je veux voler de mes propres ailes.",
		"Bientot je partirai.":
		"Bientôt je partirai.",

		"Si ce n'était qu'elle, je pourrais l'accepter.":
		"Si ce n'était qu'elle, je pourrais l'accepter.",
		"Mais mon fils dispose des mêmes armes.":
		"Mais mon fils dispose des mêmes armes.",
		"Il voudrait mon avis sur sa rédaction. ":
		"Il voudrait mon avis sur sa rédaction. ",
		"Mais je ne parviens pas à me concentrer sur le texte.":
		"Mais je ne parviens pas à me concentrer sur le texte.",
		"Étrange impression de ne pouvoir lire qu'entre les lignes…":
		"Étrange impression de ne pouvoir lire qu'entre les lignes…",


		"Suis-je si peu présent ?":
		"Suis-je si peu présent ?",
		"Si modelable ?":
		"Si modelable ?",
		"Ma propre image semble me fuir.":
		"Ma propre image semble me fuir.",
		"Elle m'échappe.":
		"Elle m'échappe.",
		"Je me sens manipulé.":
		"Je me sens manipulé.",



		"Il est temps de reprendre le contrôle.":
		"Il est temps de reprendre le contrôle.",
		"Arrêter de tourner en rond.":
		"Arrêter de tourner en rond.",

		"Retrouver une direction.":
		"Retrouver une direction.",
		"Plier le cours des événements.":
		"Plier le cours des événements.",
		"Donner un sens à mes actions.":
		"Donner un sens à mes actions.",


		"Je fais tout pour maîtriser de nouveau le cours de ma vie.":
		"Je fais tout pour maîtriser de nouveau le cours de ma vie.",
		"Je choisis.":
		"Je choisis.",
		"Mes émotions.":
		"Mes émotions.",
		"Le sens à donner aux choses.":
		"Le sens à donner aux choses.",
		"Enfin, je me suis repr":
		"Enfin, je me suis repr",

    "fin":
    "fin"

};
