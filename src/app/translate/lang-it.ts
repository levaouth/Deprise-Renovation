// lang-it.ts

export const LANG_IT_NAME = 'it';

export const LANG_IT_TRANS = {
    "Toute ma vie, j'ai cru avoir devant moi un champ des possibles infini.":
    "Per tutta la vita ho creduto di avere, davanti a me un campo di possibili infinite.",
  "Accueil":
  "Informazioni",
  "Déprise": "Perdersi",
  "déprise": "perdersi",
  "Présentation":
  "Prezentatione",
  "Crédits":
  "Ringraziamenti",
  "Prix":
  "Premii",
  "Scènes":
  "Scene",
  "Scène":
  "Scena",
  "Version Flash":
  "Versione Flash",
  "Version application smartphone":
  "App for smartphones",
  "Version Angular":
  "Versione JavaScript",
  "Langues":
  "Lingue",
  "Réalisation":
  "Realizatione",
  "Traduction":
  "Traduzione",
  "Voix":
  "Voce",
  "Épouse":
  "Spoza",
  "Musique":
  "Musica",
  "Vidéo":
  "Video",
  "Téléphone":
  "Telefono",
  "Narrateur":
  "Narratore",
  "ado":
  "adolescente",
  "Ambiance":
  "Atmosfera",
  "Manipulation":
  "Manipolazione",
  "Saxophone":
  "Sassofono",
  "Guitare électrique":
  "Chitarra elettrica",
  "Batterie/Montage":
  "Batteria/Montaggio",
  "Extrait du disque":
  "Estratto del disco",

  "Déprise a gagné le prix New Media Writing Prize en 2011.":
  "Perdersi ha vinto il new Medid Wriging Prize 2011",
  "Voir le site":
  "Vedere il sito",

  "Déprise est également disponible sous forme d'application mobile." :
  "This creation is also available as an application for mobile devices:" ,
  "Application sur Google Play":
  "Google Play",
  "Application sur l'Apple Store":
  "App Store",

  "hello world":
  "bonjorno",

    "Déprise est une création sur les notions de prise et perte de contrôle. Quand a-t-on l'impression d'être en situation de prise ou de perte de prise dans la vie? Six scènes racontent l'histoire d'un personnage en pleine déprise. Parallèlement, ce jeu sur prise et déprise permet de mettre en scène la situation du lecteur d'une oeuvre interactive.":
        "Perdersi è un lavoro sulla perdita di controllo di sé. Cosa succede quando si ha l'impressione di avere il controllo della propria vita e poi di perdere questo controllo? Sei scene narrano la storia di un personaggio in piena crisi esistenziale. Parallelamente questo gioco di controllo/perdita di se stessi permette di mettere in scena la situazione del lettore di un'opera interattiva.",
	"Avez-vous pensé à allumer vos enceintes ?":
  "Ha acceso gli altoparlanti ?",
  "(Appuyez sur n'importe quelle touche pour continuer)": "Caricamento in corso",
		"\"L'univers entier m'appartient\", pensais-je.":
    "\"L'intero universo mi appartiene\", pensavo.",
		"J'ai le choix.":
    "Posso scegliere.",
		"Je suis maître de mon destin.":
    "Sono il padrone del mio destino.",
		"Je peux prendre ce qui me plaît.":
    "Posso avere tutto quello che desidero.",
		"Je deviendrai ce que je veux.":
    "Diventerò ciò che voglio io.",
		"J'ai tracé mon propre chemin.":
    "Ho tracciato il mio proprio destino.",
		"J'ai parcouru de magnifiques paysages.":
    "Ho attraversato paesaggi magnifici.",
		"Quoi de plus naturel, je les avais choisis.":
    " Naturalmente, li avevo scelti io.",
		"Mais depuis un moment, j'ai des doutes.":
    "Ma da qualche tempo, ho dei dubbi.",
		"Comment avoir prise sur ce qui m'arrive ?":
    " Come controllare quello che mi accade ?",
		"Tout s'échappe.":
    "Tutto mi sfugge.",
		"Me glisse entre les doigts.":
    "Mi scivola via tra le dita.",
		"Les objets, les personnes.":
    "Le cose, le persone.",
		"J'ai l'impression de ne plus rien contrôler.":
    "Ho come l'impressione di non controllare più nulla.",
		"Depuis quelques temps maintenant,":
    "Da qualche tempo ormai,",
		"Je n'attends qu'une chose.":
    "non aspetto che una cosa.",
		"La suite.":
    ".Il seguito.",

	  "Et le rendez-vous est arrivé.":
    "E l'appuntamento è arrivato.",


		"Mais le rendez-vous était biaisé.":
    "Ma l'appuntamento era illusorio.",
		"Je ne m'en suis rendu compte que beaucoup plus tard.":
    "Me ne sarei reso, conto solo molto più tardi.",
		"La femme en face de moi, qui paraissait si parfaite, me laissait bouche bée.":
    "La donna davanti a me, che sembrava essere così perfetta, mi lasciò senza parole.",
		"Impossible de prononcer quelque chose de cohérent.":
    "Impossibile pronunciare qualcosa di coerente.",
		"Sa présence me bouleversait...":
    "La sua presenza mi sconvolgeva...",
		"Il fallait que je pose des questions pour la mettre à jour.":
    "Bisognava che facessi delle domande per aggiornarla.",
		"Sans que je m'en aperçoive, cette inconnue devenait ma femme.":
    "Senza che me ne rendessi conto, quella sconosciuta sarebbe diventata mia moglie.",
		"On a tout partagé.":
    "Abbiamo  condiviso tutto.",
		"Mais jamais je ne suis parvenu à vraiment la connaître.":
    "Ma non sono mai riuscito a conoscerla veramente.",
		"Aujourd'hui encore je me pose des questions.":
    "Ancora oggi mi faccio delle domande.",
		"Qui d'elle ou moi suit l'autre ?":
    "Chi tra me e lei segue l'altro ?",
		"Quand je l'aime, elle me sème.":
    "Quando l'amo, lei mi semina.",

		"Qui êtes-vous ?":
    "Chi sei?",
		"Vous aimez...":
    "Ami...",
		"Que pensez-vous...":
    "Che pensi...",
		"D'où venez-vous ?":
    "Di dove sei ?",
		"Où allez-vous ?":
    "Dove vai ?",
		"Vous pensez quoi de...":
    "Cosa pensi di ciò.",
		"Vous habitez la région depuis longtemps ?":
    "Abiti in questa regione da molto ?",
    "Vous évitez la légion depuis longtemps ?":
    "Eviti la legione da molto ?",
		"Et vous travaillez dans quoi ?":
    "E che lavoro fai ?",
    "Et vous travaillez l'envoi ?":
    "E ancora lo fai ?",
		"Je vous trouve vraiment très jolie !":
    "Sei molto bella Cavalli",
    "Chevaux, brousse, bêlement... près jolis":
    "Cespuglio e pure una sella",
		"J'ai l'impression qu'on a beaucoup de points communs":
    "Ho l'impressione che abbiamo molti punti in comune",
    "Chemins pression en Allemagne point comme un...":
    "Mi fa impressione che in Germania non ci siano dune",
		"Vous avez des yeux somptueux":
    "Hai degli occhi deliziosi",
    "Vous avouez des notions de tueurs":
    "Hai dei modi rabbiosi",
		"Vous venez souvent ici ?":
    "Qui ci vieni spesso ?",
    "Vous avez l'absent acquis":
    "Hai comprato il lesso",
		"Puis-je vous offrir un autre verre ?":
    "Un altro drink te lo posso offrire ?",
    "Pigeon ouïr un Notre Père ?":
    "Il Padre Nostro rosso sentire ?",
		"J'aime votre façon de sourire":
    "Ho il tuo modo di sorridere",
    "Gêne, votre garçon mourir":
    "So il tuo ruolo nell’uccidere",
		"Vous voulez marcher un peu ?":
    "Ti va di camminare un po' ?",
    "Nouveaux-nés barges et il pleut.":
    "Ti va di scalciare, no ?",

		"Vingt ans se sont écoulés depuis notre rencontre.":
    "Son trascorsi venti anni dal nostro incontro.",
		"Ce matin, je me perds dans un mot qu'elle m'a laissé.":
    "Questa mattina, mi son perso in un biglietto che mi ha lasciato.",
		"Tout se brouille dans mon esprit.":
    "Tutto sfuma nella mia mente.",
		"Je ne sais comment l'interpréter.":
    "Non so come interpretarlo.",
		"Mot d'amour ou de rupture ?":
    "Biglietto di amore o di separazione ?",
		"Le fait-elle exprès ?":
    "L'avrà fatto apposta ?",






		"Je sais que c'est pour toi un choc":
    "Lo so che per te è uno choc",
		"Je n'ai que de l'amour pour toi":
    "\"Ho solo amore per te\"",
		"Est un mensonge, et":
    "È una bugia",
		"Dans un couple, il y en a un qui souffre et un qui s'ennuie":
    "\"In una coppia ce ne è uno che soffre e uno che si annoia\"",
		"Je veux que tous nos amis sachent que":
    "Voglio che tutti i nostri amici sappiano che",
		"Je ne veux pas rester avec toi,":
    "non voglio restare con te,",
		"Depuis le premier jour, je ne sais pas comment tu peux croire que":
    "Dal primo giorno non so come tu possa credere",
		"Je t'aime":
    "Che ti amo",
		"Mon amour":
    "Il mio amore",
		"A disparu":
    "È scomparso",
		"L'indifférence":
    "L'indifferenza",
		"Est plus vivace que jamais":
    "È più viva che mai",
		"Le charme de notre rencontre":
    "Lo charme del nostro incontro",
		"S'est dissipé à présent":
    "Si è dileguato adesso",
		"Et le moindre malentendu":
    "E il minimo malinteso",
		"A vaincu":
    "Ha vinto",
		"Notre amour":
    "Il nostro amore",


	  "Je n'ai pas de héros. D'aussi loin qu'il m'en souvienne, et même après une longue réflexion, je n'ai jamais eu de héros. La figure du héros ne me séduit pas. Sans doute parce que je ne préfère pas une qualité à une autre, une valeur morale à une autre. Les héros, je les connais, je les reconnais, mais je n'éprouve ni adoration, ni idolâtrie pour eux. À dire vrai, le fanatisme me rend fou. Si l'on considère que le héros obtient son titre par ses actions, son statut est donc une forme de récompense pour la prouesse, la hardiesse, l'originalité. Mais que reste-t-il à la personne, une fois l'acte terminé ? Rien, sinon le titre. On peut présumer que le héros retire de son action une aura : l'action brille à travers lui. J'ai tendance à croire, que l'oeuvre — l'action selon les domaines — doit quitter son créateur pour pouvoir vivre pleinement. Les enfants-livres des auteurs vont eux-mêmes tracer leur chemin, se cognant à l'occasion à quelques Zoïles.":
    "Non ho eroi. Per quanto posso ricordare, anche dopo lunga riflessione, non ho mai avuto eroi io. La figura dell'eroe non fa per me. Senza dubbio perché non preferisco una qualità ad un altra, un valore morale ad un altro. Gli eroi li conosco, li riconosco, ma non provo né adorazione né idolatria per loro.  A dire il vero il bigottismo mi fa impazzire. Se si considera che l'eroe ottiene il suo titolo dalle sue azioni, il suo status una forma di ricompensa per l'abilità, il coraggio e l'originalità. Ma cosa resta alla persona una volta che il gesto compiuto?? Nulla, se non il titolo. Presumibilmente, l'eroe ottiene dalle sue azioni un'aura: l'azione risplende attraverso lui.  Io tendo a credere che l'opera in azione secondo diversi campi deve lasciare il suo creatore per poter vivere pienamente.  I libri-bambini degli autori tracceranno loro stessi il proprio cammino sbattendo a l'occasione in qualche Zoilo.",
		"Je ne t'aime pas.":
    "Non ti amo.",
		"Tu ne me connais pas.":
    "tu non mi conosci.",
		"Nous n'avons rien en commun.":
    "non abbiamo niente in comune.",
		"Je ne veux rien de toi.":
    "non voglio niente da te.",
		"Tu n'es pas un modèle pour moi.":
    "Tu non sei un modello per me.",
		"Je veux voler de mes propres ailes.":
    "Voglio volare con le mie proprie ali.",
		"Bientot je partirai.":
    "presto partiro.",

		"Si ce n'était qu'elle, je pourrais l'accepter.":
    "Se non fosse che per lei, potrei accettarlo.",
		"Mais mon fils dispose des mêmes armes.":
    "Ma mio figlio ha le mie stesse braccia.",
		"Il voudrait mon avis sur sa rédaction. ":
    "Vorrebbe un mio consiglio su un suo tema.",
		"Mais je ne parviens pas à me concentrer sur le texte.":
    "Ma non riesco a concentrarmi sul testo",
		"Étrange impression de ne pouvoir lire qu'entre les lignes…":
    "È strana impressione il non poter leggere che le linee.",


		"Suis-je si peu présent ?":
    " Sono così poco presente ?",
		"Si modelable ?":
    "Così modellabile ?",
		"Ma propre image semble me fuir.":
    "La mia propria immagine sembra sfuggirmi.",
		"Elle m'échappe.":
    "Mi sfugge.",
		"Je me sens manipulé.":
    "Mi sento manipolato.",



		"Il est temps de reprendre le contrôle.":
    "é giunto il momento di riprendere il controllo.",
		"Arrêter de tourner en rond.":
    "Smettere di girare intorno.",
		"Retrouver une direction.":
    "Ritrovare una direzione.",
		"Plier le cours des événements.":
    "Piegare il corso degli eventi.",
		"Donner un sens à mes actions.":
    "Dare un senso alle mie azioni.",








		"Je fais tout pour maîtriser de nouveau le cours de ma vie.":
    "Faccio di tutto per riprendere il controllo della mia vita.",
		"Je choisis.":
    "Scelgo.",
		"Mes émotions.":
    "Le mie emozioni.",
		"Le sens à donner aux choses.":
    "Il senso da dare alle cose.",
		"Enfin, je me suis repr":
    "Finalmente, mi sono ripr",

    "fin":
    "fin",

// fichiers sonores
// chp1

    "assets/sound/chapitre1/BienvenueAppuyez.mp3":
    "assets/sound/chapitre1/BienvenueAppuyez-it.mp3",
    "assets/sound/chapitre1/BipEtBravo.mp3":
    "assets/sound/chapitre1/BipEtBravo-it.mp3",
    "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous.mp3":
    "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous-it.mp3",
    "assets/sound/chapitre1/BravoVotreRendezVousEstArrive.mp3":
    "assets/sound/chapitre1/BravoVotreRendezVousEstArrive-it.mp3",

// chp2
    "assets/sound/chapitre2/julien1.mp3":
    "assets/sound/chapitre2/IT_man_1.mp3",
    "assets/sound/chapitre2/julien2.mp3":
    "assets/sound/chapitre2/IT_man_2.mp3",
    "assets/sound/chapitre2/julien3.mp3":
    "assets/sound/chapitre2/IT_man_3.mp3",
    "assets/sound/chapitre2/julien4.mp3":
    "assets/sound/chapitre2/IT_man_4.mp3",
    "assets/sound/chapitre2/julien5.mp3":
    "assets/sound/chapitre2/IT_man_5.mp3",
    "assets/sound/chapitre2/julien6.mp3":
    "assets/sound/chapitre2/IT_man_6.mp3",
    "assets/sound/chapitre2/julien7.mp3":
    "assets/sound/chapitre2/IT_man_7.mp3",
    "assets/sound/chapitre2/julien8.mp3":
    "assets/sound/chapitre2/IT_man_8.mp3",
    "assets/sound/chapitre2/julien9.mp3":
    "assets/sound/chapitre2/IT_man_9.mp3",

// chp4


    "assets/sound/chapitre4/maximilienEssay.mp3":
    "assets/sound/chapitre4/maximilienEssay-it.mp3",
    "assets/sound/chapitre4/max-jenetaimepas.mp3":
    "assets/sound/chapitre4/jenetaimepas-it.mp3",
    "assets/sound/chapitre4/max-tunemeconnaispas.mp3":
    "assets/sound/chapitre4/tunemeconnaispas-it.mp3",
    "assets/sound/chapitre4/max-nousnavonsrienencommun.mp3":
    "assets/sound/chapitre4/nousnavonsrienencommun-it.mp3",
    "assets/sound/chapitre4/max-jeneveuxriendetoi.mp3":
    "assets/sound/chapitre4/jeneveuxriendetoi-it.mp3",
    "assets/sound/chapitre4/max-tunespasunmodelepourmoi.mp3":
    "assets/sound/chapitre4/tunespasunmodelepourmoi-it.mp3",
    "assets/sound/chapitre4/max-jeveuxvolerdemespropresailes.mp3":
    "assets/sound/chapitre4/jeveuxvolerdemespropresailes-it.mp3",
    "assets/sound/chapitre4/max-bientotjepartirai.mp3":
    "assets/sound/chapitre4/bientotjepartirai-it.mp3",




};
