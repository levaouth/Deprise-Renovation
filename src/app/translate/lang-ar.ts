// lang-ar.ts

export const LANG_AR_NAME = 'ar';

export const LANG_AR_TRANS = {

  "Accueil":
  "الرئيسية",
  "Présentation":
  "تقديم",
  "Crédits":
  "الفريق",
  "Prix":
  "المكافآت",
  "Scènes":
  "المشاهد",
  "Scène":
  "المشهد",
  "Version Flash":
  "نسخة فلاش",
  "Version application smartphone":
  "تطبيق للهاتف الذكي",
  "Version Angular":
  "نسخة جافا سكريبت",
  "Langues":
  "اللغات",
  "Réalisation":
  "إخراج",
  "Traduction":
  "ترجمة",
  "Voix":
  "أصوات",
  "Épouse":
  "الزوجة",
  "Musique":
  "موسيقى",
  "Vidéo":
  "فيديو",
  "Téléphone":
  "الهاتف",
  "Narrateur":
  "الراوي",
  "ado":
  "المراهق",
  "Ambiance":
  "الجو",
  "Manipulation":
  "المعالجة",
  "Saxophone":
  "ساكسفون",
  "Guitare électrique":
  "جيتار كهربائي",
  "Batterie/Montage":
  "طبول/مونتاج",
  "Extrait du disque":
  "مقطع من القرص",

  "Déprise est également disponible sous forme d'application mobile." :
  ":هذا الإنشاء متاح أيضًا كتطبيق للأجهزة المحمولة" ,
  "Application sur Google Play":
  "تطبيقات جوجل",
  "Application sur l'Apple Store":
  "تطبيقات ابل ستور",

  "Déprise a gagné le prix New Media Writing Prize en 2011.":
  "فاز هذا الإنشاء بـ New Media Writing Prize 2011.  ",
  "Voir le site":
  "التطلع على الموقع",

    'hello world': 'hello world',
    "Toute ma vie, j'ai cru avoir devant moi un champ des possibles infini.": "طيلة حياتي ، اعتقدت أنه كان أمامي مجال من الاحتمالات اللانهائية.",
	"Déprise":"فقدان السيطرة",
    "Déprise est une création sur les notions de prise et perte de contrôle. Quand a-t-on l'impression d'être en situation de prise ou de perte de prise dans la vie? Six scènes racontent l'histoire d'un personnage en pleine déprise. Parallèlement, ce jeu sur prise et déprise permet de mettre en scène la situation du lecteur d'une oeuvre interactive.":
        "فقدان السيطرة هو إنشاء حول مفاهيم السيطرة والتحكم. متى يشعر المرء أنه في وضع السيطرة أو فقدان السيطرة على الحياة؟ ست مشاهد تروي قصة شخصية في حالة فقدان السيطرة. توازيا مع ذلك ، تسمح هذه اللعبة حول السيطرة و فقدان السيطرة بعرض وضع قارئ العمل التفاعلي.",
	"Avez-vous pensé à allumer vos enceintes ?":"هل فكرت في تشغيل مكبرات صوت كمبيوترك؟",
  "(Appuyez sur n'importe quelle touche pour continuer)":"(اضغط على أي مفتاح للمتابعة)",
		"\"L'univers entier m'appartient\", pensais-je.":"\"الكون كله ملك لي\", فكرت.",
		"J'ai le choix.":"الخيار عندي.",

		"Je suis maître de mon destin.":
		"أنا سيد مصيري.",
		"Je peux prendre ce qui me plaît.":
		"يمكنني أخذ كل ما يعجبني.",
		"Je deviendrai ce que je veux.":
		"سأصبح كل ما أريد.",
		"J'ai tracé mon propre chemin.":
		"رسمت طريقي الخاص.",
		"J'ai parcouru de magnifiques paysages.":
		"جبت مناظر خلابة.",
		"Quoi de plus naturel, je les avais choisis.":
		"هل من شيء غريب، لقد أخترتهم.",
		"Mais depuis un moment, j'ai des doutes.":
		"ولكن منذ مدة، لدي شكوك.",
		"Comment avoir prise sur ce qui m'arrive ?":
		"كيف يمكنني السيطرة على ما يحدث لي؟",
		"Tout s'échappe.":
		"كل شيء يفلت مني.",
		"Me glisse entre les doigts.":
		"ينزلق بين أصابعي.",
		"Les objets, les personnes.":
		"الأشياء، الأشخاص.",
		"J'ai l'impression de ne plus rien contrôler.":
		"أشعر أنني لم أعد أتحكم في شيء.",
		"Depuis quelques temps maintenant,":
		"منذ وقت الآن،",
		"Je n'attends qu'une chose.":
		"أنتظر شيء واحدا فقط.",
		"La suite.":
		"ما الذي سيلي.",

	  "Et le rendez-vous est arrivé.":
    "وقد وصل الموعد.",

		"Mais le rendez-vous était biaisé.":
		"لكن الموعد كان متحيزا.",
		"Je ne m'en suis rendu compte que beaucoup plus tard.":
		"تيقنت من ذلك إلا بعد وقت طويل.",
		"La femme en face de moi, qui paraissait si parfaite, me laissait bouche bée.":
		"المرأة التي أمامي، والتي بدت مثالية، تركتني فاغر الفم.",
		"Impossible de prononcer quelque chose de cohérent.":
		"استحال مني نطق شيء متناسق.",
		"Sa présence me bouleversait...":
		"حضورها يثير مشاعري...",
		"Il fallait que je pose des questions pour la mettre à jour.":
		"كان علي أن أطرح أسئلة لأكشفها.",
		"Sans que je m'en aperçoive, cette inconnue devenait ma femme.":
		"بدون أن ألاحظ، أصبحت هذه المجهولة زوجتي.",
		"On a tout partagé.":
		"تشاركنا في كل شيء.",
		"Mais jamais je ne suis parvenu à vraiment la connaître.":
		"لكنني لم أستطع أبدا أن أعرفها حقا.",
		"Aujourd'hui encore je me pose des questions.":
		"إلى يومنا هذا وأنا أتساءل.",
		"Qui d'elle ou moi suit l'autre ?":
		"من فينا يتبع الآخر؟",
		"Quand je l'aime, elle me sème.":
		"عندما أحبها، تفقدني.",

		"Qui êtes-vous ?":
		"من أنت؟",
		"Vous aimez...":
		"هل تحبين...",
		"Que pensez-vous...":
		"ما رأيك...",
		"D'où venez-vous ?":
		"من أين أتيتي؟",
		"Où allez-vous ?":
		"أين أنت ذاهبة؟",
		"Vous pensez quoi de...":
		"ما رأيك في...",


		"Vous habitez la région depuis longtemps ?":
    "هل تقطنين في المنطقة منذ فترة طويلة؟",
    "Vous évitez la légion depuis longtemps ?":
    "هل تهربين من الفلقة منذ فترة طويلة؟",
		"Et vous travaillez dans quoi ?":
		"وتعملين في أي مجال؟",
    "Et vous travaillez l'envoi ?":
    "وهل تعملين في الإرسال؟",
		"Je vous trouve vraiment très jolie !":
		"أجدك حقا جميلة جدا",
    "Chevaux, brousse, bêlement... près jolis":
    "أحصدك، حقلا ... جريدة حدا",
		"J'ai l'impression qu'on a beaucoup de points communs":
		"أشعر بأن لدينا الكثير من القواسم المشتركة",
    "Chemins pression en Allemagne point comme un...":
    "أشقر برلين كثيف من العواصف المنتشرة",
		"Vous avez des yeux somptueux":
    "لديك عيون رائعة",
    "Vous avouez des notions de tueurs":
    "لديك عيوب قاتلة",
		"Vous venez souvent ici ?":
		"أتأتين غالبا هنا؟",
    "Vous avez l'absent acquis":
    "أتملكين غائب لنا؟",
		"Puis-je vous offrir un autre verre ?":
		"تسمحين أن أقدم لك مشروبًا آخر؟",
    "Pigeon ouïr un Notre Père ?":
    "تسمعين عن أعلم أبا أبحر؟",
		"J'aime votre façon de sourire":
		"أحب طريقة ابتسامتك",
    "Gêne, votre garçon mourir":
    "أجيد سرقة ابن عمتك",
		"Vous voulez marcher un peu ?":
		"هل تريدين المشي قليلا؟",
    "Nouveaux-nés barges et il pleut.":
    "هل تمطرين بالقش ليلا؟",

		"Vingt ans se sont écoulés depuis notre rencontre.":
		"مرت عشرون سنة منذ لقائنا.",
		"Ce matin, je me perds dans un mot qu'elle m'a laissé.":
		"هذا الصباح، أنا ضائع في كلمة تركتها لي.",
		"Tout se brouille dans mon esprit.":
		"كل شيء مشوش في ذهني.",
		"Je ne sais comment l'interpréter.":
		"لا أدري كيفية تأويل ذلك.",
		"Mot d'amour ou de rupture ?":
		"كلمة حب أو إنهاء علاقة؟",
		"Le fait-elle exprès ?":
		"هل تفعل ذلك عن قصد؟",


		"Je sais que c'est pour toi un choc":
		"أعلم أنها صدمة لك.",
		"Je n'ai que de l'amour pour toi":
		"ليس لدي لك سوى الحب",
		"Est un mensonge, et":
		"هي كذبة، و",
		"Dans un couple, il y en a un qui souffre et un qui s'ennuie":
		"\"في الزوجين، هناك واحد يعاني والآخر يمل\"",
		"Je veux que tous nos amis sachent que":
		"أريد أن يعرف كل أصدقائنا أن",
		"Je ne veux pas rester avec toi,":
		"أني لا أريد أن أبقى معك،",
		"Depuis le premier jour, je ne sais pas comment tu peux croire que":
		"منذ اليوم الأول، لا أعلم كيف يمكنك تصديق أن",
		"Je t'aime":
		"أني أحبك",
		"Mon amour":
		"حبي",
		"A disparu":
		"اختفى",
		"L'indifférence":
		"اللامبالاة",
		"Est plus vivace que jamais":
		"هي أمتن أكثر من أي وقت مضى",
		"Le charme de notre rencontre":
		"سحر لقائنا",
		"S'est dissipé à présent":
		"قد زال الآن",
		"Et le moindre malentendu":
		"وأدنى سوء تفاهم",
		"A vaincu":
		"فاز",
		"Notre amour":
		"حبنا",
	  "Je n'ai pas de héros. D'aussi loin qu'il m'en souvienne, et même après une longue réflexion, je n'ai jamais eu de héros. La figure du héros ne me séduit pas. Sans doute parce que je ne préfère pas une qualité à une autre, une valeur morale à une autre. Les héros, je les connais, je les reconnais, mais je n'éprouve ni adoration, ni idolâtrie pour eux. À dire vrai, le fanatisme me rend fou. Si l'on considère que le héros obtient son titre par ses actions, son statut est donc une forme de récompense pour la prouesse, la hardiesse, l'originalité. Mais que reste-t-il à la personne, une fois l'acte terminé ? Rien, sinon le titre. On peut présumer que le héros retire de son action une aura : l'action brille à travers lui. J'ai tendance à croire, que l'oeuvre — l'action selon les domaines — doit quitter son créateur pour pouvoir vivre pleinement. Les enfants-livres des auteurs vont eux-mêmes tracer leur chemin, se cognant à l'occasion à quelques Zoïles.":
    "ليس لدي بطل. بقدر ما أستطيع أن أتذكر، وحتى بعد تفكير طويل، لم يكن لدي بطل. شخصية البطل لا تفتنني. ربما لأنني لا أفضل صفة عن أخرى، قيمة أخلاقية عن أخرى. الأبطال، أنا أعرفهم، أتعرف عليهم، لكنني لا أشعر بالعشق ولا بالحب الأعمى اتجاههم. لقول الحقيقة، يدفعني التعصب إلى الجنون. إذا اعتبرنا أن البطل يحصل على لقبه من خلال أفعاله، فإن وضعه هو شكل من أشكال المكافأة على البراعة والجرأة والأصالة. ولكن ماذا يبقى للشخص بمجرد انتهاء الفعل؟ لا شيء، باستثناء اللقب. يمكن افتراض أن البطل يتحصل عن هالة من فعله: العمل يتألق من خلاله. أميل إلى الاعتقاد بأن العمل - العمل وفقًا للمجالات - يجب أن يتخلى عن نشئه لكي يستطيع العيش بشكل كامل. الكتب-الأطفال للمؤلفين سيرسمون بأنفسهم طريقهم، مصطدمين أحيانًا ببعض الزوليين.",
		"Je ne t'aime pas.":
		"أنا لا أحبك.",
		"Tu ne me connais pas.":
		"أنت لا تعرفني.",
		"Nous n'avons rien en commun.":
		"ليس لدينا شيء مشترك.",
		"Je ne veux rien de toi.":
		"لا اريد شيءا منك.",
		"Tu n'es pas un modèle pour moi.":
		"أنت لست نموذجا لي.",
		"Je veux voler de mes propres ailes.":
		"أريد أن أطيربجناحي.",
		"Bientot je partirai.":
		"سأذهب قريبا.",
		"Si ce n'était qu'elle, je pourrais l'accepter.":
		"لو كانت هي فقط، لكان بإمكاني قبول ذلك.",
		"Mais mon fils dispose des mêmes armes.":
		"لكن ابني يملك نفس الأسلحة.",
		"Il voudrait mon avis sur sa rédaction. ":
		"يريد رأيي في كتابته.",
		"Mais je ne parviens pas à me concentrer sur le texte.":
		"لكنني لا أستطيع التركيز على النص.",
		"Étrange impression de ne pouvoir lire qu'entre les lignes…":
		"انطباع غريب بشأن القدرة على القراءة بين السطور فقط...",


		"Suis-je si peu présent ?":
		"هل حضوري ضعيف جدا؟",
		"Si modelable ?":
		"جد طيع؟",
		"Ma propre image semble me fuir.":
		"صورتي الخاصة تبدو تهرب مني.",
		"Elle m'échappe.":
		"تفلت مني.",
		"Je me sens manipulé.":
		"أشعر بالتلاعب بي.",



		"Il est temps de reprendre le contrôle.":
		"حان الوقت لاستعادة السيطرة.",

		"Retrouver une direction.":
		"والعثور على اتجاه.",
		"Plier le cours des événements.":
		"طي مجرى الأحداث.",
		"Donner un sens à mes actions.":
        "إعطاء معنى لأفعالي.",
        "Arrêter de tourner en rond.":
		"والتوقف عن الدوران في حلقة.",


		"Je fais tout pour maîtriser de nouveau le cours de ma vie.":
		"أفعل كل شيء للتحكم من جديد بمسار حياتي.",
		"Je choisis.":
		"اختار.",
		"Mes émotions.":
		"مشاعري.",
		"Le sens à donner aux choses.":
		"معنى لإعطائه للأشياء.",
		"Enfin, je me suis repr":
		"وفي الأخير، تحكمـ",

    "fin":
        "النهاية",
    
    // fichiers sonores
    // chp1

    "assets/sound/chapitre1/BienvenueAppuyez.mp3":
        "assets/sound/chapitre1/BienvenueAppuyez-ar.mp3",
    "assets/sound/chapitre1/BipEtBravo.mp3":
        "assets/sound/chapitre1/BipEtBravo-ar.mp3",
    "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous.mp3":
        "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous-ar.mp3",
    "assets/sound/chapitre1/BravoVotreRendezVousEstArrive.mp3":
        "assets/sound/chapitre1/BravoVotreRendezVousEstArrive-ar.mp3",

    // chp2
    "assets/sound/chapitre2/julien1.mp3":
        "assets/sound/chapitre2/AR_man_1.mp3",
    "assets/sound/chapitre2/julien2.mp3":
        "assets/sound/chapitre2/AR_man_2.mp3",
    "assets/sound/chapitre2/julien3.mp3":
        "assets/sound/chapitre2/AR_man_3.mp3",
    "assets/sound/chapitre2/julien4.mp3":
        "assets/sound/chapitre2/AR_man_4.mp3",
    "assets/sound/chapitre2/julien5.mp3":
        "assets/sound/chapitre2/AR_man_5.mp3",
    "assets/sound/chapitre2/julien6.mp3":
        "assets/sound/chapitre2/AR_man_6.mp3",
    "assets/sound/chapitre2/julien7.mp3":
        "assets/sound/chapitre2/AR_man_7.mp3",
    "assets/sound/chapitre2/julien8.mp3":
        "assets/sound/chapitre2/AR_man_8.mp3",
    "assets/sound/chapitre2/julien9.mp3":
        "assets/sound/chapitre2/AR_man_9.mp3",

    // chp4


    "assets/sound/chapitre4/maximilienEssay.mp3":
        "assets/sound/chapitre4/maximilienEssay-ar.mp3",
    "assets/sound/chapitre4/max-jenetaimepas.mp3":
        "assets/sound/chapitre4/jenetaimepas-ar.mp3",
    "assets/sound/chapitre4/max-tunemeconnaispas.mp3":
        "assets/sound/chapitre4/tunemeconnaispas-ar.mp3",
    "assets/sound/chapitre4/max-nousnavonsrienencommun.mp3":
        "assets/sound/chapitre4/nousnavonsrienencommun-ar.mp3",
    "assets/sound/chapitre4/max-jeneveuxriendetoi.mp3":
        "assets/sound/chapitre4/jeneveuxriendetoi-ar.mp3",
    "assets/sound/chapitre4/max-tunespasunmodelepourmoi.mp3":
        "assets/sound/chapitre4/tunespasunmodelepourmoi-ar.mp3",
    "assets/sound/chapitre4/max-jeveuxvolerdemespropresailes.mp3":
        "assets/sound/chapitre4/jeveuxvolerdemespropresailes-ar.mp3",
    "assets/sound/chapitre4/max-bientotjepartirai.mp3":
        "assets/sound/chapitre4/bientotjepartirai-ar.mp3"
};
