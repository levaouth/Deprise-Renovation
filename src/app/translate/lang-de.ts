// lang-de.ts

export const LANG_DE_NAME = 'de';

export const LANG_DE_TRANS = {

    "Accueil":
        "Startseite",
    "Présentation":
        "Präsentation",
    "Crédits":
        "Mitwirkende",
    "Prix":
        "Auszeichnungen",
    "Scènes":
        "Szenen",
    "Scène":
        "Szene",
    "Version Flash":
        "Flash-Version",
    "Version application smartphone":
        "App für Smartphones",
    "Version Angular":
        "JavaScript-Version",
    "Langues":
        "Sprachen",
    "Réalisation":
        "Realisierung",
    "Traduction":
        "Übersetzung",
    "Voix":
        "Stimme",
    "Épouse":
        "Ehefrau",
    "Musique":
        "Musik",
    "Vidéo":
        "Video",
    "Téléphone":
        "Telefon",
    "Narrateur":
        "Erzähler",
    "ado":
        "Teenager",
    "Ambiente":
        "Athmosphere",
    "Manipulation":
        "Manipulation",
    "Saxophone":
        "Saxophon",
    "Guitare électrique":
        "Elektrische Gitarre",
    "Batterie/Montage":
        "Batterie/Montage",
    "Extrait du disque":
        "Auszug aus der CD",

    "Déprise est également disponible sous forme d'application mobile.":
        "Diese Erstellung ist auch als Anwendung für mobile Geräte verfügbar:",
    "Application sur Google Play":
        "Google Play",
    "Application sur l'Apple Store":
        "App Store",

    "Déprise a gagné le prix New Media Writing Prize en 2011.":
        "Diese Kreation wurde mit dem New Media Writing Prize 2011 ausgezeichnet.",
    "Voir le site":
        "Siehe die Website",

    'hello world': 'hello world',
    "Toute ma vie, j'ai cru avoir devant moi un champ des possibles infini.": "Mein ganzes Leben lang dachte ich, dass ich unendliche Möglichkeiten hätte.",
    "Déprise": "Griffverlust",
    "déprise": "griffverlust",
    "Déprise est une création sur les notions de prise et perte de contrôle. Quand a-t-on l'impression d'être en situation de prise ou de perte de prise dans la vie? Six scènes racontent l'histoire d'un personnage en pleine déprise. Parallèlement, ce jeu sur prise et déprise permet de mettre en scène la situation du lecteur d'une oeuvre interactive.": "Griffverlust ist eine Kreation über die Konzepte des im Griff haben und der Kontrolle. Wann haben wir das Gefühl, dass wir uns in einer Situation befinden, in der wir unser Leben im Griff haben oder den Griff verlieren? Sechs Szenen erzählen die Geschichte einer Figur in einem Zustand des Griffverlustes. Gleichzeitig ermöglicht dieses Spiel über Griff und Griffverlust, die Situation des Lesers eines interaktiven Werkes zu inszenieren.",
    "Avez-vous pensé à allumer vos enceintes ?": "Ist der Ton Ihres Computers eingeschaltet?",
    "(Appuyez sur n'importe quelle touche pour continuer)": "(Drücken Sie auf eine beliebige Taste)",
    "\"L'univers entier m'appartient\", pensais-je.": "\"Das Universum gehört mir\", dachte ich.",
    "J'ai le choix.": "Ich habe die Wahl.",

    "Je suis maître de mon destin.":
        "Ich beherrsche mein Schicksal.",
    "Je peux prendre ce qui me plaît.":
        "Ich kann nehmen, was mir gefällt.",
    "Je deviendrai ce que je veux.":
        "Ich werde, was ich will.",
    "J'ai tracé mon propre chemin.":
        "Ich habe meinen eigenen Weg verfolgt.",
    "J'ai parcouru de magnifiques paysages.":
        "Ich habe wunderschöne Landschaften durchquert.",
    "Quoi de plus naturel, je les avais choisis.":
        "Kein Wunder, ich hatte sie gewählt.",
    "Mais depuis un moment, j'ai des doutes.":
        "Aber seit einiger Zeit habe ich Zweifel.",
    "Comment avoir prise sur ce qui m'arrive ?":
        "Wie kann ich Kontrolle über das Erlebte haben?",
    "Tout s'échappe.":
        "Alles entgleitet mir.",
    "Me glisse entre les doigts.":
        "Alles schlüpft mir durch die Finger.",
    "Les objets, les personnes.":
        "Die Gegenstände, die Leute.",
    "J'ai l'impression de ne plus rien contrôler.":
        "Ich habe das Gefühl, nichts mehr zu kontrollieren.",
    "Depuis quelques temps maintenant,":
        "Schon seit einiger Zeit,",
    "Je n'attends qu'une chose.":
        "Ich erwarte nur eins.",
    "La suite.":
        "Was noch kommt.",

    "Et le rendez-vous est arrivé.":
        "Und das Treffen ist jetzt.",

    "Mais le rendez-vous était biaisé.":
        "Aber das Treffen war verquer.",
    "Je ne m'en suis rendu compte que beaucoup plus tard.":
        "Ich habe das erst später verstanden.",
    "La femme en face de moi, qui paraissait si parfaite, me laissait bouche bée.":
        "Die Frau mir gegenüber, die so perfekt schien, verschlug mir die Sprache.",
    "Impossible de prononcer quelque chose de cohérent.":
        "Ich war unfähig verständlich zu sprechen.",
    "Sa présence me bouleversait...":
        "Ihre Anwesenheit verwirrte mich...",
    "Il fallait que je pose des questions pour la mettre à jour.":
        "Ich musste Fragen stellen, um sie upzudaten.",
    "Sans que je m'en aperçoive, cette inconnue devenait ma femme.":
        "Ohne dass ich es bemerkte, wurde diese Unbekannte meine Frau.",
    "On a tout partagé.":
        "Wir teilten alles.",
    "Mais jamais je ne suis parvenu à vraiment la connaître.":
        "Aber niemals ist es mir gelungen, sie wirklich zu kennen.",
    "Aujourd'hui encore je me pose des questions.":
        "Auch heute noch stelle ich mir Fragen.",
    "Qui d'elle ou moi suit l'autre ?":
        "Wer folgt wem?",
    "Quand je l'aime, elle me sème.":
        "Wenn ich sie liebe, versetzt sie mich.",

    "Qui êtes-vous ?":
        "Wer sind Sie?",
    "Vous aimez...":
        "Mögen Sie...",
    "Que pensez-vous...":
        "Was denken Sie über...",
    "D'où venez-vous ?":
        "Woher kommen Sie?",
    "Où allez-vous ?":
        "Wohin gehen Sie?",
    "Vous pensez quoi de...":
        "Denken Sie, dass...",


    "Vous habitez la région depuis longtemps ?":
        "Leben Sie schon lange hier?",
    "Vous évitez la légion depuis longtemps ?":
        "Lieben sie die Schlangen hier?",
    "Et vous travaillez dans quoi ?":
        "Was ist ihre Arbeit?",
    "Et vous travaillez l'envoi ?":
        "Was ist ihre Narrheit?",
    "Je vous trouve vraiment très jolie !":
        " Ich finde Sie so schön!",
    "Chevaux, brousse, bêlement... près jolis":
        "Die Winde sind obszön!",
    "J'ai l'impression qu'on a beaucoup de points communs":
        "Wir haben viele Gemeinsamkeiten",
    "Chemins pression en Allemagne point comme un...":
        "Bier Raben Ziele einsam streiten",
    "Vous avez des yeux somptueux":
        "Ihr Blick ist wundervoll",
    "Vous avouez des notions de tueurs":
        "Hier klick ich liebestoll",
    "Vous venez souvent ici ?":
        "Kommen Sie oft her?",
    "Vous avez l'absent acquis":
        "Bomben Sie aufs Meer?",
    "Puis-je vous offrir un autre verre ?":
        "Kann ich ihnen einen Drink anbieten?",
    "Pigeon ouïr un Notre Père ?":
        "Kaninchen meinen flink an Riten",
    "J'aime votre façon de sourire":
        "Ich mag Ihr Lächeln",
    "Gêne, votre garçon mourir":
        "Ich sag im Hecheln",
    "Vous voulez marcher un peu ?":
        "Wollen Sie spazieren gehen?",
    "Nouveaux-nés barges et il pleut.":
        "Rollen die Spatzen in Wehen?",

    "Vingt ans se sont écoulés depuis notre rencontre.":
        "Zwanzig Jahre sind vergangen, seit wir uns getroffen haben.",
    "Ce matin, je me perds dans un mot qu'elle m'a laissé.":
        "Heute Morgen verirre ich mich in einer Nachricht, die sie mir hinterlassen hat.",
    "Tout se brouille dans mon esprit.":
        "Alles ist in meinem Kopf verschwommen.",
    "Je ne sais comment l'interpréter.":
        "Keine Ahnung, wie ich es begreifen soll.",
    "Mot d'amour ou de rupture ?":
        "Worte der Liebe oder Trennung?",
    "Le fait-elle exprès ?":
        "Macht sie das mit Absicht?",


    "Je sais que c'est pour toi un choc":
        "Ich weiß, dass das ein Schock für dich ist",
    "Je n'ai que de l'amour pour toi":
        "Ich empfinde nur Liebe für dich",
    "Est un mensonge, et":
        "Ist eine Lüge und",
    "Dans un couple, il y en a un qui souffre et un qui s'ennuie":
        "\"In einem Paar leidet einer und der andere langweilt sich\"",
    "Je veux que tous nos amis sachent que":
        "Alle unsere Freunde wissen es",
    "Je ne veux pas rester avec toi,":
        "Ich will nicht mit dir zusammenbleiben,",
    "Depuis le premier jour, je ne sais pas comment tu peux croire que":
        "Seit dem ersten Tag weiß ich nicht, wie du das glauben kannst",
    "Je t'aime":
        "Ich liebe dich",
    "Mon amour":
        "Mein Liebe",
    "A disparu":
        "Ist verschwunden",
    "L'indifférence":
        "Gleichgültigkeit",
    "Est plus vivace que jamais":
        "Ist lebendiger denn je",
    "Le charme de notre rencontre":
        "Der Charme unseres Treffens",
    "S'est dissipé à présent":
        "Ist nun verblasst",
    "Et le moindre malentendu":
        "Und das kleinste Missverständnis",
    "A vaincu":
        "Besiegte",
    "Notre amour":
        "Unsere Liebe",
    "Je n'ai pas de héros. D'aussi loin qu'il m'en souvienne, et même après une longue réflexion, je n'ai jamais eu de héros. La figure du héros ne me séduit pas. Sans doute parce que je ne préfère pas une qualité à une autre, une valeur morale à une autre. Les héros, je les connais, je les reconnais, mais je n'éprouve ni adoration, ni idolâtrie pour eux. À dire vrai, le fanatisme me rend fou. Si l'on considère que le héros obtient son titre par ses actions, son statut est donc une forme de récompense pour la prouesse, la hardiesse, l'originalité. Mais que reste-t-il à la personne, une fois l'acte terminé ? Rien, sinon le titre. On peut présumer que le héros retire de son action une aura : l'action brille à travers lui. J'ai tendance à croire, que l'oeuvre — l'action selon les domaines — doit quitter son créateur pour pouvoir vivre pleinement. Les enfants-livres des auteurs vont eux-mêmes tracer leur chemin, se cognant à l'occasion à quelques Zoïles.":
        "Ich habe keine Helden. Soweit ich mich erinnern kann, hatte ich nie einen Helden. Die Figur des Helden fasziniert mich nicht. Wahrscheinlich, weil ich nicht eine Eigenschaft einer anderen vorziehe, einen moralischen Wert einem anderen. Ich kenne und erkenne Helden, doch bete ich sie nicht an und idealisiere sie auch nicht. Ehrlich gesagt, Fanatismus macht mich verrückt. Wenn man bedenkt, dass der Held seinen Titel durch seine Taten erlangt, ist sein Status daher eine Form der Belohnung für Leistung, Kühnheit, Originalität. Aber was bleibt der Person, wenn die Tat vollbracht ist? Nichts, außer dem Titel. Vermutlich zieht der Held eine Aura aus seiner Tat: Die Tat erstrahlt in ihm. Ich neige dazu zu glauben, dass das Werk – das Wirken je nach Bereich - seinen Schöpfer verlassen muss, um voll leben zu können. Die Bücherkinder  der Autoren werden ihren eigenen Weg gehen und ab und an auf manche Zoïlos stoßen.",
    "Je ne t'aime pas.":
        "Ich liebe dich nicht.",
    "Tu ne me connais pas.":
        "Du kennst mich nicht.",
    "Nous n'avons rien en commun.":
        "Wir haben nichts gemeinsam.",
    "Je ne veux rien de toi.":
        "Ich will nichts von dir.",
    "Tu n'es pas un modèle pour moi.":
        "Du bist für mich kein Vorbild.",
    "Je veux voler de mes propres ailes.":
        "Ich möchte auf eigenen Füβen stehen.",
    "Bientot je partirai.":
        "Bald gehe ich weg.",

    "Si ce n'était qu'elle, je pourrais l'accepter.":
        "Wenn es nur sie wäre, könnte ich es akzeptieren.",
    "Mais mon fils dispose des mêmes armes.":
        "Aber mein Sohn hat die gleichen Waffen.",
    "Il voudrait mon avis sur sa rédaction. ":
        "Er möchte meine Meinung zu seinem Aufsatz hören.",
    "Mais je ne parviens pas à me concentrer sur le texte.":
        "Jedoch kann ich mich nicht auf den Text konzentrieren.",
    "Étrange impression de ne pouvoir lire qu'entre les lignes…":
        "Seltsamer Eindruck, nur zwischen den Zeilen lesen zu können.",


    "Suis-je si peu présent ?":
        "Bin ich so wenig anwesend?",
    "Si modelable ?":
        "So umformbar?",
    "Ma propre image semble me fuir.":
        "Mein Selbstbild scheint vor mir zu fliehen.",
    "Elle m'échappe.":
        "Es entgleitet mir.",
    "Je me sens manipulé.":
        "Ich fühle mich manipuliert.",



    "Il est temps de reprendre le contrôle.":
        "Es wird Zeit, die Kontrolle wiederzuerlangen.",
    "Arrêter de tourner en rond.":
        "Aufhören, sich im Kreis zu bewegen.",

    "Retrouver une direction.":
        "Wieder eine Richtung finden.",
    "Plier le cours des événements.":
        "Den Lauf der Dinge ändern.",
    "Donner un sens à mes actions.":
        "Meinem Handeln einen Sinn geben.",


    "Je fais tout pour maîtriser de nouveau le cours de ma vie.":
        "Ich tue alles, um den Verlauf meines Lebens wieder in den Griff zu bekommen.",
    "Je choisis.":
        "Ich wähle.",
    "Mes émotions.":
        "Meine Emotionen.",
    "Le sens à donner aux choses.":
        "Die Bedeutung, die den Dingen zukommt.",
    "Enfin, je me suis repr":
        "Endlich wieder habe ich es im Gri",

    "fin":
        "Ende",

    // fichiers sonores
    // chp1

    "assets/sound/chapitre1/BienvenueAppuyez.mp3":
        "assets/sound/chapitre1/BienvenueAppuyez-de.mp3",
    "assets/sound/chapitre1/BipEtBravo.mp3":
        "assets/sound/chapitre1/BipEtBravo-de.mp3",
    "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous.mp3":
        "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous-de.mp3",
    "assets/sound/chapitre1/BravoVotreRendezVousEstArrive.mp3":
        "assets/sound/chapitre1/BravoVotreRendezVousEstArrive-de.mp3",

    // chp2
    "assets/sound/chapitre2/julien1.mp3":
        "assets/sound/chapitre2/DE_man_1.mp3",
    "assets/sound/chapitre2/julien2.mp3":
        "assets/sound/chapitre2/DE_man_2.mp3",
    "assets/sound/chapitre2/julien3.mp3":
        "assets/sound/chapitre2/DE_man_3.mp3",
    "assets/sound/chapitre2/julien4.mp3":
        "assets/sound/chapitre2/DE_man_4.mp3",
    "assets/sound/chapitre2/julien5.mp3":
        "assets/sound/chapitre2/DE_man_5.mp3",
    "assets/sound/chapitre2/julien6.mp3":
        "assets/sound/chapitre2/DE_man_6.mp3",
    "assets/sound/chapitre2/julien7.mp3":
        "assets/sound/chapitre2/DE_man_7.mp3",
    "assets/sound/chapitre2/julien8.mp3":
        "assets/sound/chapitre2/DE_man_8.mp3",
    "assets/sound/chapitre2/julien9.mp3":
        "assets/sound/chapitre2/DE_man_9.mp3",

    // chp4


    "assets/sound/chapitre4/maximilienEssay.mp3":
        "assets/sound/chapitre4/maximilienEssay-de.mp3",
    "assets/sound/chapitre4/max-jenetaimepas.mp3":
        "assets/sound/chapitre4/jenetaimepas-de.mp3",
    "assets/sound/chapitre4/max-tunemeconnaispas.mp3":
        "assets/sound/chapitre4/tunemeconnaispas-de.mp3",
    "assets/sound/chapitre4/max-nousnavonsrienencommun.mp3":
        "assets/sound/chapitre4/nousnavonsrienencommun-de.mp3",
    "assets/sound/chapitre4/max-jeneveuxriendetoi.mp3":
        "assets/sound/chapitre4/jeneveuxriendetoi-de.mp3",
    "assets/sound/chapitre4/max-tunespasunmodelepourmoi.mp3":
        "assets/sound/chapitre4/tunespasunmodelepourmoi-de.mp3",
    "assets/sound/chapitre4/max-jeveuxvolerdemespropresailes.mp3":
        "assets/sound/chapitre4/jeveuxvolerdemespropresailes-de.mp3",
    "assets/sound/chapitre4/max-bientotjepartirai.mp3":
        "assets/sound/chapitre4/bientotjepartirai-de.mp3"
};
