// lang-en.ts

export const LANG_EN_NAME = 'en';

export const LANG_EN_TRANS = {

    "Accueil":
        "Home",
    "Présentation":
        "Presentation",
    "Crédits":
        "Credits",
    "Prix":
        "Prize",
    "Scènes":
        "Scenes",
    "Scène":
        "Scene",
    "Version Flash":
        "Flash Version",
    "Version application smartphone":
        "App for smartphones",
    "Version Angular":
        "JavaScript Version",
    "Langues":
        "Languages",
    "Réalisation":
        "Direction",
    "Traduction":
        "Translation",
    "Voix":
        "Voice",
    "Épouse":
        "Wife",
    "Musique":
        "Music",
    "Vidéo":
        "Video",
    "Téléphone":
        "Phone",
    "Narrateur":
        "Narrator",
    "ado":
        "Teenager",
    "Ambiance":
        "Athmosphere",
    "Manipulation":
        "Manipulation",
    "Saxophone":
        "Saxophone",
    "Guitare électrique":
        "Electric guitar",
    "Batterie/Montage":
        "Drum/Montage",
    "Extrait du disque":
        "Extract of the disk",

    "Déprise est également disponible sous forme d'application mobile.":
        "This creation is also available as an application for mobile devices:",
    "Application sur Google Play":
        "Google Play",
    "Application sur l'Apple Store":
        "App Store",

    "Déprise a gagné le prix New Media Writing Prize en 2011.":
        "This creation won the New Media Writing Prize 2011.",
    "Voir le site":
        "See the website",

    'hello world': 'hello world',
    "Toute ma vie, j'ai cru avoir devant moi un champ des possibles infini.": "My entire life, I believed I had infinite prospects before me.",
    "Déprise": "Loss of Grasp",
    "déprise": "Loss of Grasp",
    "Déprise est une création sur les notions de prise et perte de contrôle. Quand a-t-on l'impression d'être en situation de prise ou de perte de prise dans la vie? Six scènes racontent l'histoire d'un personnage en pleine déprise. Parallèlement, ce jeu sur prise et déprise permet de mettre en scène la situation du lecteur d'une oeuvre interactive.":
        "Loss of Grasp is a digital creation about the notions of grasp and control. Under which circumstances do we feel we have a grip on our life or not ? Six scenes feature a character who is losing grasp. At the same time, this play on grasp and loss of grasp mirrors the reader's experience of an interactive digital work.",
    "Avez-vous pensé à allumer vos enceintes ?": "Is your computer's sound on?",
    "(Appuyez sur n'importe quelle touche pour continuer)": "(Press any key to continue)",
    "\"L'univers entier m'appartient\", pensais-je.": "\"The whole universe belongs to me\", I thought.",
    "J'ai le choix.": "I have the choice.",

    "Je suis maître de mon destin.":
        "I control my destiny.",
    "Je peux prendre ce qui me plaît.":
        "I am the king of the world.",
    "Je deviendrai ce que je veux.":
        "I will become what I want.",
    "J'ai tracé mon propre chemin.":
        "I followed my own path.",
    "J'ai parcouru de magnifiques paysages.":
        "I browsed beautiful landscapes.",
    "Quoi de plus naturel, je les avais choisis.":
        "No wonder, because I had chosen them.",
    "Mais depuis un moment, j'ai des doutes.":
        "But for a while, I have had doubts.",
    "Comment avoir prise sur ce qui m'arrive ?":
        "How can I have grasp on what happens to me?",
    "Tout s'échappe.":
        "Everything escapes me.",
    "Me glisse entre les doigts.":
        "Slips through my fingers.",
    "Les objets, les personnes.":
        "Objects, people.",
    "J'ai l'impression de ne plus rien contrôler.":
        "I feel I've lost control.",
    "Depuis quelques temps maintenant,":
        "For some time now,",
    "Je n'attends qu'une chose.":
        "I expect but one thing.",
    "La suite.":
        "What comes next.",

    "Et le rendez-vous est arrivé.":
        "Meeting time has arrived.",

    "Mais le rendez-vous était biaisé.":
        "But the meeting was trumped.",
    "Je ne m'en suis rendu compte que beaucoup plus tard.":
        "I only realized it later.",
    "La femme en face de moi, qui paraissait si parfaite, me laissait bouche bée.":
        "The woman in front of me seemed so perfect, I was flabbergasted.",
    "Impossible de prononcer quelque chose de cohérent.":
        "I couldn't say anything coherent.",
    "Sa présence me bouleversait...":
        "I was distraught.",
    "Il fallait que je pose des questions pour la mettre à jour.":
        "I had to ask questions to reveal her.",
    "Sans que je m'en aperçoive, cette inconnue devenait ma femme.":
        "Without my being aware of it, this stranger became my wife.",
    "On a tout partagé.":
        "We shared everything.",
    "Mais jamais je ne suis parvenu à vraiment la connaître.":
        "But I never got to truly know her.",
    "Aujourd'hui encore je me pose des questions.":
        "Today, I still wonder.",
    "Qui d'elle ou moi suit l'autre ?":
        "Who is following whom?",
    "Quand je l'aime, elle me sème.":
        "When I love her, she loses me.",

    "Qui êtes-vous ?":
        "Who are you?",
    "Vous aimez...":
        "Do you like...",
    "Que pensez-vous...":
        "What do you think about...",
    "D'où venez-vous ?":
        "Where are you from?",
    "Où allez-vous ?":
        "Where are you going?",
    "Vous pensez quoi de...":
        "Do you think...",


    "Vous habitez la région depuis longtemps ?":
        "Have you lived around here for a long time?",
    "Vous évitez la légion depuis longtemps ?":
    "Have you used the wrong ear for a long time?",
		"Et vous travaillez dans quoi ?":
		"What do you do for a living?",
    "Et vous travaillez l'envoi ?":
        "What do you do fall and evening?",
    "Je vous trouve vraiment très jolie !":
        "You are very pretty!",
    "Chevaux, brousse, bêlement... près jolis":
        "You all very picky!",
    "J'ai l'impression qu'on a beaucoup de points communs":
        "I feel we have a lot in common",
    "Chemins pression en Allemagne point comme un...":
    "I feel we have a lottery, come on",
		"Vous avez des yeux somptueux":
    "You have gorgeous eyes",
    "Vous avouez des notions de tueurs":
        "You have girly size",
    "Vous venez souvent ici ?":
        "Do you come here often?",
    "Vous avez l'absent acquis":
        "Dew comes here often?",
    "Puis-je vous offrir un autre verre ?":
        "Can I get you another drink?",
    "Pigeon ouïr un Notre Père ?":
        "Caning gets you into the drink",
    "J'aime votre façon de sourire":
        "I like the way you smile.",
    "Gêne, votre garçon mourir":
        "I light the west aisle",
    "Vous voulez marcher un peu ?":
        "Shall we go for a walk?",
    "Nouveaux-nés barges et il pleut.":
        "Shall the gopher talk?",

    "Vingt ans se sont écoulés depuis notre rencontre.":
        "Twenty years have gone by since we met.",
    "Ce matin, je me perds dans un mot qu'elle m'a laissé.":
        "This morning, I am reading a note she left me.",
    "Tout se brouille dans mon esprit.":
        "I am at a loss.",
    "Je ne sais comment l'interpréter.":
        "I don't know what to make of it.",
    "Mot d'amour ou de rupture ?":
        "Love poem or break up note ?",
    "Le fait-elle exprès ?":
        "Does she do it on purpose ?",


    "Je sais que c'est pour toi un choc":
        "I know it's a shock for you",
    "Je n'ai que de l'amour pour toi":
        "All I feel for you is love",
    "Est un mensonge, et":
        "Is a lie, and",
    "Dans un couple, il y en a un qui souffre et un qui s'ennuie":
        "\"In a couple, there is always one who suffers and one who is bored\"",
    "Je veux que tous nos amis sachent que":
        "I want all our friends to know that",
    "Je ne veux pas rester avec toi,":
        "I don't want to stay with you,",
    "Depuis le premier jour, je ne sais pas comment tu peux croire que":
        "From the first day, I have wondered how you can believe that",
    "Je t'aime":
        "I love you",
    "Mon amour":
        "My love",
    "A disparu":
        "Has disappeared",
    "L'indifférence":
        "Indifference",
    "Est plus vivace que jamais":
        "Is more vivid than ever",
    "Le charme de notre rencontre":
        "The charm of our encounter",
    "S'est dissipé à présent":
        "Has dissolved",
    "Et le moindre malentendu":
        "And the slightest misunderstanding",
    "A vaincu":
        "Has vanquished",
    "Notre amour":
        "Our Love",
    "Je n'ai pas de héros. D'aussi loin qu'il m'en souvienne, et même après une longue réflexion, je n'ai jamais eu de héros. La figure du héros ne me séduit pas. Sans doute parce que je ne préfère pas une qualité à une autre, une valeur morale à une autre. Les héros, je les connais, je les reconnais, mais je n'éprouve ni adoration, ni idolâtrie pour eux. À dire vrai, le fanatisme me rend fou. Si l'on considère que le héros obtient son titre par ses actions, son statut est donc une forme de récompense pour la prouesse, la hardiesse, l'originalité. Mais que reste-t-il à la personne, une fois l'acte terminé ? Rien, sinon le titre. On peut présumer que le héros retire de son action une aura : l'action brille à travers lui. J'ai tendance à croire, que l'oeuvre — l'action selon les domaines — doit quitter son créateur pour pouvoir vivre pleinement. Les enfants-livres des auteurs vont eux-mêmes tracer leur chemin, se cognant à l'occasion à quelques Zoïles.":
        "I don't have a hero. As far back as I can remember, and even after thinking hard, I have never had a hero. The hero figure doesn't appeal to me. No doubt because I don't value one quality or moral value more than another. I know heroes, I can recognize them, but I don't love them nor worship them. To tell the truth, I hate fanatics. If one considers that what makes a hero is what he does, his title is a reward for his feats, his heroic actions, his uniqueness. But what is he left with once his heroic deeds are over? Nothing but the title. It can be assumed that the hero retains an aura : the action shines through him. I tend to believe that the deed has to free itself from its creator to live a life of its own. The authors' offspring will meet their own audience, occasionally finding on their way a few harsh and envious reviewers.",
    "Je ne t'aime pas.":
        "I don't love you.",
    "Tu ne me connais pas.":
        "You don't know me.",
    "Nous n'avons rien en commun.":
        "We have nothing in common.",
    "Je ne veux rien de toi.":
        "I don't want anything from you.",
    "Tu n'es pas un modèle pour moi.":
        "You're not a model for me.",
    "Je veux voler de mes propres ailes.":
        "I want to make my own way.",
    "Bientot je partirai.":
        "Soon I will leave",

    "Si ce n'était qu'elle, je pourrais l'accepter.":
        "I can take it from my wife.",
    "Mais mon fils dispose des mêmes armes.":
        "But how can my son do this to me…",
    "Il voudrait mon avis sur sa rédaction. ":
        "He wants me to read his paper.",
    "Mais je ne parviens pas à me concentrer sur le texte.":
        "But I can't focus on the words.",
    "Étrange impression de ne pouvoir lire qu'entre les lignes…":
        "How come I can only read between the lines…",


    "Suis-je si peu présent ?":
        "Am I so little here?",
    "Si modelable ?":
        "So easily deformed?",
    "Ma propre image semble me fuir.":
        "My own image seems to escape me.",
    "Elle m'échappe.":
        "It fails me.",
    "Je me sens manipulé.":
        "I feel manipulated.",



    "Il est temps de reprendre le contrôle.":
        "It's time to take control again.",
    "Arrêter de tourner en rond.":
        "To stop going round in circles.",

    "Retrouver une direction.":
        "Find some guidelines.",
    "Plier le cours des événements.":
        "Shape events.",
    "Donner un sens à mes actions.":
        "Give meaning to my actions.",


    "Je fais tout pour maîtriser de nouveau le cours de ma vie.":
        "I'm doing all I can to get a grip on my life again.",
    "Je choisis.":
        "I make choices.",
    "Mes émotions.":
        "I control my emotions.",
    "Le sens à donner aux choses.":
        "The meaning of things.",
    "Enfin, je me suis repr":
        "At last, I have a gra",

    "fin":
        "The end",

    // fichiers sonores
    // chp1

    "assets/sound/chapitre1/BienvenueAppuyez.mp3":
        "assets/sound/chapitre1/BienvenueAppuyez-en.mp3",
    "assets/sound/chapitre1/BipEtBravo.mp3":
        "assets/sound/chapitre1/BipEtBravo-en.mp3",
    "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous.mp3":
        "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous-en.mp3",
    "assets/sound/chapitre1/BravoVotreRendezVousEstArrive.mp3":
        "assets/sound/chapitre1/BravoVotreRendezVousEstArrive-en.mp3",

    // chp2
    "assets/sound/chapitre2/julien1.mp3":
        "assets/sound/chapitre2/serge1-en.mp3",
    "assets/sound/chapitre2/julien2.mp3":
        "assets/sound/chapitre2/serge2-en.mp3",
    "assets/sound/chapitre2/julien3.mp3":
        "assets/sound/chapitre2/serge3-en.mp3",
    "assets/sound/chapitre2/julien4.mp3":
        "assets/sound/chapitre2/serge4-en.mp3",
    "assets/sound/chapitre2/julien5.mp3":
        "assets/sound/chapitre2/serge5-en.mp3",
    "assets/sound/chapitre2/julien6.mp3":
        "assets/sound/chapitre2/serge6-en.mp3",
    "assets/sound/chapitre2/julien7.mp3":
        "assets/sound/chapitre2/serge7-en.mp3",
    "assets/sound/chapitre2/julien8.mp3":
        "assets/sound/chapitre2/serge8-en.mp3",
    "assets/sound/chapitre2/julien9.mp3":
        "assets/sound/chapitre2/serge9-en.mp3",

    // chp4


    "assets/sound/chapitre4/maximilienEssay.mp3":
        "assets/sound/chapitre4/maximilienEssay-en-Kieran.mp3",
    "assets/sound/chapitre4/max-jenetaimepas.mp3":
        "assets/sound/chapitre4/jenetaimepas-en.mp3",
    "assets/sound/chapitre4/max-tunemeconnaispas.mp3":
        "assets/sound/chapitre4/tunemeconnaispas-en.mp3",
    "assets/sound/chapitre4/max-nousnavonsrienencommun.mp3":
        "assets/sound/chapitre4/nousnavonsrienencommun-en.mp3",
    "assets/sound/chapitre4/max-jeneveuxriendetoi.mp3":
        "assets/sound/chapitre4/jeneveuxriendetoi-en.mp3",
    "assets/sound/chapitre4/max-tunespasunmodelepourmoi.mp3":
        "assets/sound/chapitre4/tunespasunmodelepourmoi-en.mp3",
    "assets/sound/chapitre4/max-jeveuxvolerdemespropresailes.mp3":
        "assets/sound/chapitre4/jeveuxvolerdemespropresailes-en.mp3",
    "assets/sound/chapitre4/max-bientotjepartirai.mp3":
        "assets/sound/chapitre4/bientotjepartirai-en.mp3"




};
