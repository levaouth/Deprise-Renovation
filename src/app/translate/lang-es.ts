// lang-es.ts

export const LANG_ES_NAME = 'es';

export const LANG_ES_TRANS = {
    "hello world": "Hola mundo",
	"déprise":"Perderse",
    "Déprise":"Perderse",

  "Accueil":
  "Presentación",
  "Présentation":
  "Presentación",
  "Crédits":
  "Agradecimientos",
  "Prix":
  "Premio",
  "Scènes":
  "Escena",
  "Scène":
  "Escena",
  "Version Flash":
  "Versiòn Flash",
  "Version application smartphone":
  "App for smartphones",
  "Version Angular":
  "Versiòn JavaScript",
  "Langues":
  "Idiomas",
  "Réalisation":
  "Regia",
  "Traduction":
  "Traducciòn",
  "Voix":
  "Voz",
  "Épouse":
  "Espoza",
  "Musique":
  "Música",
  "Vidéo":
  "Video",
  "Téléphone":
  "Teléfono",
  "Narrateur":
  "Narrador",
  "ado":
  "adolescente",
  "Ambiance":
  "Ambiente",
  "Manipulation":
  "Manipulación",
  "Saxophone":
  "Saxofón",
  "Guitare électrique":
  "Guitarra eléctrica",
  "Batterie/Montage":
  "Batteria/Montaje",
  "Extrait du disque":
  "Extracto del disco",

  "Déprise a gagné le prix New Media Writing Prize en 2011.":
  "This creation won the New Media Writing Prize 2011.",
  "Voir le site":
  "Ver el sitio Web",

  "Déprise est également disponible sous forme d'application mobile." :
  "This creation is also available as an application for mobile devices:" ,
  "Application sur Google Play":
  "Google Play",
  "Application sur l'Apple Store":
  "App Store",

	"Déprise est une création sur les notions de prise et perte de contrôle. Quand a-t-on l'impression d'être en situation de prise ou de perte de prise dans la vie? Six scènes racontent l'histoire d'un personnage en pleine déprise. Parallèlement, ce jeu sur prise et déprise permet de mettre en scène la situation du lecteur d'une oeuvre interactive.":
	"Perderse es una creación digital en torno a la pérdida de control de uno mismo. ¿Cuándo tenemos la impresión de controlar o no nuestra vida? En seis escenas se cuenta la historia de un personaje en plena crisis existencial. Paralelamente, este juego de control-pérdida permite escenificar la posición del lector dentro de una obra interactiva.",
	"Avez-vous pensé à allumer vos enceintes ?":
	"¿Tienes los altavoces encendidos?",


  "Appuyez sur n'importe quelle touche":
	"Pulsa una tecla cualquiera",
    "Toute ma vie, j'ai cru avoir devant moi un champ des possibles infini.":
		"Toda mi vida creí tener por delante infinidad de mundos posibles.",
		"\"L'univers entier m'appartient\", pensais-je.":
		"\"El universo entero me pertenece\", pensaba.",
		"J'ai le choix.":
		"Puedo elegir.",
		"Je suis maître de mon destin.":
		"Soy dueño de mi destino.",
		"Je peux prendre ce qui me plaît.":
		"Puedo tener aquello que deseo.",
		"Je deviendrai ce que je veux.":
		"Seré lo que quiero ser.",
		"J'ai tracé mon propre chemin.":
		"Escribí mi propio destino.",
		"J'ai parcouru de magnifiques paysages.":
		"Recorrí los más bellos paisajes.",
		"Quoi de plus naturel, je les avais choisis.":
		"Normal, yo mismo los había elegido.",
		"Mais depuis un moment, j'ai des doutes.":
		"Pero últimamente tengo mis dudas.",
		"Comment avoir prise sur ce qui m'arrive ?":
		"¿Cómo tomar el control de las cosas que me pasan?",
		"Tout s'échappe.":
		"Todo se escapa.",
		"Me glisse entre les doigts.":
		"Se me escurre entre los dedos.",
		"Les objets, les personnes.":
		"Los objetos, las personas.",
		"J'ai l'impression de ne plus rien contrôler.":
		"Tengo la impresión que de ya que no controlo nada.",
		"Depuis quelques temps maintenant,":
		"De un tiempo a esta parte",
		"Je n'attends qu'une chose.":
		"sólo puedo esperar una cosa.",
		"La suite.":
		"La continuación.",

	  "Et le rendez-vous est arrivé.":
	  "Y llegó mi turno.",
		"Mais le rendez-vous était biaisé.":
		"Pero la cosa tenía trampa.",
		"Je ne m'en suis rendu compte que beaucoup plus tard.":
		"No me di cuenta hasta mucho después.",
		"La femme en face de moi, qui paraissait si parfaite, me laissait bouche bée.":
		"La mujer frente a mí parecía tan perfecta que me dejó boquiabierto.",
		"Impossible de prononcer quelque chose de cohérent.":
		"Imposible articular nada coherente.",
		"Sa présence me bouleversait...":
		"Su mera presencia me hacía temblar...",
		"Il fallait que je pose des questions pour la mettre à jour.":
		"Tuve que hacer preguntas para seguir navegando.",
		"Sans que je m'en aperçoive, cette inconnue devenait ma femme.":
		"Sin que me diera cuenta, aquella desconocida se iba convirtiendo en mi mujer.",
		"On a tout partagé.":
		"Lo compartimos todo.",
		"Mais jamais je ne suis parvenu à vraiment la connaître.":
		"Pero nunca logré llegar a conocerla de verdad.",
		"Aujourd'hui encore je me pose des questions.":
		"Todavía hoy me sigo haciendo preguntas.",
		"Qui d'elle ou moi suit l'autre ?":
		"¿Quién de los dos persigue a quién?",
		"Quand je l'aime, elle me sème.":
		"Cuando la amo, es ella quien me siembra.",


		"Qui êtes-vous ?":
		"¿Quién eres?",
		"Vous aimez...":
		"¿Te gusta...",
		"Que pensez-vous...":
		"¿Qué piensas...",
		"D'où venez-vous ?":
		"¿De dónde vienes?",
		"Où allez-vous ?":
		"¿A dónde vas?",
		"Vous pensez quoi de...":
		"¿Qué piensas de...",
		"Vous habitez la région depuis longtemps ?":
		"¿Hace mucho que vives por aquí?",
    "Vous évitez la légion depuis longtemps ?":
    "¿Hace mucho que mueres así?",
		"Et vous travaillez dans quoi ?":
		"¿Y a qué te dedicas?" ,
    "Et vous travaillez l'envoi ?":
    "¿Y dónde te pica?" ,
		"Je vous trouve vraiment très jolie !":
		"¡Si es que no se puede ser más guapa!" ,
    "Chevaux, brousse, bêlement... près jolis":
    "¡Miente, sol, no duele...la hojalata!" ,
		"J'ai l'impression qu'on a beaucoup de points communs":
		"Me parece que tenemos tanto en común" ,
    "Chemins pression en Allemagne point comme un...":
    "Me parece que tenemos llanto atún..." ,
		"Vous avez des yeux somptueux":
		"¡Qué ojos más bonitos tienes!",
    "Vous avouez des notions de tueurs":
    "¡Qué loro y pan bonito verde!" ,
		"Vous venez souvent ici ?":
		"¿Vienes mucho por aquí?" ,
    "Vous avez l'absent acquis":
    "¿Siempre chuchos para mí?" ,
		"Puis-je vous offrir un autre verre ?":
		"¿Puedo invitarte a otra copa?" ,
    "Pigeon ouïr un Notre Père ?":
    "¿Miedo a imitarte y la ropa?" ,
		"J'aime votre façon de sourire":
		"Me encanta tu sonrisa" ,
    "Gêne, votre garçon mourir":
    "Andar por la cornisa." ,
		"Vous voulez marcher un peu ?":
		"¿Te apetece dar una vuelta?" ,
    "Nouveaux-nés barges et il pleut.":
    "No amanece apretar tuercas." ,



		"Vingt ans se sont écoulés depuis notre rencontre.":
		"Han pasado veinte años desde que nos conocimos.",
		"Ce matin, je me perds dans un mot qu'elle m'a laissé.":
		"Esta mañana, me encuentro con una nota suya",
		"Tout se brouille dans mon esprit.":
		"y es igual que un laberinto.",
		"Je ne sais comment l'interpréter.":
		"No sé qué pensar.",
		"Mot d'amour ou de rupture ?":
		"¿Nota de amor o de ruptura?",
		"Le fait-elle exprès ?":
		"¿Lo habrá hecho adrede?",



		"Je sais que c'est pour toi un choc":
		" Sé que no te lo esperabas",
		"Je n'ai que de l'amour pour toi":
		"\"No siento más que amor hacia ti\"",
		"Est un mensonge, et":
		"Es mentira y",
		"Dans un couple, il y en a un qui souffre et un qui s'ennuie":
		"\"En toda pareja, hay uno que sufre y otro que se aburre\"",
		"Je veux que tous nos amis sachent que":
		"Quiero que todos nuestros amigos sepan que",
		"Je ne veux pas rester avec toi,":
		"No puedo seguir contigo,",
		"Depuis le premier jour, je ne sais pas comment tu peux croire que":
		"Desde el primer día, no sé cómo puedes pensar que",






		"Je t'aime":
		"Te quiero",
		"Mon amour":
		"Mi amor",
		"A disparu":
		"Se ha evaporado",
		"L'indifférence":
		"La indiferencia",
		"Est plus vivace que jamais":
		"Está más viva que nunca",
		"Le charme de notre rencontre":
		"La magia de nuestro encuentro",
		"S'est dissipé à présent":
		"Se agosta en el presente",
		"Et le moindre malentendu":
		"Y el menor malentendido",
		"A vaincu":
		"Sale victorioso",
		"Notre amour":
		"Nuestro amor",












	  "Je n'ai pas de héros. D'aussi loin qu'il m'en souvienne, et même après une longue réflexion, je n'ai jamais eu de héros. La figure du héros ne me séduit pas. Sans doute parce que je ne préfère pas une qualité à une autre, une valeur morale à une autre. Les héros, je les connais, je les reconnais, mais je n'éprouve ni adoration, ni idolâtrie pour eux. À dire vrai, le fanatisme me rend fou. Si l'on considère que le héros obtient son titre par ses actions, son statut est donc une forme de récompense pour la prouesse, la hardiesse, l'originalité. Mais que reste-t-il à la personne, une fois l'acte terminé ? Rien, sinon le titre. On peut présumer que le héros retire de son action une aura : l'action brille à travers lui. J'ai tendance à croire, que l'oeuvre — l'action selon les domaines — doit quitter son créateur pour pouvoir vivre pleinement. Les enfants-livres des auteurs vont eux-mêmes tracer leur chemin, se cognant à l'occasion à quelques Zoïles.":
	"No tengo héroes. Desde que tengo uso de razón, y por más que lo piense, me pasa que no tengo héroes.  La figura del héroe no me seduce. Seguramente porque no prefiero tal cualidad a tal otra, tal valor moral a tal otro. A los héroes los conozco, los reconozco, pero no experimento ni adoración, ni idolatría hacia ellos. A decir verdad, el fanatismo me saca de mis casillas. Si consideramos que el héroe se define como tal por sus acciones, su estatus sería una forma de recompensa por sus proezas, su audacia, su originalidad. Pero, ¿qué le queda a la persona, más allá del acto? Nada, excepto el estatus. Supongamos que sus acciones le proporcionaran al héroe un aura: que la acción brilla a través de él. Me inclino por pensar, que la obra -o la acción, según el campo- debe abandonar a su creador para poder vivir con plenitud. Los hijos-libros de sus autores habrán de escribir ellos mismos su camino, topándose de cuando en cuando con algún que otro Zoilo.",

		"Je ne t'aime pas.":
		"No te quiero.",
		"Tu ne me connais pas.":
		"No me conoces.",
		"Nous n'avons rien en commun.":
		"No tenemos nada en común.",
		"Je ne veux rien de toi.":
		"No quiero nada tuyo.",
		"Tu n'es pas un modèle pour moi.":
		"No eres un ejemplo para mí.",
		"Je veux voler de mes propres ailes.":
		"Quiero volar con mis propias alas.",
		"Bientot je partirai.":
		"Pronto me marcharé.",

		"Si ce n'était qu'elle, je pourrais l'accepter.":
		"Lo de mi mujer, pase.",
		"Mais mon fils dispose des mêmes armes.":
		"Pero mi hijo...",
		"Il voudrait mon avis sur sa rédaction. ":
		"Quiere mi opinión sobre una redacción que ha escrito.",
		"Mais je ne parviens pas à me concentrer sur le texte.":
		"Pero no logro concentrarme en el texto",
		"Étrange impression de ne pouvoir lire qu'entre les lignes…":
		"Qué extraña sensación, sólo puedo leer entre líneas...",


		"Suis-je si peu présent ?":
		"¿Acaso soy tan poca cosa?",
		"Si modelable ?":
		"¿Tan volátil?",
		"Ma propre image semble me fuir.":
		"Hasta mi propia imagen parece que se escapa.",
		"Elle m'échappe.":
		"Se me escapa.",
		"Je me sens manipulé.":
		"Me siento una marioneta.",


		"Il est temps de reprendre le contrôle.":
		"Tengo que recuperar de una vez el control.",
		"Arrêter de tourner en rond.":
		"Basta ya de caminar en círculos.",

		"Retrouver une direction.":
		"Saber a dónde ir.",
		"Plier le cours des événements.":
		"Que vuelvan las aguas a su cauce.",
		"Donner un sens à mes actions.":
		"Darle sentido a mis acciones.",

		"Je fais tout pour maîtriser de nouveau le cours de ma vie.":
			"Lo estoy intentando todo para que mi vida vuelva a su cauce.",
		"Je choisis.":
	  "Yo elijo.",
		"Mes émotions.":
		"Mis emociones.",
		"Le sens à donner aux choses.":
		"Qué sentido darle a cada cosa.",
		"Enfin, je me suis repr":
		"Volver a enchufa.",

    "fin":
    "fin",

// fichiers sonores
// chp1

    "assets/sound/chapitre1/BienvenueAppuyez.mp3":
    "assets/sound/chapitre1/BienvenueAppuyez-es.mp3",
    "assets/sound/chapitre1/BipEtBravo.mp3":
    "assets/sound/chapitre1/BipEtBravo-es.mp3",
    "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous.mp3":
    "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous-es.mp3",
    "assets/sound/chapitre1/BravoVotreRendezVousEstArrive.mp3":
    "assets/sound/chapitre1/BravoVotreRendezVousEstArrive-es.mp3",

// chp2
    "assets/sound/chapitre2/julien1.mp3":
    "assets/sound/chapitre2/ES_man_1.mp3",
    "assets/sound/chapitre2/julien2.mp3":
    "assets/sound/chapitre2/ES_man_2.mp3",
    "assets/sound/chapitre2/julien3.mp3":
    "assets/sound/chapitre2/ES_man_3.mp3",
    "assets/sound/chapitre2/julien4.mp3":
    "assets/sound/chapitre2/ES_man_4.mp3",
    "assets/sound/chapitre2/julien5.mp3":
    "assets/sound/chapitre2/ES_man_5.mp3",
    "assets/sound/chapitre2/julien6.mp3":
    "assets/sound/chapitre2/ES_man_6.mp3",
    "assets/sound/chapitre2/julien7.mp3":
    "assets/sound/chapitre2/ES_man_7.mp3",
    "assets/sound/chapitre2/julien8.mp3":
    "assets/sound/chapitre2/ES_man_8.mp3",
    "assets/sound/chapitre2/julien9.mp3":
    "assets/sound/chapitre2/ES_man_9.mp3",

// chp4


    "assets/sound/chapitre4/maximilienEssay.mp3":
    "assets/sound/chapitre4/maximilienEssay-es.mp3",
    "assets/sound/chapitre4/max-jenetaimepas.mp3":
    "assets/sound/chapitre4/jenetaimepas-es.mp3",
    "assets/sound/chapitre4/max-tunemeconnaispas.mp3":
    "assets/sound/chapitre4/tunemeconnaispas-es.mp3",
    "assets/sound/chapitre4/max-nousnavonsrienencommun.mp3":
    "assets/sound/chapitre4/nousnavonsrienencommun-es.mp3",
    "assets/sound/chapitre4/max-jeneveuxriendetoi.mp3":
    "assets/sound/chapitre4/jeneveuxriendetoi-es.mp3",
    "assets/sound/chapitre4/max-tunespasunmodelepourmoi.mp3":
    "assets/sound/chapitre4/tunespasunmodelepourmoi-es.mp3",
    "assets/sound/chapitre4/max-jeveuxvolerdemespropresailes.mp3":
    "assets/sound/chapitre4/jeveuxvolerdemespropresailes-es.mp3",
    "assets/sound/chapitre4/max-bientotjepartirai.mp3":
    "assets/sound/chapitre4/bientotjepartirai-es.mp3",

};
