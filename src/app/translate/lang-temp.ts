// lang-pt.ts

export const LANG_PT_NAME = 'pt';

export const LANG_PT_TRANS = {
    "hello world": "je ne sais pas parler pt",
    "Accueil":
        "Pagina initial",
    "Présentation":
        "Apresentação",
    "Crédits":
        "Agradeços",
    "Prix":
        "Premios",
    "Scènes":
        "Cenas",
    "Scène":
        "Cena",
    "Version Flash":
        "Versão Flash",
    "Version application smartphone":
        "App for smartphones",
    "Version":
        "Versões",
    "Langues":
        "Línguas",
    "Réalisation":
        "Realização",
    "Traduction":
        "Tradução",
    "Voix":
        "Voz",
    "Épouse":
        "Esposa",
    "Musique":
        "Música",
    "Vidéo":
        "Video",
    "Téléphone":
        "Telefone",
    "Narrateur":
        "Narrador",
    "ado":
        "adolescente",
    "Ambiance":
        "Ambiente",
    "Manipulation":
        "Manuseamento",
    "Saxophone":
        "Saxofone",
    "Guitare électrique":
        "Guitarra elétrica",
    "Batterie/Montage":
        "Batteria/Montagem",
    "Extrait du disque":
        "Excerto del disco",

    "Déprise a gagné le prix New Media Writing Prize en 2011.":
        "This creation won the New Media Writing Prize 2011.",
    "Voir le site":
        "See the website",

    "Déprise est également disponible sous forme d'application mobile.":
        "This creation is also available as an application for mobile devices:",
    "Application sur Google Play":
        "Google Play",
    "Application sur l'Apple Store":
        "App Store",


    "Déprise":
        "Perda de controlo",
    "déprise":
        "perda de controlo",
    "Déprise est une création sur les notions de prise et perte de contrôle. Quand a-t-on l'impression d'être en situation de prise ou de perte de prise dans la vie? Six scènes racontent l'histoire d'un personnage en pleine déprise. Parallèlement, ce jeu sur prise et déprise permet de mettre en scène la situation du lecteur d'une oeuvre interactive.":
        "Perda de controlo é uma criação digital sobre a noção de (perda de) controlo. Em que circunstâncias sentimos que temos (ou não) controlo sobre a nossa vida? Seis cenas apresentam uma personagem em plena crise existencial. Simultaneamente, este jogo entre ter e perder o controlo espelha a experiência do leitor na relação com uma obra digital interactiva.",
    "Avez-vous pensé à allumer vos enceintes ?":
        "O som do computador está ligado?",
    "(Appuyez sur n'importe quelle touche pour continuer)":
        "Pressione qualquer tecla",
    "Toute ma vie, j'ai cru avoir devant moi un champ des possibles infini.":
        "Sempre acreditei ter diante de mim uma infinidade de mundos possíveis.",
    "\"L'univers entier m'appartient\", pensais-je.":
        "\"O universo pertence-me\", pensei.",
    "J'ai le choix.":
        "Posso escolher.",
    "Je suis maître de mon destin.":
        "Controlo o meu destino.",
    "Je peux prendre ce qui me plaît.":
        "Sou o dono do mundo.",
    "Je deviendrai ce que je veux.":
        "Posso ser aquilo que eu quiser.",
    "J'ai tracé mon propre chemin.":
        "Segui o meu próprio caminho.",
    "J'ai parcouru de magnifiques paysages.":
        "Contemplei as mais belas paisagens.",
    "Quoi de plus naturel, je les avais choisis.":
        "Não admira, pois fui eu quem as escolheu.",
    "Mais depuis un moment, j'ai des doutes.":
        "Mas, nos últimos tempos, vivo assaltado por dúvidas.",
    "Comment avoir prise sur ce qui m'arrive ?":
        "Como posso controlar o que me acontece?",
    "Tout s'échappe.":
        "Tudo me escapa.",
    "Me glisse entre les doigts.":
        "Por entre os dedos.",
    "Les objets, les personnes.":
        "Objectos, pessoas.",
    "J'ai l'impression de ne plus rien contrôler.":
        "Sinto que perdi o controlo.",
    "Depuis quelques temps maintenant,":
        "Há já algum tempo,",
    "Je n'attends qu'une chose.":
        "Espero apenas por uma coisa.",
    "La suite.":
        "O que virá a seguir.",

    "Et le rendez-vous est arrivé.":
        "Chegou a hora da reunião.",

    "Mais le rendez-vous était biaisé.":
        "Mas a reunião foi cancelada.",
    "Je ne m'en suis rendu compte que beaucoup plus tard.":
        "Só me apercebi depois.",
    "La femme en face de moi, qui paraissait si parfaite, me laissait bouche bée.":
        "À minha frente, uma mulher absolutamente perfeita. Fiquei sem palavras.",
    "Impossible de prononcer quelque chose de cohérent.":
        "Não conseguia dizer nada com coerência.",
    "Sa présence me bouleversait...":
        "Estava perturbado.",
    "Il fallait que je pose des questions pour la mettre à jour.":
        "Tive de lançar perguntas para descobrir algo sobre ela.",
    "Sans que je m'en aperçoive, cette inconnue devenait ma femme.":
        "Sem que me tenha apercebido disso, esta desconhecida casou comigo.",
    "On a tout partagé.":
        "Partilhávamos tudo.",
    "Mais jamais je ne suis parvenu à vraiment la connaître.":
        "Mas nunca cheguei a conhecê-la verdadeiramente.",
    "Aujourd'hui encore je me pose des questions.":
        "Ainda hoje me pergunto.",
    "Qui d'elle ou moi suit l'autre ?":
        "Quem segue quem?",
    "Quand je l'aime, elle me sème.":
        "Quando a amo, ela escapa-se-me.",
    "Qui êtes-vous ?":
        "Quem és?",
    "Vous aimez...":
        "Gostas de...",
    "Que pensez-vous...":
        "O que pensas acerca de...",
    "D'où venez-vous ?":
        "De onde és?",
    "Où allez-vous ?":
        "Para onde vais?",
    "Vous pensez quoi de...":
        "Achas que...",
    "Vous habitez la région depuis longtemps ?":
        "Vives aqui há muito tempo?",
    "Vous évitez la légion depuis longtemps ?":
        "Vindes aqui com muito tento?",
    "Et vous travaillez dans quoi ?":
        "Qual o teu trabalho?",
    "Et vous travaillez l'envoi ?":
        "Qual o teu atrapalho?",
    "Je vous trouve vraiment très jolie !":
        "Acho-te muito gira!",
    "Chevaux, brousse, bêlement... près jolis":
        "Acho a multa gira!",
    "J'ai l'impression qu'on a beaucoup de points communs":
        "Sinto que temos muito em comum",
    "Chemins pression en Allemagne point comme un...":
        "Sinto que temos muito comuna",
    "Vous avez des yeux somptueux":
        "Tens uns olhos lindos",
    "Vous avouez des notions de tueurs":
        "Tens uns molhos lindos",
    "Vous venez souvent ici ?":
        "Vens cá muitas vezes?",
    "Vous avez l'absent acquis":
        "Vens-te cá muitas vezes?",
    "Puis-je vous offrir un autre verre ?":
        "Posso ir buscar-te mais uma bebida?",
    "Pigeon ouïr un Notre Père ?":
        "Posso ir buscar-te mais uma vida?",
    "J'aime votre façon de sourire":
        "Gosto da maneira como sorris.",
    "Gêne, votre garçon mourir":
        "Gosto da bandeira como só ris",
    "Vous voulez marcher un peu ?":
        "Vamos dar um passeio?",
    "Nouveaux-nés barges et il pleut.":
        "Vamos doar o teu seio?",

    "Vingt ans se sont écoulés depuis notre rencontre.":
        "Passaram vinte anos desde que nos conhecemos.",
    "Ce matin, je me perds dans un mot qu'elle m'a laissé.":
        "Esta manhã, estou a ler uma nota que ela me deixou.",
    "Tout se brouille dans mon esprit.":
        "Sinto-me perdido.",
    "Je ne sais comment l'interpréter.":
        "Não sei o que fazer disto.",
    "Mot d'amour ou de rupture ?":
        "Carta de amor ou de despedida?",
    "Le fait-elle exprès ?":
        "O que hei de fazer?",
    "Je sais que c'est pour toi un choc":
        "Sei que isto será um choque para ti",
    "Je n'ai que de l'amour pour toi":
        "Digo que te amo",
    "Est un mensonge, et":
        "É uma mentira, e",
    "Dans un couple, il y en a un qui souffre et un qui s'ennuie":
        "\"Num casal, há sempre alguém que sofre e alguém que se aborrece\"",
    "Je veux que tous nos amis sachent que":
        "Quero que todos os nossos amigos saibam",
    "Je ne veux pas rester avec toi,":
        "Não quero ficar contigo",
    "Depuis le premier jour, je ne sais pas comment tu peux croire que":
        "Desde o primeiro dia, perguntei-me como poderias ter pensado que",
    "Je t'aime":
        "Gosto de ti",
    "Mon amour":
        "Meu amor",
    "A disparu":
        "Desapareceu",
    "L'indifférence":
        "A indiferença",
    "Est plus vivace que jamais":
        "É mais real do que nunca",
    "Le charme de notre rencontre":
        "A beleza do nosso encontro",
    "S'est dissipé à présent":
        "Desapareceu",
    "Et le moindre malentendu":
        "O mais pequeno desentendimento",
    "A vaincu":
        "Venceu",
    "Notre amour":
        "O nosso amor",
    "Je n'ai pas de héros. D'aussi loin qu'il m'en souvienne, et même après une longue réflexion, je n'ai jamais eu de héros. La figure du héros ne me séduit pas. Sans doute parce que je ne préfère pas une qualité à une autre, une valeur morale à une autre. Les héros, je les connais, je les reconnais, mais je n'éprouve ni adoration, ni idolâtrie pour eux. À dire vrai, le fanatisme me rend fou. Si l'on considère que le héros obtient son titre par ses actions, son statut est donc une forme de récompense pour la prouesse, la hardiesse, l'originalité. Mais que reste-t-il à la personne, une fois l'acte terminé ? Rien, sinon le titre. On peut présumer que le héros retire de son action une aura : l'action brille à travers lui. J'ai tendance à croire, que l'oeuvre — l'action selon les domaines — doit quitter son créateur pour pouvoir vivre pleinement. Les enfants-livres des auteurs vont eux-mêmes tracer leur chemin, se cognant à l'occasion à quelques Zoïles.":
        "Não tenho heróis. Tanto quanto me posso lembrar, e mesmo depois de muito pensar, a verdade é que nunca tive heróis. A figura do herói não me diz nada. Talvez porque não troco nenhuma qualidade por outra, nenhum valor moral por qualquer outro. Conheço alguns heróis, consigo reconhecê-los, mas não os amo nem os admiro. Para dizer a verdade, detesto fanáticos. Se o que constitui um herói são as suas acções, então, o seu mérito é uma recompensa pelos seus feitos, as suas acções heróicas, a sua singularidade. Mas o que resta, uma vez terminados os feitos heróicos? Nada, a não ser o seu título. Podemos assumir que um herói conserva uma aura: a acção brilha através dele. Tendo a acreditar que, para que possa ter vida própria, o feito deve libertar-se do seu criador. Os discípulos de um autor encontrarão o seu próprio público, ainda que encontrem aqui e acolá algumas críticas adversas.",
    "Je ne t'aime pas.":
        "Não gosto de ti.",
    "Tu ne me connais pas.":
        "Não me conheces.",
    "Nous n'avons rien en commun.":
        "Não temos nada em comum.",
    "Je ne veux rien de toi.":
        "Não quero nada que venha de ti.",
    "Tu n'es pas un modèle pour moi.":
        "Não és um modelo a seguir.",
    "Je veux voler de mes propres ailes.":
        "Quero seguir o meu próprio caminho.",
    "Bientot je partirai.":
        "Partirei em breve.",

    "Si ce n'était qu'elle, je pourrais l'accepter.":
        "Da minha mulher posso aguentá-lo.",
    "Mais mon fils dispose des mêmes armes.":
        "Mas como pode o meu filho fazer-me isto?",
    "Il voudrait mon avis sur sa rédaction. ":
        "Quer que leia o seu artigo. ",
    "Mais je ne parviens pas à me concentrer sur le texte.":
        "Mas não me consigo concentrar nas palavras.",
    "Étrange impression de ne pouvoir lire qu'entre les lignes…":
        "Por que razão só consigo ler nas entrelinhas.",

    "Suis-je si peu présent ?":
        "Serei assim tão insignificante?",
    "Si modelable ?":
        "Assim tão deformado?",
    "Ma propre image semble me fuir.":
        "Escapa-se-me a minha própria imagem.",
    "Elle m'échappe.":
        "Falha-me.",
    "Je me sens manipulé.":
        "Sinto-me manipulado.",

    "Il est temps de reprendre le contrôle.":
        "É tempo de ganhar de novo o controlo.",
    "Arrêter de tourner en rond.":
        "Parar de andar em círculos.",

    "Retrouver une direction.":
        "Moldar os acontecimentos.",
    "Plier le cours des événements.":
        "Dar sentido às minhas acções.",
    "Donner un sens à mes actions.":
        "Parar de andar em círculos.",


    "Je fais tout pour maîtriser de nouveau le cours de ma vie.":
        "Faço tudo o que posso ter controlo de novo sobre a minha vida.",
    "Je choisis.":
        "Faço escolhas.",
    "Mes émotions.":
        "Controlo as minhas emoções.",
    "Le sens à donner aux choses.":
        "O significado das coisas.",
    "Enfin, je me suis repr":
        "Por fim, tenho o contro",
    "fin":
        "Fim",

    // fichiers sonores
    // chp1

    "assets/sound/chapitre1/BienvenueAppuyez.mp3":
        "assets/sound/chapitre1/BienvenueAppuyez-pt.mp3",
    "assets/sound/chapitre1/BipEtBravo.mp3":
        "assets/sound/chapitre1/BipEtBravo-pt.mp3",
    "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous.mp3":
        "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous-pt.mp3",
    "assets/sound/chapitre1/BravoVotreRendezVousEstArrive.mp3":
        "assets/sound/chapitre1/BravoVotreRendezVousEstArrive-pt.mp3",

    // chp2
    "assets/sound/chapitre2/julien1.mp3":
        "assets/sound/chapitre2/PT_man_1.mp3",
    "assets/sound/chapitre2/julien2.mp3":
        "assets/sound/chapitre2/PT_man_2.mp3",
    "assets/sound/chapitre2/julien3.mp3":
        "assets/sound/chapitre2/PT_man_3.mp3",
    "assets/sound/chapitre2/julien4.mp3":
        "assets/sound/chapitre2/PT_man_4.mp3",
    "assets/sound/chapitre2/julien5.mp3":
        "assets/sound/chapitre2/PT_man_5.mp3",
    "assets/sound/chapitre2/julien6.mp3":
        "assets/sound/chapitre2/PT_man_6.mp3",
    "assets/sound/chapitre2/julien7.mp3":
        "assets/sound/chapitre2/PT_man_7.mp3",
    "assets/sound/chapitre2/julien8.mp3":
        "assets/sound/chapitre2/PT_man_8.mp3",
    "assets/sound/chapitre2/julien9.mp3":
        "assets/sound/chapitre2/PT_man_9.mp3",

    // chp4


    "assets/sound/chapitre4/maximilienEssay.mp3":
        "assets/sound/chapitre4/maximilienEssay-pt.mp3",
    "assets/sound/chapitre4/max-jenetaimepas.mp3":
        "assets/sound/chapitre4/jenetaimepas-pt.mp3",
    "assets/sound/chapitre4/max-tunemeconnaispas.mp3":
        "assets/sound/chapitre4/tunemeconnaispas-pt.mp3",
    "assets/sound/chapitre4/max-nousnavonsrienencommun.mp3":
        "assets/sound/chapitre4/nousnavonsrienencommun-pt.mp3",
    "assets/sound/chapitre4/max-jeneveuxriendetoi.mp3":
        "assets/sound/chapitre4/jeneveuxriendetoi-pt.mp3",
    "assets/sound/chapitre4/max-tunespasunmodelepourmoi.mp3":
        "assets/sound/chapitre4/tunespasunmodelepourmoi-pt.mp3",
    "assets/sound/chapitre4/max-jeveuxvolerdemespropresailes.mp3":
        "assets/sound/chapitre4/jeveuxvolerdemespropresailes-pt.mp3",
    "assets/sound/chapitre4/max-bientotjepartirai.mp3":
        "assets/sound/chapitre4/bientotjepartirai-pt.mp3"




};
