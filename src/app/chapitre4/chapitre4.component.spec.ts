import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Chapitre4Component } from './chapitre4.component';

describe('Chapitre4Component', () => {
  let component: Chapitre4Component;
  let fixture: ComponentFixture<Chapitre4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Chapitre4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Chapitre4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
