import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Observable, Subscription } from 'rxjs/Rx';
import ShuffleText from 'shuffle-text';
import { TranslateService } from '../translate';

@Component({
    selector: 'app-chapitre4',
    templateUrl: './chapitre4.component.html',
    styleUrls: ['./chapitre4.component.css'],
    providers: [TranslateService],
    encapsulation: ViewEncapsulation.None
})
export class Chapitre4Component implements OnInit {

    constructor(private _translate: TranslateService) { 
        this.lang = _translate.currentLang;
    }

    Phrase: string[] = ["4",
        "Si ce n'était qu'elle, je pourrais l'accepter.",
        "Mais mon fils dispose des mêmes armes.",
        "Il voudrait mon avis sur sa rédaction.",
        "Mais je ne parviens pas à me concentrer sur le texte.", //lancement du son maximilienEssai.mp3
        "Étrange impression de ne pouvoir lire qu'entre les lignes…",
        //"Mais je ne parviens pas à me concentrer sur le texte."
        ""
    ] //Après trois explosions sur le texte, on transforme cette phrase en lien vers le chapitre 5
    lang;
    son = "assets/sound/chapitre4/maximilienEssay.mp3";
    sonMoment: string;

    ticks = 0; //compteur de secondes ecoulees depuis le lancement du chapitre4, comme dans le chapitre 1
    ticksPhrase: number; //seconds ecoulees depuis l'affichage de la derniere phrase
    description = this.Phrase[0];

    //tableau dont la valeur i indique si la Phrase[i] est affichee ou non
    //(pratique pour eviter d'afficher la meme phrase sur deux passages de la souris
    // d'affilée par exemple). La premiere valeur est ici a true car le chapitre 4
    //commence directement avec l'affichage d'un 4
    afficherPhrase: boolean[] = [true,
        false,
        false,
        false,
        false,
        false]

    rienAfficher = false;
    j = 1; //j parcours de tableau comme dans le chapitre 1
    //commence ici a 1 car les hover commencer a partir de Phrase[1] dans ce chapitre

    //besoin de ces deux attributs pour faire fonctionner le timer
    private timer;
    private sub: Subscription;

    margintop: number;

    //entier permettant de changer la taille du 2 mais pas des autres textes du chapitre
    fontsize: number = 400; //tout de suite initialises car le 2 est le premier texte affiche dans le chapitre
    top: number = 10; //pour recentrer le 2 sur la page après l'avoir agrandi

    ngOnInit() {
        this.son = this._translate.instant("assets/sound/chapitre4/maximilienEssay.mp3");

        this.sonsSrc = [ // son des remarques
            this._translate.instant("assets/sound/chapitre4/max-jenetaimepas.mp3"),
            this._translate.instant("assets/sound/chapitre4/max-tunemeconnaispas.mp3"),
            this._translate.instant("assets/sound/chapitre4/max-nousnavonsrienencommun.mp3"),
            this._translate.instant("assets/sound/chapitre4/max-jeneveuxriendetoi.mp3"),
            this._translate.instant("assets/sound/chapitre4/max-tunespasunmodelepourmoi.mp3"),
            this._translate.instant("assets/sound/chapitre4/max-jeveuxvolerdemespropresailes.mp3"),
            this._translate.instant("assets/sound/chapitre4/max-bientotjepartirai.mp3"),
        ]

        this.redaction = this._translate.instant("Je n'ai pas de héros. D'aussi loin qu'il m'en souvienne, et même après une longue réflexion, je n'ai jamais eu de héros. La figure du héros ne me séduit pas. Sans doute parce que je ne préfère pas une qualité à une autre, une valeur morale à une autre. Les héros, je les connais, je les reconnais, mais je n'éprouve ni adoration, ni idolâtrie pour eux. À dire vrai, le fanatisme me rend fou. Si l'on considère que le héros obtient son titre par ses actions, son statut est donc une forme de récompense pour la prouesse, la hardiesse, l'originalité. Mais que reste-t-il à la personne, une fois l'acte terminé ? Rien, sinon le titre. On peut présumer que le héros retire de son action une aura : l'action brille à travers lui. J'ai tendance à croire, que l'oeuvre — l'action selon les domaines — doit quitter son créateur pour pouvoir vivre pleinement. Les enfants-livres des auteurs vont eux-mêmes tracer leur chemin, se cognant à l'occasion à quelques Zoïles.");

        this.Phrase = ["4",
            this._translate.instant("Si ce n'était qu'elle, je pourrais l'accepter."),
            this._translate.instant("Mais mon fils dispose des mêmes armes."),
            this._translate.instant("Il voudrait mon avis sur sa rédaction. "),
            this._translate.instant("Mais je ne parviens pas à me concentrer sur le texte."), //lancement du son maximilienEssai.mp3
            this._translate.instant("Étrange impression de ne pouvoir lire qu'entre les lignes…")
        ] //Après trois explosions sur le texte, on transforme cette phrase en lien vers le chapitre 5

        this.phrases = [ // remarques du fils
            this._translate.instant("Je ne t'aime pas."),
            this._translate.instant("Tu ne me connais pas."),
            this._translate.instant("Nous n'avons rien en commun."),
            this._translate.instant("Je ne veux rien de toi."),
            this._translate.instant("Tu n'es pas un modèle pour moi."),
            this._translate.instant("Je veux voler de mes propres ailes."),
            this._translate.instant("Bientot je partirai.")
        ]

        this.timer = Observable.timer(0, 1000);
        this.sub = this.timer.subscribe((t) =>
            this.tickerFunc(t)
        );


        for (let i = 0; i < 150; i++) {
            this.table[i] = [];
        }


        let cbody = document.body;
        let chtml = document.documentElement;


        this.audio = document.getElementById("audio");

        // initialisation des rebords du cadre
        this.MAX_X = document.body.clientWidth; // rebord droit
        var body = document.body,
            html = document.documentElement;
        this.MAX_Y = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight); // rebord bas
        this.MIN_X = 0; // rebord rebord gauche
        this.MIN_Y = 30; //rebord haut
        switch (this.lang) {
            case "fr" : {
                this.maxLength = 100;
                break;
            }
            case "it" : {
                this.maxLength = 100;
                break;
            }
            case "en" : {
                this.maxLength = 100;
                break;
            }
            case "ar": {
                this.maxLength = 95;
                break;
            }
            case "zh": {
                this.maxLength = 105;
                break;
            }
            case "ru": {
                this.maxLength = 90;
                break;
            }
            default: {
                this.maxLength = 95;
            }
        }
    }

    tickerFunc(tick) {
        this.ticks = tick;
        //On affiche le 4 pendant 2 secondes, puis la premiere phrase du tableau
        //et on indique dans le tableau de booleens que la phrase 1 est affichee
        //et que la phrase 0 ne l'est plus
        if (this.ticks >= 3 && this.afficherPhrase[0]) {
            this.description = this.Phrase[1];
            //On remet le texte a une taille correcte apres l'agrandissement du numero de chapitre
            this.fontsize = 25;
            this.top = 90;
            this.afficherPhrase[0] = false;
            this.afficherPhrase[1] = true;
            //Des que le numéro de chapitre est affiche au centre de la page
            //On affiche les phrases du narrateur bien plus bas sur la page
            this.margintop = 90;
        }
    }

    changementInterdit(i: number): boolean {
        //pas de changement s'il s'est ecoule moins de deux secondes depuis le dernier affichage de phrase
        if (this.ticks - this.ticksPhrase < 1) {
            //console.log("CHANGEMENT INTERDIT");
            return true;
        } else if (this.j == 4) {
            //si j vaut 4 (lors de la phrase "mais je ne parviens pas..."),
            //le changement n'est autorise que si le boolen changementAutorise
            //vaut true
            if (this.changementAutorise) {
                return false;
            } else {
                return true;
            }
        } else {
            //pour les autres valeurs de j, le changement est autorise
            return false;
        }
    }
    //Changement de texte au passage de la souris sur celui-ci
    phraseSuivante() {
        if (!this.changementInterdit(this.j)) {
            //juste avant l'affichage de la phrase "mais je n'arrive pas..."
            //on lance le wordwrapper
            if (this.j == 3) { // si on est sur la bonne phrase, lance l'affichage de la rédaction
                this.sonMoment = this.son;
                addEventListener("keydown", (event: KeyboardEvent) => { this.skip() });
                setTimeout(() => { this.wordWrapper('container', this.redaction, 16); }, 1000);
                // le lancement est différé pour permettre le chargement du conteneur par la script
                // (on ne va pas attendre la fin de la rédaction pour afficher la phrase suivante)
                //this.wordWrapper('container', this.redaction, 16);
            }

            if (!this.afficherPhrase[0]) {
                var el = document.getElementById("chap4");
                var shuffle = new ShuffleText(el);
                shuffle.start();
            }

            if (this.afficherPhrase[this.j]) {

                //On autorise le passage de la phrase suivante en changeant le booleen correspondant
                if ((this.j) + 1 < this.Phrase.length) {
                    this.afficherPhrase[(this.j) + 1] = true;
                    this.description = this.Phrase[(this.j) + 1];
                    //Etant donne qu'on a change de texte, il faut prevenir le Shuffle
                    //qu'on met a jour le texte a melanger. Pour cela, on arrete le Shuffle
                    //en cours et on en relance un nouveau avec le nouveau contenu de la balise html
                    shuffle.stop();
                    el.innerHTML = this.description;
                    shuffle = new ShuffleText(el)
                    shuffle.start();
                }

                //La phrase i n'étant plus affichee, on met son booleen a false pour
                //eviter de le reafficher lors du hover suivant
                this.afficherPhrase[this.j] = false;
                //on incrémente j pour permettre le changement de phrase au hover suivant
                this.j = (this.j) + 1;
                //sauvegarde du nombre de secondes a l'affichage de la phrase
                //pour savoir quand on va autoriser l'affichage de la phrase suivante
                this.ticksPhrase = this.ticks;
                //Des l'affichage de "Je ne parviens pas a me concentrer sur le texte", on
                //lance le son "je n'ai pas de heros"
            }
        }
    }

    /*debut de la partie du code consacree a la redaction */
    /* variables globales de gestion de l'environnement */
    v0init = .8; // vitesse de départ de l'explosion
    STEP = 40; // pas de calcul de l'explosion (en ms)
    explodeTime = 3000; // durée totale de l'explosion
    waitTime = 0; // durée d'attente avant le retour des caractères
    comeBackTime = 2000; // temps de retour pour les caractères
    compensationTime = 1500; // temps pour compenser le temps de calcul entre les setTimeout
    NB_STEPS = this.comeBackTime / this.STEP; // nombre d'étapes pour le retour des lettres à leur place
    acceleration = -this.v0init / (1.8 * this.explodeTime); // accélération à v0 + dt
    maxLength = 100; // longueur maximale d'une ligne (en caractères)
    MAX_HIGH = 2; // nombre de lignes prises dans l'explosion
    MAX_DIST = 50; // taille du cercle de l'explosion
    rudnessWritten = false; // indique si une remarque du fils est affichée
    skipAnimation = false; // indique si l'utilisateur souhaite éviter l'explosion
    TIME_BTW_LETTERS = 40; // temps entre l'affichage de chaque lettre
    TIME_BTW_LETTERS_ARABE = 200; // temps entre l'affichage de chaque lettre pour l'arabe
    resetLineTime = 1000; // temps d'attente pour remettre la ligne au lieu des lettres

    MIN_X;
    MAX_X;
    MIN_Y;
    MAX_Y;

    redaction = "Je n'ai pas de héros. D'aussi loin qu'il m'en souvienne, et même après une longue réflexion, je n'ai jamais eu de héros. La figure du héros ne me séduit pas. Sans doute parce que je ne préfère pas une qualité à une autre, une valeur morale à une autre. Les héros, je les connais, je les reconnais, mais je n'éprouve ni adoration, ni idolâtrie pour eux. À dire vrai, le fanatisme me rend fou. Si l'on considère que le héros obtient son titre par ses actions, son statut est donc une forme de récompense pour la prouesse, la hardiesse, l'originalité. Mais que reste-t-il à la personne, une fois l'acte terminé ? Rien, sinon le titre. On peut présumer que le héros retire de son action une aura : l'action brille à travers lui. J'ai tendance à croire, que l'oeuvre — l'action selon les domaines — doit quitter son créateur pour pouvoir vivre pleinement. Les enfants-livres des auteurs vont eux-mêmes tracer leur chemin, se cognant à l'occasion à quelques Zoïles";
    // texte initial


    nbLines = 10; // nombre de Lignes affichées ( doit être supérieur à 0 initialement, sera set plus tard correctement)
    wasReset = true; // indique que l'affichage est clean

    phrases: string[] = [ // remarques du fils
        "Je ne t'aime pas",
        "Tu ne me connais pas",
        "Nous n'avons rien en commun",
        "Je ne veux rien de toi",
        "Tu n'es pas un modèle pour moi",
        "Je veux voler de mes propres ailes",
        "Bientôt je partirai"
    ]

    sonsSrc: string[] = [ // son des remarques
        "assets/sound/chapitre4/max-jenetaimepas.mp3",
        "assets/sound/chapitre4/max-tunemeconnaispas.mp3",
        "assets/sound/chapitre4/max-nousnavonsrienencommun.mp3",
        "assets/sound/chapitre4/max-jeneveuxriendetoi.mp3",
        "assets/sound/chapitre4/max-tunespasunmodelepourmoi.mp3",
        "assets/sound/chapitre4/max-jeveuxvolerdemespropresailes.mp3",
        "assets/sound/chapitre4/max-bientotjepartirai.mp3",
    ]




    displayText(lineNumber = 0) {
        // affiche le texte avec l'animation
        let lineNumberStr = String(lineNumber);
        let line: any = document.getElementById(lineNumberStr);
        if (line) {
            // lance l'affichage d'une ligne si celle-ci existe (géré par popLetter)
            if (lineNumber == this.nbLines) {
                // si on est sur la dernière ligne, affiche Il famoso point quelques pixels en dessous
                let pointSpan = document.createElement("span");
                let lastDisplayedLine = document.getElementById(String(this.nbLines - 4)); // on prend nbLines-4 pour avoir une ligne affichée et donc des coordonnées différentes de 0
                let linePos = lastDisplayedLine.getBoundingClientRect();
                let left = linePos.left + 550;
                let top = linePos.top + 200;
                pointSpan.style.position = "fixed";
                pointSpan.style.top = top + 'px';
                pointSpan.style.left = left + 'px';
                pointSpan.innerText = String(".");
                document.getElementById("container").appendChild(pointSpan);
            }
            this.letterWrapper(line.parentElement, line.innerText, line.innerText.length, lineNumber, true);
            this.popLetter(0, lineNumber);
        }
    }



    popLetter(x: number, y: number) {
        // affiche une lettre avec une zolie transition
        // console.log("MEME LA PROD ELLE SAIT PASSER PAR POPLETTER")
        if (!this.skipAnimation) {
            let elt = this.table[x][y];
            if (y > this.nbLines) {
            }
            else if (elt) { //si elt a bien permis de prendre un element de table, alors
                //on prend ses coordonnees sur la page dans left et top
                let left = elt.style.left;
                let top = elt.style.top;
                let left0: number;
                let top0: number;
                let rand = Math.floor(Math.random() * 3);
                //choix du bord ou on veut faire apparaitre la lettre avant qu'elle rejoigne
                //sa bonne position dans le texte (sans le bord haut)
                switch (rand) {
                    case 0: left0 = this.MIN_X - 10;
                        top0 = Math.random() * (this.MIN_Y + this.MAX_Y) - this.MIN_Y;
                        break;
                    case 1: left0 = this.MAX_X;
                        top0 = Math.random() * (this.MIN_Y + this.MAX_Y) - this.MIN_Y;
                        break;
                    case 2: top0 = this.MAX_Y;
                        left0 = Math.random() * (this.MIN_X + this.MAX_X) - this.MIN_X;
                        break;
                }
                let newLetter = elt;
                newLetter.style.position = "fixed";
                let deg = 180;
                newLetter.style.transform = 'rotate(' + deg + 'deg)';
                newLetter.style.position = "fixed";
                newLetter.innertext = elt.innertext;
                newLetter.style.top = top0 + "px";
                newLetter.style.left = left0 + "px";
                newLetter.style.display = "inline";


                //				 newLetter.animate([], { duration: 1000});
                //le moment ou les lettres de la scene 4 partent des 4 coins de l'ecran pour former le texte au debut

                newLetter.classList.add("horizTranslate"); // horizTranslate -> classe qui contient les transitions
                //newLetter.style.refresh(); // on force le rechargement du CSS pour que la transition se fasse
                //console.log("MEME LA PROD ELLE SAIT PASSER PAR LA LIGNE AVANT HORIZTRANSLATER")

                //newLetter.offsetHeight;
                newLetter.top = newLetter.offsetHeight;

                //newLetter.Height;
                //console.log ("ET LA OFFSET HEIGHT VAUT " + newLetter.offsetHeight);

                newLetter.style.top = top;
                newLetter.style.left = left;
                newLetter.style.transform = 'rotate(360deg)';

                // affichage de la lettre suivante
                if (this.lang == "ar") {
                    setTimeout(() => { this.popLetter(x + 1, y); }, this.TIME_BTW_LETTERS_ARABE);
                } else {
                    setTimeout(() => { this.popLetter(x + 1, y); }, this.TIME_BTW_LETTERS); // on affiche la lettre suivante après un intervalle predefini
                }
            } else {
                // si il n'y a plus de lettres à afficher, on remplace toutes les lettres par un span qui contient la ligne et on lance l'affichage de la ligne suivante
                setTimeout(() => {
                    this.resetLine(y);
                }, this.resetLineTime);

                this.displayText(y + 1);
            }
        }
    }

    wordWrapper(id, str, maxlength) {
        // Divise la rédaction en lignes de taille à peu près équivalentes et génère les spans qui y correspondent
        let elt = document.getElementById(String(id));
        let words;
        if (this.lang == 'zh') {
            words = str.split("");
        } else {
            words = str.split(" "); //on prend tous les mots dans un tableau de mots
        }
        let i;
        let y = 0;
        let curLine;
        let lineLength = 0;
        let myBreak;
        let mySpan;
        
        for (i = 0; i < words.length; i++) { // on ajoute les mots un par un dans la ligne jusqu'à avoir la bonne taille
            let z = y;
            //si la ligne n'a pas deja ete entamee ou si la ligne actuelle est trop longue,
            //on cree une nouvelle ligne
            if (lineLength == 0 || lineLength > this.maxLength - 20) { // 20 est arbitraire (taille d'une ligne)
                curLine = document.createElement('span');
                myBreak = document.createElement('br');
                // myspan contient une ligne des mots
                mySpan = document.createElement('span');
                curLine.setAttribute('class', 'line');
                elt.appendChild(curLine);
                curLine.appendChild(mySpan);
                elt.appendChild(myBreak);
                mySpan.style.display = "none";
                mySpan.addEventListener('click', (event: MouseEvent) => {
                    this.primalExplode(z, event);
                });
                mySpan.id = y;
                y++;
            }
            if (this.lang == 'zh') {
                mySpan.innerHTML += words[i];
                lineLength = mySpan.innerHTML.length * 2.4;
            }
            else {
                mySpan.innerHTML += words[i] + ' ';
                lineLength = mySpan.innerHTML.length;
            }
        }
        let j;
        this.nbLines = y;
        for (i = y; i >= 0; i--) { // on passe l'affichage des phrases en position absolue (en commençant par le bas pour éviter le replacement auto) le replacement n'est peut être pas auto, à vérifier
            for (j = str.length; j >= 0; j--) {
                let letter = document.getElementById(j + '.' + i);
                if (letter) {
                    let left = letter.getBoundingClientRect().left;
                    let top = letter.getBoundingClientRect().top;
                    letter.style.position = "fixed";
                    letter.style.top = top + "px";
                    letter.style.left = left + "px";
                }
            }
        }
        // lance l'apparition
        this.displayText();
    }


    skip() {
        this.wasReset = false;
        this.resetDisplay();
        this.skipAnimation = true;
    }

    table = []; // table contient l'ensemble des spans des lettres ( pour eviter les accès DOM)

    /**
     * on divise une ligne en lettres, même fonctionnement que wordWrapper
     * @param elt parentElement de la ligne
     * @param str texte de la ligne
     * @param maxlength 
     * @param y index de la ligne
     * @param hideLetter utile pour le premier affichage seulement
     */
    letterWrapper(elt, str, maxlength, y, hideLetter = false) {
        // on divise une ligne en lettres, même fonctionnement que wordWrapper
        // hideletter est utile pour le premier affichage seulement
        let i;
        let mySpan;
        if (this.lang == "ar") {
            str = str.split(' ');
        }
        for (let x = 0; x < str.length; x++) {
            mySpan = document.createElement('span');
            if (this.lang == "ar") {
                mySpan.setAttribute("style", "float:right;")
                mySpan.innerHTML = str[x] + "&nbsp;";
            } else {
                mySpan.innerHTML = str[x];
            }
            this.table[x][y] = mySpan;
            // si langue est l'arabe, on commence a droite
            elt.appendChild(mySpan);
        }
        for (i = str.length; i >= 0; i--) {
            let letter = this.table[i][y];
            if (letter && letter.innerHTML) {
                let left = letter.getBoundingClientRect().left;
                let top = letter.getBoundingClientRect().top;
                letter.style.position = "fixed";
                letter.style.top = top + "px";
                letter.style.left = left + "px";
                if (hideLetter == true) { letter.style.display = "none"; }
            }
        }
    }

    nbTrigger = 0; // nombre de fois que le texte a explosé (3 min pour passer à la suite)
    changementAutorise = false; // dis si l'on peut passer de j=4 à j=5
    audio: any // pointeur sur la source audio

    /**
     * gère la réaction à 1 clic:
     * @param y index de ligne
     * @param ev event
     */
    primalExplode(y, ev) {

        if (this.wasReset) { // la fonction d'explosion ne fonctionne que si l'affichage est clean (pas de lettres qui se balade)
            this.wasReset = false;
            this.nbTrigger++;
            if (this.nbTrigger == 3 && this.j == 4) {
                this.changementAutorise = true;
                this.phraseSuivante();
            }
            let i;
            let j;
            let left0 = ev.pageX;
            let top0 = ev.pageY;
            let curLine;


            let n = Math.floor(Math.random() * this.phrases.length);

            let rudness = this.phrases[n]
            this.sonMoment = this.sonsSrc[n];
            this.audio.play();
            let MAX_DIST = rudness.length * 6 + 150;

            this.rudnessWritten = true;
            let rudnessSpan = document.createElement("span");
            rudnessSpan.id = "rudness";
            rudnessSpan.style.width = MAX_DIST - Math.pow((y-j),2)*15 + "px";
            rudnessSpan.style.position = "fixed";
            rudnessSpan.style.top = top0 - 10 +  "px";
            // offset de l'arabe est un peu different
            if (this.lang == "ar") {
                rudnessSpan.style.left = left0 - rudness.length * 3 + "px";
            } else {
                rudnessSpan.style.left = left0 - rudness.length * 3 - 30 + "px";
            }
            rudnessSpan.style.textAlign = "center";
            rudnessSpan.innerHTML = rudness;
            document.getElementById("container").appendChild(rudnessSpan);

            for (j = y - this.MAX_HIGH; j <= y + this.MAX_HIGH; j++) { // balayage en hauteur des lignes et wrapping sur les lettres
                curLine = document.getElementById(j);
                if (curLine) {
                    curLine.style.display = "none";
                    this.letterWrapper(curLine.parentElement, curLine.innerText, 300, j);
                }
                for (i = 0; i < this.maxLength; i++) {
                    let letter = this.table[i][j];
                    if (letter && letter.innerText) {
                        let left = letter.getBoundingClientRect().left;
                        let right = letter.getBoundingClientRect().right;
                        let top = letter.getBoundingClientRect().top;
                        let distX = left - left0;
                        let distY = top - top0;
                        let dist = Math.sqrt(Math.pow(distX, 2) + Math.pow(distY, 2)); // distance entre l'objet et l'épicentre
                        if (dist < MAX_DIST / 2 - Math.pow((y-j),2)*15) {
                            // if (j == y && !this.rudnessWritten) {
                            //     this.rudnessWritten = true;
                            //     let rudnessSpan = document.createElement("span");
                            //     rudnessSpan.id = "rudness";
                            //     rudnessSpan.style.width = MAX_DIST - Math.pow((y-j),2)*15 + "px";
                            //     rudnessSpan.style.position = "fixed";
                            //     rudnessSpan.style.top = top + "px";
                            //     rudnessSpan.style.left = left + Math.pow((y-j),2)*15+ "px";
                            //     rudnessSpan.style.textAlign = "center";
                            //     rudnessSpan.innerHTML = rudness;
                            //     document.getElementById("container").appendChild(rudnessSpan);
                            // }
                            // calcul de l'angle theta à impulser
                            let cos = distX / dist;
                            let sin = distY / dist;
                            let theta = Math.acos(cos) % (Math.PI * 2);
                            if (dist == 0) {
                                theta = Math.PI / 2;
                                cos = 0;
                                sin = 1;
                            }
                            if (sin < 0) {
                                theta = -theta;
                            } else if ((sin === 0)) {
                                theta += 0.1 * (Math.random() - 0.5);
                            }

                            theta += 0.2 * (Math.random() - 0.5);
                            this.explode(letter, theta, left, top, this.v0init, 0, 0, left, top);
                        }
                    }
                }
            }
        }
    }

    explode(elt, theta, left, top, v0, t0, t, leftInit, topInit) {
        // déplace une lettre avec une trajectoire de type v(t) = a0*t  et pos = v0*t + pos0


        // elt => l'element
        // theta => l'angle depuis le dernier rebond
        // left, top => la position du dernier rebond
        // v0 => la vitesse initiale
        // t0 => t au dernier rebond
        // t => le temps écoulé depuis le début de l'animation
        // leftInit, topInit => la position initiale de la lettre

        let v = this.acceleration * (t + this.compensationTime) + v0;
        let newValueY = - v * Math.sin(theta) * (t - t0) + top;
        let newValueX = v * Math.cos(theta) * (t - t0) + left;

        if (t < this.explodeTime) {
            let inBorders = (newValueX < this.MAX_X)
                && (newValueX > this.MIN_X)
                && (newValueY < this.MAX_Y)
                && (newValueY > this.MIN_Y);
            if (inBorders) {
                // si on est dans le cadre, on applique simplement la position
                elt.style.left = newValueX + 'px';
                elt.style.top = newValueY + 'px';
            }
            else {
                // sinon, on calcule un rebond
                let oldTheta = theta;
                if (newValueX > this.MAX_X) { // rebond sur bord droit
                    if (theta > 0) {
                        theta = Math.PI - theta % (Math.PI * 2); // rebond en venant d'en bas
                    } else {
                        theta = Math.PI * 1.5 + theta % (Math.PI * 2); // rebond en venant d'en haut
                    }
                }
                else if (newValueX < this.MIN_X) { // rebond sur bord gauche
                    if (theta > Math.PI) { // en venant d'en bas
                        theta = theta + (Math.PI / 2) % (2 * Math.PI)
                    }
                    else { // en venant d'en haut
                        theta = Math.PI - theta % (2 * Math.PI);
                    }
                }
                else { // rebond sur bord haut ou bas
                    theta = -theta % (Math.PI * 2);
                }

                left = v * Math.cos(oldTheta) * (t - this.STEP - t0) + left;
                top = - v * Math.sin(oldTheta) * (t - this.STEP - t0) + top;


                t0 = t;
                newValueY = - v * Math.sin(theta) * (t - t0) + top;
                newValueX = v * Math.cos(theta) * (t - t0) + left;
                elt.style.left = newValueX + 'px';
                elt.style.top = newValueY + 'px';
            }
            setTimeout(() => {
                this.explode(elt, theta, left, top, v0, t0, t + this.STEP, leftInit, topInit)
            }, this.STEP);
        }
        else {
            top = - v * Math.sin(theta) * (t - t0) + top;
            left = newValueX = v * Math.cos(theta) * (t - t0) + left;
            setTimeout(() => {
                this.comeBack(elt, left, top, leftInit, topInit, 0);
            }, this.waitTime);
        }
    }

    comeBack(elt, origLeft, origTop, destLeft, destTop, nStep) {
        // retour à la position initiale
        let newValueX;
        let newValueY;
        newValueX = origLeft + nStep * (destLeft - origLeft) / this.NB_STEPS;
        elt.style.left = newValueX + "px";
        newValueY = origTop + nStep * (destTop - origTop) / this.NB_STEPS;
        elt.style.top = newValueY + "px";
        if (nStep == Math.floor(this.NB_STEPS / 2) + 20 && this.rudnessWritten) {
            this.rudnessWritten = false;
            let rudness = document.getElementById("rudness");
            document.getElementById("container").removeChild(rudness);
        }
        if (nStep != this.NB_STEPS) {
            setTimeout(() => {
                this.comeBack(elt, origLeft, origTop, destLeft, destTop, nStep + 1)
            }, this.STEP);
        }
        else {
            this.resetDisplay();
        }
    }

    resetLine(y) {
        // reset l'affichage d'une ligne (détruit les lettres isolées et affiche la ligne en un bloc
        let letter;
        for (let i = 0; i < this.maxLength; i++) {
            letter = this.table[i][y];
            if (letter) {
                letter.parentElement.removeChild(letter);
                this.table[i][y] = null;
            }
        }
        let line = document.getElementById(y);
        if (this.lang == "ar") {
            line.style.cssFloat = "right";
        }
        line.style.display = "inline";
    }

    resetDisplay() {
        // reset l'affichage du texte en entier (détruit les lettres isolées et affiche la ligne en un bloc
        if (!this.wasReset) {
            // ne peut pas être appelé sur un écran déjà reset
            this.wasReset = true;

            // affichage du famoso point
            // let pointSpan = document.createElement("span");
            // let lastDisplayedLine = document.getElementById(String(this.nbLines - 4));
            // let linePos = lastDisplayedLine.getBoundingClientRect();
            // let left = linePos.left + 550;
            // let top = linePos.top + 200;
            // pointSpan.style.position = "fixed";
            // pointSpan.style.top = top + 'px';
            // pointSpan.style.left = left + 'px';
            // pointSpan.innerText = String(".");
            // document.getElementById("container").appendChild(pointSpan);
            // affichage du famoso point

            // destruction des lettres isolées
            for (let i = 0; i < this.maxLength; i++) {
                for (let j = 0; j < this.nbLines; j++) {
                    let letter = this.table[i][j];
                    if (letter && letter.innerHTML) {
                        letter.parentElement.removeChild(letter);
                        this.table[i][j] = null;
                    }
                }
            }
            // affichage des lignes
            for (let i = 0; i < this.nbLines; i++) {
                if (this.lang == "ar") {
                    document.getElementById(String(i)).style.cssFloat = "right";
                }
                document.getElementById(String(i)).style.display = "inline";
            }
        }
    }

}
