export interface Flatterie {
  phraseNormale: string;
  phraseTravers: string;
  son: string;
  id: number;
}
