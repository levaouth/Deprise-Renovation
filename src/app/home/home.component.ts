import { Component, OnInit, AfterViewInit } from '@angular/core';
import { TranslateService } from '../translate';
import ShuffleText from 'shuffle-text';
import { routes } from '../app.router';
@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {

    constructor(private _translate: TranslateService) { }

    ngOnInit() {
        if (this._translate.currentLang) {
            this.language = this._translate.currentLang
        } else {
            this.language = "fr";
        }
    }

    ngAfterViewInit() { 
        this.afficheDescription();
    }

    language: string;
    titleFrancais = "Déprise";
    descriptionFrancais = "Déprise est une création sur les notions de prise et perte de contrôle. Quand a-t-on l'impression d'être en situation de prise ou de perte de prise dans la vie? Six scènes racontent l'histoire d'un personnage en pleine déprise. Parallèlement, ce jeu sur prise et déprise permet de mettre en scène la situation du lecteur d'une oeuvre interactive.";
    // titleAnglais = "Loss of Grasp";
    // descriptionAnglais = "Loss of grasp is a digital creation about the notion of grasp. Under which circumstances do we feel we have a grip on our life or not? Six scenes feature a character who is losing grasp. At the same time, this play on grasp and loss of grasp mirrors the reader’s experience of an interactive digital work. ";
    // titleEspagnol = "Perderse";
    // descriptionEspagnol = "Perderse es una creación digital en torno a la pérdida de control de uno mismo. ¿Cuándo tenemos la impresión de estar al cargo o no de nuestra vida? En seis escenas se cuenta la historia de un personaje en plena crisis existencial. Paralelamente, este juego de control/pérdida permite escenificar la posición del lector dentro de una obra interactiva. ";
    // titleItalien = "Perdersi";
    // descriptionItalien = "Perdersi è un lavoro sulla perdita di controllo di sé. Cosa succede quando si ha l’impressione di avere il controllo della propria vita e poi di perdere questo controllo? Sei scene narrano la storia di un personaggio in piena crisi esistenziale. Parallelamente questo gioco di controllo/perdita di se stessi permette di mettere in scena la situazione del lettore di un’opera interattiva.";
    // titlePortugais = "Perda de controlo";
    // descriptionPortugais = "Perda de controlo é uma criação digital sobre a noção de (perda de) controlo. Em que circunstâncias sentimos que temos (ou não) controlo sobre a nossa vida? Seis cenas apresentam uma personagem em plena crise existencial. Simultaneamente, este jogo entre ter e perder o controlo espelha a experiência do leitor na relação com obra digital interactiva.";
    ambianceTitrePath = 'assets/sound/home/ambianceTitreSoft.mp3';

    fr_flag = "fr";
    es_flag = "es";
    hi_flag = "hi";
    en_flag = "en";
    it_flag = "it";
    pt_flag = "pt";
    de_flag = "de";
    hu_flag = "hu";
    pl_flag = "pl";
    ar_flag = "ar";
    zh_flag = "zh";
    ru_flag = "ru";

    countries: string[] = new Array(this.fr_flag, this.en_flag, this.es_flag, this.it_flag, this.pt_flag, this.de_flag, this.zh_flag, this.ar_flag, this.hu_flag, this.pl_flag, this.ru_flag, this.hi_flag);
    // countriesComingSoon: string[] = new Array();
    // title: string[] = new Array("Déprise", "Loss of Grasp", "Perderse", "Perdersi", "Perda de controlo", "Griffverlust", "deprise_hu", "deprise_pl", "失控","deprise_ar");

    linkDisable = false;

    changeLanguage(index: number) {
        // blur le drapeau focused en ce moment
        this.linkDisable = false;
        console.log(this.linkDisable);
        var flags = document.getElementsByClassName("flag-block");
        for (let flag of flags as any) {
            flag.blur();
        }
        // focus le drapeau hovered maintenant
        document.getElementById("flag" + index).focus();
        this.language = this.countries[index];
        this.afficheDescription();
    }

    afficheDescription() {
        this._translate.use(this.language)
        var elTitle = document.getElementById("title");
        var elDescription = document.getElementById("description");
        var shuffleTitle = new ShuffleText(elTitle);
        var shuffleDescription = new ShuffleText(elDescription);
        shuffleDescription.setText(this._translate.translate(this.descriptionFrancais));
        shuffleTitle.setText(this._translate.translate(this.titleFrancais));
        shuffleTitle.start();
        shuffleDescription.start();
    }

    comingSoon(index: number) {
        this.linkDisable = true;
        var flags = document.getElementsByClassName("flag-block");
        for (let flag of flags as any) {
            flag.blur();
        }
        document.getElementById("flagTemp" + index).focus();
        var elTitle = document.getElementById("title");
        var elDescription = document.getElementById("description");
        var shuffleTitle = new ShuffleText(elTitle);
        var shuffleDescription = new ShuffleText(elDescription);
        shuffleDescription.setText("");
        shuffleTitle.setText("Coming soon");
        shuffleTitle.start();
        shuffleDescription.start();
    }


    // hover(index: number) {
    //   if (this.countries[index] !== this._translate.currentLang) {
    //     console.log(this.countries[index]);
    //     console.log(this._translate.currentLang);
    //     console.log("different");
    //     (document.activeElement as HTMLElement).blur();
    //   } else {
    //     console.log("same");
    //   }
    // }
}
