import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { CreditComponent } from './credit/credit.component';
import { TelechargementComponent } from './telechargement/telechargement.component';
import { PrixComponent } from './prix/prix.component';
import { HomeComponent } from './home/home.component';
import { Chapitre1Component} from './chapitre1/chapitre1.component'
import { Chapitre2Component} from './chapitre2/chapitre2.component'
import { Chapitre3Component} from './chapitre3/chapitre3.component'
import { Chapitre4Component} from './chapitre4/chapitre4.component'
import { Chapitre5Component} from './chapitre5/chapitre5.component'
import { Chapitre6Component} from './chapitre6/chapitre6.component'
import { MobileComponent} from './mobile/mobile.component'

export const router: Routes =[
    {path: '', redirectTo: 'home', pathMatch:'full'},
    {path: 'home', component: HomeComponent },
    {path: 'credit', component: CreditComponent },
    {path: 'telechargement', component: TelechargementComponent },
    {path: 'prix', component: PrixComponent },
    {path: 'chapitre1', component: Chapitre1Component },
    {path: 'chapitre2', component: Chapitre2Component },
    {path: 'chapitre3', component: Chapitre3Component },
    {path: 'chapitre4', component: Chapitre4Component },
    {path: 'chapitre5', component: Chapitre5Component },
    {path: 'chapitre6', component: Chapitre6Component },
    {path: 'mobile', component: MobileComponent},
    {path: '**', component: HomeComponent}
]

export const routes: ModuleWithProviders = RouterModule.forRoot(router);
