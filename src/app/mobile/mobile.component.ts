import { Component, OnInit } from '@angular/core';
import { TranslateService } from '../translate'

@Component({
  selector: 'app-mobile',
  templateUrl: './mobile.component.html',
  styleUrls: ['./mobile.component.css']
})
export class MobileComponent implements OnInit {

  constructor(private _translate: TranslateService) { }

  ngOnInit() {
  }

}
