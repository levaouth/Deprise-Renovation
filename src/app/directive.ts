import { Directive, HostListener, Renderer, ElementRef, Input} from '@angular/core';
import { TranslateService }   from './translate';
@Directive({
    selector: '[changementCar]'
})
export class ChangeDirective{

    constructor(
        private renderer: Renderer,
        private el: ElementRef,
        private _translate: TranslateService
    ){}


    //cette string est aussi presente dans chapitre6.ts pour la methode verifierFin() de ce fichier
    ngOnInit(){
        console.log("hey!");
        console.log( this._translate.instant("hello world") );
        this.Texte = this._translate.instant("Je fais tout pour maîtriser de nouveau le cours de ma vie.") +" \n"+this._translate.instant("Je choisis.")+" \n"+this._translate.instant("Mes émotions.")+" \n"+this._translate.instant("Le sens à donner aux choses.")+" \n"+this._translate.instant("Enfin, je me suis repr");
    }



    Texte: string;
    //Texte = "Je fais tout pour maîtriser de nouveau le cours de ma vie. \nJe choisis. \nMes émotions. \nLe sens à donner aux choses. \nEnfin, je me suis repr";
    compteur=0;
    son : string[] = ["assets/sound/chapitre6/key.mp3","assets/sound/chapitre6/carretReturn.mp3"];
    sonMoment: string = this.son[0];

    //remplacement du caractere entre par l'utilisateur
    @HostListener('keyup')
    onKeyUp() {

      if (this.compteur<this.Texte.length){
        if (this.compteur==0){
           this.el.nativeElement.value = this.Texte[0];
        } else{
            this.el.nativeElement.value = this.el.nativeElement.value.substr(0, this.compteur) + this.Texte[this.compteur];
        }
        this.compteur=this.compteur+1;
      } else{ //Une fois qu'on a affiche tous les caracteres du message, on change le son d'entree des touches
        this.sonMoment=this.son[1];
        //this.visibilityState="hidden";
      }

    }

  //lancement du bruit des touches du clavier quand l'utilisateur entre le texte deforme
    @HostListener('keydown')
    bruitEntree() {
      //console.log("UN KEY UP")
      var audio = new Audio();
      audio.src = this.sonMoment;
      //audio.load();
      audio.play();
    }


}
