import { Component, HostListener, OnInit, Renderer, ElementRef, state, animate, transition, style, trigger } from '@angular/core';
import { Observable, Subscription } from 'rxjs/Rx';
import ShuffleText from 'shuffle-text';
import { TranslateService } from '../translate';
import { Directive } from '@angular/core';
import { NgControl } from '@angular/forms';
import { first } from 'rxjs/operator/first';
//import {ChangeDirective} from '../directive'

//import { FormsModule } from '@angular/forms';


@Component({
    selector: 'app-chapitre6',
    templateUrl: './chapitre6.component.html',
    styleUrls: ['./chapitre6.component.css'],
    providers: [TranslateService],
    animations: [
        trigger('visibilityChanged', [
            state('shown', style({ opacity: 1 })),
            state('hidden', style({ opacity: 0 })),
            transition('shown => hidden', animate('2000ms')),
        ]),
        trigger('visibilityChanged1', [
            state('shown', style({ opacity: 1 })),
            state('hidden', style({ opacity: 0 })),
            transition('shown => hidden', animate('750ms')),
            transition('hidden => shown', animate('500ms')),
        ]),
    ]
})
export class Chapitre6Component implements OnInit {
    constructor(private renderer: Renderer,
        private el: ElementRef,
        private _translate: TranslateService) { }

    Phrase: string[] = ["6",
        this._translate.instant("Il est temps de reprendre le contrôle."),
        this._translate.instant("Arrêter de tourner en rond."),
        this._translate.instant("fin")]

    sprayText: string[] = [this._translate.instant("Retrouver une direction."),
    this._translate.instant("Plier le cours des événements."),
    this._translate.instant("Donner un sens à mes actions."),
    this._translate.instant("Arrêter de tourner en rond.")];
    sprayTextArab: string[];
    isTextArabShown: boolean[];
    caractereAffiche = "";
    parcoursPhraseSprayText: number = 0;
    parcoursEltSprayText: number = 0;
    compteurEspaces: number = 0;
    compteurPlacageLettres: number = 0; //permet d'estimer la frequence de placage de lettres au passage de la souris
    autoriserPhraseSuivante: boolean = false; //autorise le passage a la phrase suivante si les 4 phrases du tableau ont ete mise sur le canvas

    //on a besoin de mettre ce texte ici et pas seulement dans la directive, car on a besoin de savoir sa taille dans verifierFin();
    //Texte = "Je fais tout pour maîtriser de nouveau le cours de ma vie. \nJe choisis. \nMes émotions. \nLe sens à donner aux choses. \nEnfin, je me suis repr";
    Texte = this._translate.instant("Je fais tout pour maîtriser de nouveau le cours de ma vie.") + " \n" + this._translate.instant("Je choisis.") + " \n" + this._translate.instant("Mes émotions.") + " \n" + this._translate.instant("Le sens à donner aux choses.") + " \n" + this._translate.instant("Enfin, je me suis repr");
    //décommenter cette ligne et commenter la suivante quand on
    //s'occupera du son en fonction du deplacement des lettres
    //son = "assets/sound/chapitre6/spray.mp3";
    //son : string = "../assets/sound/chapitre6/key.mp3";
    //sonMoment : string;
    ticks = 0; //compteur de secondes ecoulees depuis le lancement du chapitre4, comme dans le chapitre 1
    ticksPhrase: number; //seconds ecoulees depuis l'affichage de la derniere phrase
    description = this.Phrase[0];

    //tableau dont la valeur i indique si la Phrase[i] est affichee ou non
    //(pratique pour eviter d'afficher la meme phrase sur deux passages de la souris
    // d'affilée par exemple). La premiere valeur est ici a true car le chapitre 6
    //commence directement avec l'affichage d'un 6
    afficherPhrase: boolean[] = [true,
        false,
        false,
        false]

    rienAfficher = false;
    j = 1; //j parcours de tableau comme dans le chapitre 1

    //entier permettant de changer la taille du 2 mais pas des autres textes du chapitre
    fontsize: number = 400; //tout de suite initialises car le 2 est le premier texte affiche dans le chapitre
    top: number = 10; //pour recentrer le 2 sur la page après l'avoir agrandi

    //besoin de ces deux attributs pour faire fonctionner le timer
    private timer;
    private sub: Subscription;
    margintop: number;

    //controle de l'affichage de la zone de texte
    display: string;

    //controle de l'affichage de la zone de suivi de la souris
    displayCanvasPhraseSouris: string;

    // charge de faire disparaitre le block d'insertion de texte a la fin
    visibilityState = "shown";
    //charge de faire disparaitre progressivement les phrases apres disparition
    visibilityState1 = "shown";
    //wobbleState indique l'etat de l'animation
    //son passage de "inactive" a "active" la declenche
    public wobbleState: string = "inactive";

    //boolean indiquant si on peut afficher le lien "fin" vers home ou pas
    chapitreFini: boolean = false;
    // si les 4 phrase sont deja affiche, les lettres ne sont plus ajoute et va disparaitre directement
    autoriserDeplacerLettre: boolean = true;
    // compter le nombre de fois la 4eme spray text affiche, pour aider de logique dans la fonction moveLetter()
    sprayText4 = 0;
    // compter index de lettre de arabe pour affichage des 4 phrases
    wordsCountArab = 0;
    listDiv = [];
    ngOnInit() {
        this.Phrase = ["6",
            this._translate.instant("Il est temps de reprendre le contrôle."),
            this._translate.instant("Arrêter de tourner en rond."),
            this._translate.instant("fin")]

        this.sprayText = [
            this._translate.instant("Retrouver une direction."),
            this._translate.instant("Plier le cours des événements."),
            this._translate.instant("Donner un sens à mes actions."),
            this._translate.instant("Arrêter de tourner en rond.")
        ];
        this.sprayTextArab = [
            "والعثور على اتجاه.",
            "طي مجرى الأحداث.",
            "إعطاء معنى لأفعالي.",
            "والتوقف عن الدوران في حلقة."
        ];
        this.isTextArabShown = [false, false, false, false];
        // console.log(this.sprayTextArab);
        this.Texte = this._translate.instant("Je fais tout pour maîtriser de nouveau le cours de ma vie.") + " \n" + this._translate.instant("Je choisis.") + " \n" + this._translate.instant("Mes émotions.") + " \n" + this._translate.instant("Le sens à donner aux choses.") + " \n" + this._translate.instant("Enfin, je me suis repr");
        console.log(this.Texte);


        this.timer = Observable.timer(0, 1000);
        this.sub = this.timer.subscribe((t) =>
            this.tickerFunc(t)
        );
        if (this._translate.currentLang == "ar") {
            this.deplaceArabe();
        }

    }

    deplaceArabe() {
        let lines = ["0", "1", "2", "3"];
        for (let i in lines) {
            let line = document.getElementById(i);
            console.log("depla")
            line.style.left += 150;
        }
    }

    tickerFunc(tick) {
        this.ticks = tick;
        //On affiche le 6 pendant 2 secondes, puis la premiere phrase du tableau
        //et on indique dans le tableau de booleens que la phrase 1 est affichee
        //et que la phrase 0 ne l'est plus
        if (this.ticks >= 3 && this.afficherPhrase[0]) {
            //On remet le texte a une taille correcte apres l'agrandissement du numero de chapitre
            this.fontsize = 25;
            //Des que le numéro de chapitre est affiche au centre de la page
            //On affiche les phrases du narrateur bien plus bas sur la page
            this.top = 90;
            this.description = this.Phrase[1];
            this.afficherPhrase[0] = false;
            this.afficherPhrase[1] = true;
            //La zone qui suit les phrase de la souris s'affiche alors
            this.displayCanvasPhraseSouris = "block";
        }
    }

    //Verifie que le texte entre n'est pas termine
    //s'il est termine, on le cache pour afficher le mot "fin"
    verifierFin(texteAffiche: string) {
        if (texteAffiche.length == this.Texte.length) {
            this.visibilityState = 'hidden';
        }
    }

    afficherFin() {
        //console.log("fini mais pas encore dans la fonction");
        if (this.j == 2) {
            //console.log("fini, on lance la fonction");
            //this.j++;
            this.chapitreFini = true;
            this.phraseSuivante();
        }
    }

    //indique les conditions pour lequelles on ne passe pas a la suite
    //false --> on autorise le changement vers la phrase suivante
    changementInterdit(i: number): boolean {

        //pas de changement s'il s'est ecoule moins d'une seconde depuis le dernier affichage de phrase
        if (this.ticks - this.ticksPhrase < 1) {
            //console.log("CHANGEMENT INTERDIT");
            return true;
        } else {
            //si i vaut 2, on verifie si l'utilisateur a deja fait rentrer le texte final
            //dans la textarea, si c'est le cas, on affiche la fin mais sinon non
            //toutes les autres valeurs de i autorisent le passage a la phrase suivante
            //Si i vaut 1, on autorise le changement vers "Arrêter de tourner en rond"
            //seulement si la derniere phrase de spraytext a ete affichee dans le canvas
            switch (i) {
                case 1:
                    if (this.autoriserPhraseSuivante) {
                        return false;
                    } else {
                        return true;
                    }
                case 2:
                    if (this.chapitreFini) {
                        return false;
                    } else {
                        return true;
                    }
                default: //dans les autres cas, le changement est autorise
                    return false;
            }
        }
    }

    //fait passer le focus sur le texte peu importe l'endroit de l'ecran ou se trouve le curseur
    //car cette methode se lance sur un mousemove du canvas
    focusAuto() {
        //On met le focus sur le texte pour clairement inciter l'utilisateur a ecrire dedans
        document.getElementById('textefinal').focus();
    }

    //Changement de texte au passage de la souris sur celui-ci
    // faut noter ce n'est pas des textes affiches dans le canvas en lettres
    phraseSuivante() {
        var el = document.getElementById("chap6");
        var shuffle = new ShuffleText(el);
        console.log(this.j)
        //si la souris passe et que j vaut 2 a ce moment la, ca
        //veut dire que le texte affiche est "Il est temps de reprendre
        //le controle". Donc, au lieu d'afficher la phrase suivante,
        //on vire la description, la zone de suivi de la souris
        //et on affiche la zone de texte
        if (this.j == 2) {
            //on stop momentanement le programme le temps de mettre en place le canvas
            this.description = "";
            el.innerHTML = "";
            //La zone qui suit les phrase de la souris laisse place au texte
            this.visibilityState1 = "hidden";
            //this.displayCanvasPhraseSouris="none";
            this.display = "inline";
            //On met le focus sur le texte pour clairement inciter l'utilisateur a ecrire dedans
            document.getElementById('textefinal').focus();
        }

        if (this.afficherPhrase[this.j] && !this.changementInterdit(this.j)) {
            //On autorise le passage de la phrase suivante en changeant le booleen correspondant
            if ((this.j) + 1 < this.Phrase.length) {
                this.afficherPhrase[(this.j) + 1] = true;
                this.description = this.Phrase[(this.j) + 1];
                //Etant donne qu'on a change de texte, il faut prevenir le Shuffle
                //qu'on met a jour le texte a melanger. Pour cela, on arrete le Shuffle
                //en cours et on en relance un nouveau avec le nouveau contenu de la balise html
                el.innerHTML = this.description;
                shuffle = new ShuffleText(el);
                shuffle.start();
            }
            //La phrase i n'étant plus affichee, on met son booleen a false pour
            //eviter de le reafficher lors du hover suivant
            this.afficherPhrase[this.j] = false;
            //on incrémente j pour permettre le changement de phrase au hover suivant
            this.j = (this.j) + 1;

            //sauvegarde du nombre de secondes a l'affichage de la phrase
            //pour savoir quand on va autoriser l'affichage de la phrase suivante
            this.ticksPhrase = this.ticks;
        }
    }

    //formation de phrases au passage de la souris sur le canvas
    afficherSuiviSouris(event: MouseEvent) {

        let container = document.getElementById('container');

        //On affiche une lettre seulement une fois tous les trois mouseevent (sinon les lettres sont trop proches les unes des autres)
        //On fait aussi attention a ne pas afficher ces phrases si le chapitre est fini (le lien vers fin)
        if (this.compteurPlacageLettres > 5 && !this.chapitreFini) {
            try {
                // des fois l'image met trop de temps à se recharger et n'existe plus au moment de l'export, dans ce cas, on ignore (1/1000 à peu près)
                // ctx.font = "20px Arial"; // on choisit la police d'affichage
                //ctx.fillStyle = "white"; // on choisit l'image pour remplir le texte

                if (this.parcoursEltSprayText < this.sprayText[this.parcoursPhraseSprayText].split("").length) {
                    //tant qu'on ne depasse pas une phrase, on affiche les caracteres de la phrase
                    // if (this._translate.currentLang == 'ar') {
                    //     this.caractereAffiche = (this.sprayText[this.parcoursPhraseSprayText].split(""))[this.sprayText[this.parcoursPhraseSprayText].split("").length - 1 - this.parcoursEltSprayText];
                    // }
                    // else {
                    this.caractereAffiche = (this.sprayText[this.parcoursPhraseSprayText].split(""))[this.parcoursEltSprayText];
                    //}
                    this.parcoursEltSprayText++;
                    let myDiv = document.createElement('letter');
                    // myDiv.className = "letter";
                    myDiv.textContent = this.caractereAffiche;
                    myDiv.style.position = "absolute";
                    myDiv.style.top = event.pageY + "px";
                    myDiv.style.left = event.pageX + "px";
                    //myDiv.style.font=ctx.font;
                    myDiv.style.font = "20px Arial";
                    //myDiv.style.color=ctx.fillStyle;
                    myDiv.style.color = "white";
                    //On rajoute les lettres de la phrase actuellement créée
                    container.appendChild(myDiv);
                    this.listDiv.push(myDiv);
                    //ctx.fillText( this.caractereAffiche, positionInCanvaX, positionInCanvaY);
                } else if (this.compteurEspaces < 5) {
                    //dès qu'une phrase est passee, on la fait disparaitre et on compte 15 espaces avant de passer a la suivante
                    // this.visibilityState1 = "hidden";
                    this.caractereAffiche = " ";
                    this.compteurEspaces++;
                } else { //Si on a passe une phrase ET deja ajoute les espaces, on peut afficher la phrase suivante du tableau
                    this.compteurEspaces = 0; //on reinitialise le compteur d'espace en prevision de la fin de la phrase suivante
                    // this.visibilityState1 = "shown";
                    //le container contenait jusqu'a maintenant la phrase precedente
                    //on supprime la phrase precedent avant d'afficher la suivante
                    let indexLetter = 0;
                    let nbrLetter = container.childElementCount;
                    let nLine = this.parcoursPhraseSprayText.toString();
                    this.calculatePhaseSprayText();
                    while (indexLetter < nbrLetter) {
                        //let firstLetter = container.childNodes[indexLetter];
                        let firstLetter;
                        // if (this._translate.currentLang == 'ar') {
                        //     firstLetter = this.listDiv.pop();
                        // }
                        // else {
                        firstLetter = this.listDiv.shift();
                        // }
                        // console.log("index" + indexLetter);
                        // console.log("nbrLetter" + nbrLetter);
                        // console.log(container.childNodes.length);
                        indexLetter++;
                        setTimeout(() => {
                            this.moveLetter(firstLetter, container, nLine);
                        }, 20 * indexLetter);
                    }
                }

                //maintenant qu'on sait quel est le caractere a afficher,
                //on crée une nouvelle div dont le contenu sera le caractere choisi
                //c'est plus simple que d'utiliser directement le canvas
                //car on a besoin de supprimer ces div nouvellement créées
                //pour la phrase suivante

                // obsolete
            } catch{ }
            this.compteurPlacageLettres = 0; //une fois qu'on a affiche une lettre, on relance l'attente avant d'afficher la suivante
            //permet d'eviter que les lettres soient trop proches les unes des autres
        } else {
            this.compteurPlacageLettres++;
        }
    }

    moveLetter(firstLetter, container, nLine) {
        if ((this.autoriserDeplacerLettre || this.sprayText4 == 1) && firstLetter.textContent != null) {
            let collectionLine = document.getElementById(nLine);
            let previousSibling = <HTMLElement>collectionLine.childNodes[collectionLine.childElementCount - 1];
            let linePos = previousSibling.getBoundingClientRect();
            let left = linePos.right;
            let top = linePos.top;
            if (this._translate.currentLang == "ar") {
                // console.log("nLine" + nLine)
                if (!this.isTextArabShown[nLine]) {
                    firstLetter.textContent = this.sprayTextArab[nLine];
                    this.isTextArabShown[nLine] = true;
                } else {
                    firstLetter.innerText = null;
                }
                firstLetter.style.position = "fixed";
                firstLetter.style.top = top + 'px';
                firstLetter.style.left = left - linePos.width - firstLetter.getBoundingClientRect().width + 'px';
            } else {
                // console.log("nLine" + nLine)
                firstLetter.style.position = "fixed";
                firstLetter.style.top = top + 'px';
                firstLetter.style.left = left + 'px';
            }
            collectionLine.appendChild(firstLetter);
        }
        else {
            container.removeChild(firstLetter);
            this.phraseSuivante();
        }
    }

    calculatePhaseSprayText() {
        if (this.sprayText4 == 1) {
            this.sprayText4++;
        }
        console.log("calculatePhaseSprayText" + this.parcoursPhraseSprayText);
        if (this.parcoursPhraseSprayText < this.sprayText.length - 1) {
            //on incremente le compteur pour passer a la phrase suivante
            this.parcoursPhraseSprayText++;
        } else { //on revient a la premiere phrase si la derniere phrase a ete lue
            //on permet de passer au shuffle suivant dans ce cas
            this.autoriserDeplacerLettre = false;
            this.sprayText4++;
            this.autoriserPhraseSuivante = true;
            //console.log("ON CHANGE");
            if (this.j == 1) {
                this.phraseSuivante();
            }
            this.parcoursPhraseSprayText = 0;
        }
        //on revient au premier caractere de la phrase et on l'affiche
        this.parcoursEltSprayText = 0;
        this.caractereAffiche = (this.sprayText[this.parcoursPhraseSprayText].split(""))[this.parcoursEltSprayText];
        // this.parcoursEltSprayText++; //sans oublier d'incrementer le caractere parcouru au debut
    }

    getWidth() {
        const styleSize = { 'width': window.innerWidth + 'px', 'height': '450px' };
        return styleSize;
    }
}


//tentative de mettre le son. Ne fonctionne pas comme sur le projet Flash
//voir comment detecter un deplacement de souris au sein de l'ecran
//(sans forcement quitter la fenetre)
/*bruitageSouris() {
  this.sonMoment=this.son;
  //console.log("ICI LA ZIKMU");
}

finBruitageSouris() {
  this.sonMoment="";
}*/
