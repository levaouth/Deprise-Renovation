import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Chapitre6Component } from './chapitre6.component';

describe('Chapitre6Component', () => {
  let component: Chapitre6Component;
  let fixture: ComponentFixture<Chapitre6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Chapitre6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Chapitre6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
