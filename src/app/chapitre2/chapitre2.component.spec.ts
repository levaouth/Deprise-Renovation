import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Chapitre2Component } from './chapitre2.component';

describe('Chapitre2Component', () => {
  let component: Chapitre2Component;
  let fixture: ComponentFixture<Chapitre2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Chapitre2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Chapitre2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
