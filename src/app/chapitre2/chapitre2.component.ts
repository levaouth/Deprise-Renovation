import { Component, OnInit,trigger, keyframes, animate, transition, style, state, NgZone } from '@angular/core';
import { TranslateService }   from '../translate';

import {Observable,Subscription} from 'rxjs/Rx';
//import {HostListener} from '@angular/core';
import ShuffleText from 'shuffle-text';


import {Flatterieaffichee} from '../flatterieaffichee';
import {Flatterie} from '../flatterie';
import {AppComponent} from '../app.component';

@Component({
  selector: 'app-chapitre2',
  templateUrl: './chapitre2.component.html',
  styleUrls: ['./chapitre2.component.css'],
  //gestion de l'animation du "impossible de prononcer qqch de coherent"
  providers: [TranslateService],
  animations: [
    trigger('wobble', [
      transition('inactive => active', animate(1000, keyframes([
        style({transform: 'translate3d(0, 800%, 0)', offset: 1})
      ]))),
    ]),
    //Les 5 animations suivantes gerent les effets fadeout des phrases d'accroche du narrateur a la fille
    //c'est un peu triste d'avoir 5 animations qui font la meme chose,
    //mais chaque animation doit etre reliee a une flatterie precise du narrateur
    //et je n'ai pas trouve comment faire des tableaux d'animations qui permettraient
    //de faire ca plus elegamment
    trigger('visibilityChanged0', [
      state('shown', style({ opacity: 1 })),
      state('hidden', style({ opacity: 0 })),
      transition('shown => hidden', animate('3000ms')),
    ]),
    trigger('visibilityChanged1', [
      state('shown', style({ opacity: 1 })),
      state('hidden', style({ opacity: 0 })),
      transition('shown => hidden', animate('3000ms')),
    ]),
    trigger('visibilityChanged2', [
      state('shown', style({ opacity: 1 })),
      state('hidden', style({ opacity: 0 })),
      transition('shown => hidden', animate('3000ms')),
    ]),
    trigger('visibilityChanged3', [
      state('shown', style({ opacity: 1 })),
      state('hidden', style({ opacity: 0 })),
      transition('shown => hidden', animate('3000ms')),
    ]),
    trigger('visibilityChanged4', [
      state('shown', style({ opacity: 1 })),
      state('hidden', style({ opacity: 0 })),
      transition('shown => hidden', animate('3000ms')),
    ]),
    trigger('visibilityChanged5', [
      state('shown', style({ opacity: 1 })),
      state('hidden', style({ opacity: 0 })),
      transition('shown => hidden', animate('3000ms')),
    ])
  ]
})
export class Chapitre2Component implements OnInit {

  constructor( private _translate: TranslateService ) { }


  //Tableau des sons du chapitre 2 (meme principe que dans le chapitre1)
  sons : string[] = ["assets/sound/chapitre2/public_fond_sonore_01.mp3"];



// ATTENTION !!
// ATTENTION !!
// Ces phrases ne devraient apparaître qu'en cas de défaillance de la traduction, les modifier n'a bien souvent aucun impact visible
// ATTENTION !!
// ATTENTION !!
  Phrase : string[] = ["2",
                      "Mais le rendez-vous était biaisé.",
                      "Je ne m'en suis rendu compte que beaucoup plus tard.",
                      "La femme en face de moi, qui paraissait si parfaite, me laissait bouche bée.",
                      "Impossible de prononcer quelque chose de cohérent.",
                      "Sa présence me bouleversait...",
                      "Il fallait que je pose des questions pour la mettre à jour.",
                      "Sans que je m'en aperçoive, cette inconnue devenait ma femme.",
                      "On a tout partagé.",
                      "Mais jamais je ne suis parvenu à vraiment la connaître.",
                      "Aujourd'hui encore je me pose des questions.",
                      "Qui d'elle ou moi suit l'autre ?",
                      "Quand je l'aime, elle me sème."]

  changementManuel = [4,6] // index des phrases dont le changement ne se fait pas par phrasesuivante

    //json de toutes les flatteries en francais
    public flatteriesToutes: Flatterie[] = [
        {id: 0, phraseNormale: "Et vous travaillez dans quoi ?", phraseTravers: "Et vous travaillez l'envoi ?", son: "assets/sound/chapitre2/julien2.mp3"},
        {id: 1, phraseNormale: "Vous habitez la région depuis longtemps ?", phraseTravers: "Vous évitez la légion depuis longtemps ?", son: "assets/sound/chapitre2/julien1.mp3"},
        {id: 2, phraseNormale: "Je vous trouve vraiment très jolie !", phraseTravers: "Chevaux, brousse, bêlements... près jolis", son: "assets/sound/chapitre2/julien3.mp3" },
        {id: 3, phraseNormale: "J'ai l'impression qu'on a beaucoup de points communs", phraseTravers: "Chemins pression en Allemagne point comme un...", son: "assets/sound/chapitre2/julien4.mp3" },
        {id: 4, phraseNormale: "Vous avez des yeux somptueux.", phraseTravers: "Vous avez des notions de tueur", son: "assets/sound/chapitre2/julien5.mp3" },
        {id: 5, phraseNormale: "Vous venez souvent ici ?", phraseTravers: "Vous avez l'absent acquis.", son:  "assets/sound/chapitre2/julien6.mp3"},
        {id: 6, phraseNormale: "J'aime votre façon de sourire", phraseTravers: "Gêne, votre garçon mourir", son:  "assets/sound/chapitre2/julien8.mp3"},
        {id: 7, phraseNormale: "Vous voulez marcher un peu ?", phraseTravers: "Nouveaux-nés barges et il pleut.", son: "assets/sound/chapitre2/julien9.mp3"},
        {id: 8, phraseNormale: "Puis-je vous offrir un autre verre ?", phraseTravers: "Pigeon ouïr un Notre Père ?", son:  "assets/sound/chapitre2/julien7.mp3"},
      ]


  //On lance le bruit de foule dès le lancement du chapitre 2
  sonMoment=this.sons[0];
  //phraseParlee="";
  //var phraseParlee;
  ticks =0; //compteur de secondes ecoulees depuis le lancement du chapitre2, comme dans le chapitre 1

  ticksPhrase=0; //compteur de secondes ecoulees depuis l'affichage de la derniere phrase
  description=this.Phrase[0];

  //tableau dont la valeur i indique si la Phrase[i] est affichee ou non
  //(pratique pour eviter d'afficher la meme phrase sur deux passages de la souris
  // d'affilée par exemple). La premiere valeur est ici a true car le chapitre 2
  //commence directement avec l'affichage d'un 2
  afficherPhrase: boolean[] = [true,
                              false,
                              false,
                              false,
                              false,
                              false,
                              false,
                              false,
                              false,
                              false,
                              false,
                              false,
                              false]

  rienAfficher = false;
  hoverAllowed = true;
  j=1; //j parcours de tableau comme dans le chapitre 1
  //commence ici a 1 car les hover commencer a partir de Phrase[1] dans ce chapitre

  //entier permettant de changer la taille du 2 mais pas des autres textes du chapitre
  fontsize:number=400; //tout de suite initialises car le 2 est le premier texte affiche dans le chapitre
  top:number=10; //pour recentrer le 2 sur la page après l'avoir agrandi


  //liste des phrases que le narrateur dit a la fille qui lui plait
  //qui s'affichent toutes d'un coup apres le "impossible de prononcer qqch de coherent"
  //Les flatteriesAffiches sont stockees dans un json, on a besoin de garder l'id ET la string correspondante
  //L'interface flatterieAffichee a ete declaree dans flatterieaffichee.ts, qui fait partie des imports
  //Ces flatteriesEcran sont celles qui seront affichees dans le fichier .html des que leur attribut
  //css "hidden" sera remplace par "bloc" (c'est fait juste apres le 'impossible de prononcer qqch de coherent')
  //dans la methode 'afficherFlatteries'
  public flatteriesEcran: Flatterieaffichee[] = [
    {id: -1, placeSurLaPage: -1, sourispassee: false},
    {id: -1, placeSurLaPage: -1, sourispassee: false},
    {id: -1, placeSurLaPage: -1, sourispassee: false},
    {id: -1, placeSurLaPage: -1, sourispassee: false},
    {id: -1, placeSurLaPage: -1, sourispassee: false},
    {id: -1, placeSurLaPage: -1, sourispassee: false}
  ];

  //permet de gerer la position des phrases apres deplacement
  public margintop: Number;
  //display permet d'indiquer qu'on veut afficher la balise au moment voulu (au debut c'est hidden vu qu'il s'agit des flatteries)
  public display: string;

  //wobbleState indique l'etat de l'animation
  //son passage de "inactive" a "active" la declenche
  public wobbleState: string="inactive";

  //besoin de ces deux attributs pour faire fonctionner le timer
  private timer;
  private sub: Subscription;

  //la couleur du contour autour du point d'interrogation
  //public shadow: string;

  //Tableau indiquant l'état (hidden ou shown) de chacune des phrases
  //d'accroche du narrateur à la fille, une fois qu'elle sont affichées
  visibilityStates : string[] = ["shown",
                                "shown",
                                "shown",
                                "shown",
                                "shown",
                                "shown"]

 phraseParlee=new Audio(); //Attribut stockant la phrase dite par l'utilisateur a l'oral
 DebutPhrasesAudio: boolean=true; //l'attribut .ended des audio est par defaut a false
 //si aucun fichier audio n'est lance, ce boolen sert de faire en sorte a autoriser le lancement du premier son
 //(cf la methode flatterieVersTravers)


  ngOnInit() {
    this.Phrase =  ["2",
                        this._translate.instant("Mais le rendez-vous était biaisé."),
                        this._translate.instant("Je ne m'en suis rendu compte que beaucoup plus tard."),
                        this._translate.instant("La femme en face de moi, qui paraissait si parfaite, me laissait bouche bée."),
                        this._translate.instant("Impossible de prononcer quelque chose de cohérent."),
                        this._translate.instant("Sa présence me bouleversait..."),
                        this._translate.instant("Il fallait que je pose des questions pour la mettre à jour."),
                        this._translate.instant("Sans que je m'en aperçoive, cette inconnue devenait ma femme."),
                        this._translate.instant("On a tout partagé."),
                        this._translate.instant("Mais jamais je ne suis parvenu à vraiment la connaître."),
                        this._translate.instant("Aujourd'hui encore je me pose des questions."),
                        this._translate.instant("Qui d'elle ou moi suit l'autre ?"),
                        this._translate.instant("Quand je l'aime, elle me sème.")]

    this.flatteriesToutes = [
        {id: 0, phraseNormale: this._translate.instant("Et vous travaillez dans quoi ?"), phraseTravers: this._translate.instant("Et vous travaillez l'envoi ?"), son: this._translate.instant("assets/sound/chapitre2/julien2.mp3")},
        {id: 1, phraseNormale: this._translate.instant("Vous habitez la région depuis longtemps ?"), phraseTravers: this._translate.instant("Vous évitez la légion depuis longtemps ?"), son: this._translate.instant("assets/sound/chapitre2/julien1.mp3")},
        {id: 2, phraseNormale: this._translate.instant("Je vous trouve vraiment très jolie !"), phraseTravers: this._translate.instant("Chevaux, brousse, bêlement... près jolis"), son: this._translate.instant("assets/sound/chapitre2/julien3.mp3") },
        {id: 3, phraseNormale: this._translate.instant("J'ai l'impression qu'on a beaucoup de points communs"), phraseTravers: this._translate.instant("Chemins pression en Allemagne point comme un..."), son: this._translate.instant("assets/sound/chapitre2/julien4.mp3") },
        {id: 4, phraseNormale: this._translate.instant("Vous avez des yeux somptueux"), phraseTravers: this._translate.instant("Vous avouez des notions de tueurs"), son: this._translate.instant("assets/sound/chapitre2/julien5.mp3") },
        {id: 5, phraseNormale: this._translate.instant("Vous venez souvent ici ?"), phraseTravers: this._translate.instant("Vous avez l'absent acquis"), son:  this._translate.instant("assets/sound/chapitre2/julien6.mp3")},
        {id: 6, phraseNormale: this._translate.instant("J'aime votre façon de sourire"), phraseTravers: this._translate.instant("Gêne, votre garçon mourir"), son:  this._translate.instant("assets/sound/chapitre2/julien8.mp3")},
        {id: 7, phraseNormale: this._translate.instant("Vous voulez marcher un peu ?"), phraseTravers: this._translate.instant("Nouveaux-nés barges et il pleut."), son: this._translate.instant("assets/sound/chapitre2/julien9.mp3")},
        {id: 8, phraseNormale: this._translate.instant("Puis-je vous offrir un autre verre ?"), phraseTravers: this._translate.instant("Pigeon ouïr un Notre Père ?"), son:  this._translate.instant("assets/sound/chapitre2/julien7.mp3")},
      ]


  this.questions = [
      this._translate.instant("Qui êtes-vous ?"),
      this._translate.instant("Vous aimez..."),
      this._translate.instant("Que pensez-vous..."),
      this._translate.instant("D'où venez-vous ?"),
      this._translate.instant("Où allez-vous ?"),
      this._translate.instant("Vous pensez quoi de...")
  ]


    //lancement d'un timer des le lancement de la page comme dans le chapitre 1
    this.timer = Observable.timer(0,1000);
    this.sub=this.timer.subscribe((t)=>
      this.tickerFunc(t)
     );

     var longueurFlatterie=this.flatteriesToutes.length;
     var elementChoisi=Math.floor(Math.random() * longueurFlatterie);
    //Initialisation du tableau des flatteries qui seront affichees (aleatoires parmi la liste de toutes les flatteries possible)
    for (var i=0;i<6;i++){

      //on tire une flatterie au hasard jusqu'à en trouver une qui n'est pas deja affichee, et on l'ajoute au tableau des flatteries affichees
      while(dejaPresent(elementChoisi,this.flatteriesEcran,this.flatteriesToutes) ) {
        elementChoisi= Math.floor(Math.random() * longueurFlatterie);
      }
      //On conserve le nombre qui a ete choisi pour pouvoir retrouver son travers correspondant par la suite
      this.flatteriesEcran[i].id=elementChoisi;
      this.flatteriesEcran[i].placeSurLaPage=i;
    }
  }


  tickerFunc(tick){
    this.ticks = tick;
    //On affiche le 2 pendant 2 secondes, puis la premiere phrase du tableau
    //et on indique dans le tableau de booleens que la phrase 1 est affichee
    //et que la phrase 0 ne l'est plus
    if (this.ticks >= 3 && this.afficherPhrase[0]) {
       this.description=this.Phrase[1];
       //On remet le texte a une taille correcte apres l'agrandissement du 2
       this.fontsize=25;
       this.top=45;
       this.afficherPhrase[0]=false;
       this.afficherPhrase[1]=true;
     }

     //Si j=4, ça veut dire qu'on est à la phrase "impossible de prononcer qqch de coherent"
     //on lance alors l'animation
     if (this.j==4){
        this.wobbleState = "active";
       }
   }

   //On affiche les flatteries juste apres l'animation
   afficherFlatteries(){
   //Attention, la hauteur ne change qu'à partir de la phrase "Impossible de prononcer qqch de coherent" (ici la phrase 4)
     if (this.j==4){
       this.display="block";
       this.margintop=90; //changement de la position de "Impossible..cohérent" pour éviter qu'il ne revienne au milieu de l'écran après l'animation
     }
   }

   changementInterdit(i:number, phrases: string[]) : boolean {
     // indique si l'on peut quitter la phrase i par over.
     let listeChangementsManuels = this.changementManuel;
     let interdit;

     //pas de changement s'il s'est ecoule moins de deux secondes depuis le dernier affichage de phrase
     if (this.ticks -this.ticksPhrase < 1) {
      //console.log("CHANGEMENT INTERDIT");
       return true;
     } else if (listeChangementsManuels.indexOf(i)== -1){
       //si on n'arrive pas à retrouver j dans la liste des phrases
       //par changement "manuel" (cad pas fait par l'utilisateur),
       //alors on renvoie false (on autorise le changement vers la phrase suivante)
       return false;
     }else {
      // console.log (i)
       switch (i){
           case 4:
               //si i vaut 4, on n'autorise le passage a la phrase suivante
               //que si l'utilisateur a d'abord passe la souris sur toutes les phrases
               //de flatterie du narrateur affichees a l'ecran
               for (var k=0; k<phrases.length; k++){
                 //Si une phrase n'est pas encore hidden, ça veut dire que
                 //l'utilisateur n'a pas passe la souris sur une des phrases d'accroche
                 //donc on interdit toujours le changement
                     if (phrases[k]=="shown"){
                       return true;
                     }
                   }
                //si l'utilisateur a passe la souris sur toutes les phrases
                //d'accroche, on peut passer a la phrase suivante
                //donc on arrete l'interdiction du changement (et donc on retourne false)
               return false;
           case 6: {
             // 6 est interdit tant que pas bon affichage
             // Il faut que l'image de la fille soit "suffisamment claire"
             // on fixe arbitrairement la limite de "clarte" a 500 questions
             // coloriant l'image
             if( this.nbQuestionAffichee > this.NB_QUESTION_A_AFFICHER ){
               return false ;
             }else{
               return true;
             }
           }
           default:
            //Normalement une autre situation n'est pas censee arrivee,
            //mais au cas ou, on met true par precaution (ce qui interdit le changement)
            return true;
       }
     }
   }
  
   //Lorsque l'utilisateur passe la souris sur une phrase d'accroche affichee,
   //on la transforme en son "travers" correspondant, avec la transition shuffle-text
  flatterieVersTravers(flat: Flatterieaffichee) {
     if (this.hoverAllowed) {
     //il faut bien faire attention a ce que ce soit le travers
     //correspondant a l'accroche sur laquelle l'utilisateur a clique
     var el = document.getElementById(flat.id.toString());
     var shuffle = new ShuffleText(el);

     //On fait la transition shuffle uniquement quand on passe de la phrase "normale" a la phrase de travers
     //(au premier passage de la souris donc)
       if (!flat.sourispassee) {
         this.hoverAllowed = false;
         flat.sourispassee = true; //on indique que le hover a ete fait pour bloquer le shuffle sur cette phrase ensuite

         //On change le texte pour la phrase de travers correspondant
         el.innerHTML = this.flatteriesToutes[flat.id].phraseTravers;

         //ne pas oublier de reinitialiser shuffle ici pour qu'il prenne en compte
         //le changement du innerHTML de el apres la transition shuffle
         shuffle = new ShuffleText(el);
         shuffle.start();

         //et on lance le fadeout pour faire disparaitre la phrase apres affichage du travers
         this.visibilityStates[flat.placeSurLaPage] = 'hidden';

         //c'est plus simple d'utiliser une methode juste pour le fichier audio, pour la gestion des timers
         this.LancementPhraseAudio(flat);
         // ajoute un timeout pour que chaque phrase puisse etre fini
         setTimeout(() => {
          this.hoverAllowed = !this.hoverAllowed;
         }, 2000);
       }
     }
   }

   LancementPhraseAudio(flat: Flatterieaffichee){
     //Et on termine par la mise a jour du son "phraseParlee" qu'on entende la phrase a l'oral
     //On attend la fin d'une phrase parlee pour lancer la suivante, meme si on passe vite sur deux phrases
     //this.DebutPhrasesAudio est la pour autoriser le lancement de la premiere phrase audio uniquement,
     //puis dans les phrases suivante, l'autorisation du lancement de l'audio d'une des phrases
     //est conditionnee par this.phraseParlee.ended, qui vaut true seulement si la phrase precedente
     //a ete entierement dite
      if (this.DebutPhrasesAudio || this.phraseParlee.ended){
       this.DebutPhrasesAudio=false;
      //console.log("AUDIO FINI "+this.phraseParlee.ended);
       this.phraseParlee=new Audio();
       this.phraseParlee.src=this.flatteriesToutes[flat.id].son;
       this.phraseParlee.load();
       this.phraseParlee.play();
     } else {
       setTimeout( ()=>{ this.LancementPhraseAudio(flat); }, 150);
       //LancementPhraseAudio(flat);
     }
   }

   //Changement de texte au passage de la souris sur celui-ci
  phrasesuivante(){

    //Lors d'un hover, on lance le shuffle SAUF si le texte affiche a l'écran
    //est un 2 ou sur la phrase "impossible de prononcer qqch de coherent"
    //ou si le coloriage n'est pas fini
    //ou si on est sur la derniere phrase du chapitre
    if (!this.afficherPhrase[0] && !this.changementInterdit(this.j, this.visibilityStates) && this.description!=this.Phrase[this.Phrase.length-1]){
       var el = document.getElementById("chap2");
       var shuffle = new ShuffleText(el);
       shuffle.start();
     }

         if (this.afficherPhrase[this.j] && !this.changementInterdit(this.j, this.visibilityStates)){
          //On autorise le passage de la phrase suivante en changeant le booleen correspondant
            if ((this.j)+1<this.Phrase.length){
                 this.afficherPhrase[(this.j)+1]=true;
                 this.description=this.Phrase[(this.j)+1]
                 //console.log(this.description);
                 //console.log(this.j)
                 //Etant donne qu'on a change de texte, il faut prevenir le Shuffle
                 //qu'on met a jour le texte a melanger. Pour cela, on arrete le Shuffle
                 //en cours et on en relance un nouveau avec le nouveau contenu de la balise html
                 shuffle.stop();
                 el.innerHTML=this.description;
                 shuffle = new ShuffleText(el)
                 shuffle.start();
             }

         //La phrase i n'étant plus affichee, on met son booleen a false pour
         //eviter de le reafficher lors du hover suivant
         this.afficherPhrase[this.j]=false;
         //on incrémente j pour permettre le changement de phrase au hover suivant
         this.j=(this.j)+1;
         //sauvegarde du nombre de secondes a l'affichage de la phrase
         //pour savoir quand on va autoriser l'affichage de la phrase suivante
         this.ticksPhrase=this.ticks;
         //console.log("nb de secondes a ticksPhrase "+this.ticksPhrase);
       }

       if ( this.j == 6){ //après l'affichage de "il fallait...jour'", on affiche le canvas
           this.montrerFemme = true; //la balise d'id canvaFemme dans le html s'affiche
       }


  }

// tableau contenant l'ensemble des questions à poser au travers du visage
  questions:  string[] = [
      "Qui êtes-vous ?",
      "Vous aimez...",
      "Que pensez vous ?",
      "D'où venez vous ?",
      "Où allez vous ?",
      "Vous pensez quoi de..."
  ]

  getQuestion(i){
    // donne la i-eme question
    return this.questions[i];
  }

  getNbQuestions(){
     // donne le nombre total de questions;
     return this.questions.length;
  }
  getRandomQuestion() {
     // renvoie une question au hasard parmi celles qu'on peut poser
     let randomInt = Math.floor(Math.random() * this.getNbQuestions());
     return this.getQuestion( randomInt );

  }


  NB_QUESTION_A_AFFICHER= 500.
  nbQuestionAffichee = 0;
  montrerFemme=false;

  images : string[] = [
      "assets/img/femme.jpg",
      "assets/img/femme1.jpg",
      "assets/img/femme2.jpg",
      "assets/img/femme3.jpg"
  ]


  getImage(i){
    return this.images[i];
  }
  getNbImages(){
    return this.images.length;
  }
  getRandomImage(){
    let randomInt = Math.floor(Math.random() * this.getNbImages());
      return this.getImage(randomInt);
  }
  srcFemme = this.images[0];

  creerQuestion(){
    //console.log("ca roule");
    let c : any= document.getElementById("canvaFemme");
    let canvaPosition = c.getBoundingClientRect();
    //this.shadow="-1px 0 yellow, 0 1px yellow, 1px 0 yellow, 0 -1px yellow;"
    //let ctx = c.getContext("2d");
    //x aleatoire entre la position canvaPosition a gauche et sa largeur
    console.log("LARGEUR CANVAS "+canvaPosition.width);
    var x = Math.floor(Math.random()*canvaPosition.width);
    console.log(x);
    var y = Math.floor(Math.random()*canvaPosition.height);
    console.log(y);
    this.createText(x,y,c);
  }

  createTextCoordonneesSouris(event:MouseEvent){
    let c : any= document.getElementById("canvaFemme");
    let canvaPosition = c.getBoundingClientRect();
    let ctx = c.getContext("2d");
    let positionInCanvaX = event.pageX - canvaPosition.left;
    console.log(positionInCanvaX);
    let positionInCanvaY = event.pageY - canvaPosition.top;
    console.log(positionInCanvaY);
    this.createText(positionInCanvaX,positionInCanvaY,c);
  }

  createText(posX:number, posY:number, c:any){
     this.srcFemme=this.getRandomImage();
   // crée une phrase random, la met aux couleurs de l'image et l'affiche au bon endroit dans le canva
  // img: l'image de la femme
  // ctx: l'objet canva que l'on manipule
  // style: l'image utilisée comme style pour l'objet
      var img = document.getElementById("femme");
      //let c : any= document.getElementById("canvaFemme");
      //let canvaPosition = c.getBoundingClientRect();
      let ctx = c.getContext("2d");
  try{
  // des fois l'image met trop de temps à se recharger et n'existe plus au moment de l'export, dans ce cas, on ignore (1/1000 à peu près)
      var style = ctx.createPattern(img, 'no-repeat');
      ctx.font = "15px Arial"; // on choisit la police d'affichage
      ctx.fillStyle = style; // on choisit l'image pour remplir le texte
      //let positionInCanvaX = event.pageX - canvaPosition.left;
      //let positionInCanvaY = event.pageY - canvaPosition.top;
      // position où afficher le texte dans le canva

      ctx.fillText( this.getRandomQuestion(), posX, posY);
  }catch{}
      this.nbQuestionAffichee++; // on incrémente le nombre de phrase affichées

      //une fois qu'on a atteint la precision voulue, on passe a la phrase suivante
      if ( this.nbQuestionAffichee > this.NB_QUESTION_A_AFFICHER && this.j == 6 ) {
        this.phrasesuivante();
      }
  }



}

//fonction verifiant si flatterie[element] est deja present dans les flatteries affichees (auquel cas on renvoie true)
function dejaPresent (element : number, flatterieAff : Flatterieaffichee[], flatt : Flatterie[]) : boolean{
  for (var i = 0; i<6; i++){
    if (flatterieAff[i].id==flatt[element].id){
      return true;
    }
  }
  return false;
};

//fonction vérifiant que l'utilisateur a passe la souris sur les six phrases
//de flatterie du narrateur (et donc que leurs travers ont tous ete affichés)
/*function FinToutesAccroches(phrases: string[]) : boolean {
  for (var k=0; k<phrases.length; k++){
    //Si une phrase n'est pas encore hidden, on retourne false;
    if (phrases[k]=="shown"){
      return false;
    }
  }
  return true;
}*/
