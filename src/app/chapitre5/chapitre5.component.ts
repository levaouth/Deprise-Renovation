
import { Component, OnInit, trigger, keyframes, animate, transition, style, state, NgZone, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs/Rx';
import ShuffleText from 'shuffle-text';
declare var JSManipulate: any;
import { TranslateService } from '../translate';


@Component({
    selector: 'app-chapitre5',
    templateUrl: './chapitre5.component.html',
    styleUrls: ['./chapitre5.component.css'],
    providers: [TranslateService],
    animations: [ //animation de deplacement sur "elle m'échappe"
        trigger('wobble', [
            transition('inactive => active', animate(1000, keyframes([
                style({ transform: 'translate3d(0, 800%, 0)', offset: 1 })
            ]))),
        ]),
        trigger('visibilityChanged', [
            state('shown', style({ opacity: 1 })),
            state('hidden', style({ opacity: 0 })),
            transition('shown => hidden', animate('3000ms')),
        ])
    ]
})
export class Chapitre5Component implements OnInit, OnDestroy {
    ngOnDestroy(): void {
        console.log("quitting");
        if (this.cameraIsON) {
            this.video.srcObject.getTracks().forEach(track => track.stop());
        }
    }
    
    constructor(private _translate: TranslateService) { }

    Phrase: string[] = ["5",
        "Suis-je si peu présent ?",
        "Si modelable ?",
        "Ma propre image semble me fuir.",
        "Elle m'échappe.", // deplacement vers le bas de l'image
        "Je me sens manipulé."
    ]
    
    son = "assets/sound/chapitre5/Rubric.mp3";
    //sonMoment:string;

    //lance cette video si l'utilisateur refuse la webcam
    videoDefaut = "assets/sound/chapitre5/aPicADayEdit.mp4";

    ticks = 0; //compteur de secondes ecoulees depuis le lancement du chapitre5, comme dans le chapitre 1
    ticksPhrase: number; //seconds ecoulees depuis l'affichage de la derniere phrase
    description = this.Phrase[0];
    cameraIsON = false;
    //tableau dont la valeur i indique si la Phrase[i] est affichee ou non
    //(pratique pour eviter d'afficher la meme phrase sur deux passages de la souris
    // d'affilée par exemple). La premiere valeur est ici a true car le chapitre 5
    //commence directement avec l'affichage d'un 5
    afficherPhrase: boolean[] = [true,
        false,
        false,
        false,
        false,
        false]

    rienAfficher = false;
    j = 1; //j parcours de tableau comme dans le chapitre 1
    //commence ici a 1 car les hover commencer a partir de Phrase[1] dans ce chapitre

    afficherVideo = false;

    //entier permettant de changer la taille du 2 mais pas des autres textes du chapitre
    fontsize: number = 400; //tout de suite initialises car le 2 est le premier texte affiche dans le chapitre
    top: number = 10; //pour recentrer le 2 sur la page après l'avoir agrandi

    //wobbleState indique l'etat de l'animation
    //son passage de "inactive" a "active" la declenche
    public wobbleState: string = "inactive";

    //besoin de ces deux attributs pour faire fonctionner le timer
    private timer;
    private sub: Subscription;

    margintop: number;

    //element audio utilise pour la musique de etres indecis de l'animation
    sonMoment = new Audio();

    //string controlant la disparition du canvas et de la vidéo deformant le visage
    visibilityState = "shown";
    displayVideo: string;

    //boolean indiquant si l'utilisateur a autorise l'allumage webcam
    //utile pour le bon positionnement de la video par defaut
    camAutorisee: boolean;

    video: any;

    ngOnInit() {
        this.Phrase = ["5",
            this._translate.instant("Suis-je si peu présent ?"),
            this._translate.instant("Si modelable ?"),
            this._translate.instant("Ma propre image semble me fuir."),
            this._translate.instant("Elle m'échappe."), // deplacement vers le bas de l'image
            this._translate.instant("Je me sens manipulé.")
        ]
        this.timer = Observable.timer(0, 1000);
        this.sub = this.timer.subscribe((t) =>
            this.tickerFunc(t)
        );
    }

    tickerFunc(tick) {
        this.ticks = tick;
        //On affiche le 4 pendant 2 secondes, puis la premiere phrase du tableau
        //et on indique dans le tableau de booleens que la phrase 1 est affichee
        //et que la phrase 0 ne l'est plus
        if (this.ticks >= 3 && this.afficherPhrase[0]) {
            //On remet le texte a une taille correcte apres l'agrandissement du numero de chapitre
            this.fontsize = 25;
            this.top = 45;
            this.description = this.Phrase[1];
            this.afficherPhrase[0] = false;
            this.afficherPhrase[1] = true;
        }

        //Si j=4, ça veut dire qu'on est à la phrase "Elle m'échappe
        //on lance alors l'animation
        if (this.j == 4) {
            this.wobbleState = "active";
        }

        //toutes les secondes, on augmente un peu le volume de la musique
        //apres que la video soit affichee
        if (this.afficherVideo && this.sonMoment.volume + 0.1 <= 1) {
            //this.sonMoment.volume+=0.01;
            this.sonMoment.volume *= 1.5;
            // console.log("volume " + this.sonMoment.volume);
        }

        //lancer automatiquement un shuffle si l'animation arrive a son terme
        //et que la derniere phrase n'a pas encore ete affichee
        if (this.videoFinie() && this.description != this.Phrase[this.Phrase.length - 1]) {
            this.phraseSuivante();
        }
    }

    //repositionement du texte apres l'animation vers le bas
    placerCorrectement() {
        //Attention, la hauteur ne change qu'à partir de la phrase "Elle m'échappe" (ici la phrase 4)
        if (this.j >= 4) {
            //console.log('HEAY');
            this.margintop = 90; //besoin de rehausser un peu la phrase par rapport a l'endroit ou elle essaie d'aller
            //this.sonMoment = this.son;
            if (this.j == 4) { //lancement de la video

                this.setVideo();
                this.setup();
            }
        }
    }


    ChangementsManuels: number[] = [4]; // ensemble des elts dont le passage par dessus ne suffit pas pour passer à la suite
    videoBegin = 10000; // initialement, on set le début de la vidéo à une valeur trop haute
    VIDEO_LENGTH_MIN = 30; // durée minimale de la vidéo
    videoLength() {
        // renvoie la durée de la vidéo jusqu'à présent
        return this.ticks - this.videoBegin;
    }

    videoFinie(): boolean {
        // indique si l'on a fait joujou suffisemment de temps pour passer à la suite
        if (this.videoLength() > this.VIDEO_LENGTH_MIN) {
            //il est alors temps de supprimer en fondu la vidéo de l'écran
            this.visibilityState = "hidden";
            this.displayVideo = "none";
            return true;
        }
        else {
            return false;
        }
    }

    changementInterdit(i) {
        //pas de changement s'il s'est ecoule moins de deux secondes depuis le dernier affichage de phrase
        if (this.ticks - this.ticksPhrase < 1) {
            //console.log("CHANGEMENT INTERDIT");
            return true;
        }
        else if (this.ChangementsManuels.indexOf(i) == -1) {
            // dit si on a le droit de passer à la phrase suivante (en fonction du tableau ChangementsManuels et de comportements spécifiques définis dans changementInterdit
            return false;
        }
        else {
            switch (i) {
                case 4: {
                    if (this.videoFinie()) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
            }
        }
    }

    phraseSuivante() {
        if (!this.changementInterdit(this.j)) {

            if (!this.afficherPhrase[0]) {
                var el = document.getElementById("chap5");
                var shuffle = new ShuffleText(el);
                shuffle.start();
            }

            if (this.afficherPhrase[this.j]) {
                //On autorise le passage de la phrase suivante en changeant le booleen correspondant
                if ((this.j) + 1 < this.Phrase.length) {
                    this.afficherPhrase[(this.j) + 1] = true;
                    this.description = this.Phrase[(this.j) + 1];
                    //Etant donne qu'on a change de texte, il faut prevenir le Shuffle
                    //qu'on met a jour le texte a melanger. Pour cela, on arrete le Shuffle
                    //en cours et on en relance un nouveau avec le nouveau contenu de la balise html
                    shuffle.stop();
                    el.innerHTML = this.description;
                    shuffle = new ShuffleText(el)
                    shuffle.start();
                }

                //La phrase i n'étant plus affichee, on met son booleen a false pour
                //eviter de le reafficher lors du hover suivant
                this.afficherPhrase[this.j] = false;
                //on incrémente j pour permettre le changement de phrase au hover suivant
                this.j = (this.j) + 1;

                //sauvegarde du nombre de secondes a l'affichage de la phrase
                //pour savoir quand on va autoriser l'affichage de la phrase suivante
                this.ticksPhrase = this.ticks;

                //Des l'affichage de "Elle m'echappe" (quand j vaut 4), on
                //lance le son "les etres indecis"
                if (this.j == 4) {
                    //this.sonMoment = this.son;
                    this.sonMoment.src = this.son;
                    this.sonMoment.volume = 0.005;
                    this.sonMoment.play();
                    this.afficherVideo = true;
                }
            }
        }
    }

    paintCanvas(video, canvas, canvas2DContext) {
        // fonction qui copie la vidéo filtrée dans le canva
        if (video.paused || video.ended) return false;

        //met un rectangle noir sur la vidéo
        canvas2DContext.fillRect(0, 0, 2000, 2000);
        //    canvas2DContext.drawImage(video,0,0); // copie de la vidéo dans le canvas
        //les deux parametres apres video sont les coordonnees de placement de la video
        //le troisième est la largeur , le quatrième est la hauteur
        //placement de la video differents selon si webcam acceptee ou pas
        // if (!this.camAutorisee){
        //canvas2DContext.drawImage(video,475,100);
        //} else {
        //dimensions avant redimensionnement 15 avril:canvas2DContext.drawImage(video,350,90,600,400);
        //canvas2DContext.drawImage(video,475,100,400,30);
        canvas2DContext.drawImage(video, 475, 100, 600, 410);

        //}
        //canvas2DContext.drawImage(video,475,100); // copie de la vidéo dans le canvas
        var data = canvas2DContext.getImageData(0, 0, canvas.width, canvas.height);
        JSManipulate.twirl.filter(data, this.values);
        // application du filtre sur le canva
        // this.value contient les paramètres du twirl
        canvas2DContext.putImageData(data, 0, 0);
        var object = this;

        setTimeout(function () {
            // la fonction sera relancée toutes les 30 ms
            object.paintCanvas(video, canvas, canvas2DContext);
        }, 30);
    }


    setup() {
        var video: any = document.getElementById("video"); // vidéo source (derrière le canva)
        var canvas: any = document.getElementById("canvas"); // vidéo cible (canva)
        //canvas.width=2000;
        //canvas.height=2000;
        var canvas2DContext = canvas.getContext("2d");
        this.paintCanvas(video, canvas, canvas2DContext); //chargement de la vidéo dans le canva
        this.videoBegin = this.ticks // on note le timer de lancement pour connaitre la durée de la vidéo.
        this.cameraIsON = true;
    }

    //ne pas aller ici si on veut changer les valeurs
    //mais a newValues
    values = {
        radius: 200, // rayon du twirl
        angle: 10, // angle de rotation (+/- déformé rapidement en quittant le centre)
        centerX: 0.5, // centre x en % de l'image
        centerY: 0.5 // centre y en % de l'image
    };

    valuesTwirl = {
        radius: 200, // rayon du twirl
        angle: 180, // angle de rotation (+/- déformé rapidement en quittant le centre)
        centerX: 0.5, // centre x en % de l'image
        centerY: 0.5 // centre y en % de l'image
    };

    currentPositionX;
    currentPositionY;


    moveFilter(left, top) {
        // fonction qui replace le filtre en fonction de la position de la souris
        let c: any = document.getElementById("canvas");
        let canvaPosition = c.getBoundingClientRect();
        // début de la transcription en coordonnées utilisées par la lib
        let positionInCanvaX = left - canvaPosition.left + 300;
        let positionInCanvaY = top - canvaPosition.top + 50;
        let canvaHeight = c.offsetHeight;
        let canvaWidth = c.offsetWidth;

        let xRelativePosition = positionInCanvaX / canvaWidth;
        let yRelativePosition = positionInCanvaY / canvaWidth;
        // fin de la transcription en coordonnées utilisées par la lib


        // remplacement des anciennes coordonnées par les nouvelles
        //changer ici les valeurs si on le souhaite, pas dans values
        let newAngle;
        newAngle = this.values.angle += 2; // de base on incrémente de 1
        if (newAngle > this.angleMvtForce) { // on incrémente plus vite si l'utilisateur n'a plus le contrôle
            newAngle += 2;
        }

        let newValues = {
            radius: 400,
            angle: newAngle,
            centerX: xRelativePosition,
            centerY: yRelativePosition,
        };

        this.values = newValues;
    }

    //changementAutorise = true;
    //booleen permettant de faire en sorte a ce que la methode ne soit appelee que toutes
    //les 150 ms
    changementAutorise: boolean = true;
    changementDemarre: boolean;
    randomActivated = false;
    //randomActivated=true;
    TIME_BTW_MOVE = 200;
    NB_STEPS_BTW_CNG = 10;
    nbStepsBeforeChange = this.NB_STEPS_BTW_CNG;
    xdir = 0;
    ydir = 0;
    angleMvtForce = 100; // angle de déformation à partir duquel on impose le déplacement de la caméra

    clientMoveFilter(event: MouseEvent) {
        //changement du filtre seulement lorsque la camera a ete lancee
        if (this.changementDemarre) {
            // console.log(this.values.angle, this.angleMvtForce, this.changementAutorise);
            if (this.values.angle < this.angleMvtForce && this.changementAutorise) {
                this.changementAutorise = false;
                setTimeout(() => { this.changementAutorise = true; }, 150);
                this.currentPositionX = event.pageX;
                this.currentPositionY = event.pageY;
                this.moveFilter(this.currentPositionX, this.currentPositionY);
                this.randomActivated = false;
            }
            else if (!this.randomActivated && this.changementAutorise) {
                this.randomActivated = true;
                this.randomMoveFilter();
            }
        }
    }

    randomMoveFilter() {
        //console.log('hey! ');
        this.nbStepsBeforeChange--;
        if (this.nbStepsBeforeChange == 0) {
            this.nbStepsBeforeChange = this.NB_STEPS_BTW_CNG;
            this.xdir = (Math.random() - 0.5) * 30;
            this.ydir = (Math.random() - 0.5) * 30;
            console.log(this.xdir, this.ydir);
            while (this.currentPositionX + this.xdir * this.NB_STEPS_BTW_CNG > 800 || this.currentPositionY + this.ydir * this.NB_STEPS_BTW_CNG > 800 || this.currentPositionX + this.xdir * this.NB_STEPS_BTW_CNG < 0 || this.currentPositionY + this.ydir * this.NB_STEPS_BTW_CNG < 0) {
                this.xdir = (Math.random() - 0.5) * 30;
                this.ydir = (Math.random() - 0.5) * 30;
            }
        }
        this.currentPositionX += this.xdir;
        this.currentPositionY += this.ydir;
        this.moveFilter(this.currentPositionX, this.currentPositionY);
        setTimeout(() => { this.randomMoveFilter(); }, this.TIME_BTW_MOVE);
    }

    onPlayFunction() {
        // application du filtre vidéo à chaque image
        var video: any = document.getElementById("video");
        var canvas: any = document.getElementById("canvas");
        var canvas2DContext = canvas.getContext("2d");

        canvas.height = video.clientHeight;
        canvas.width = video.clientWidth;
        this.paintCanvas(video, canvas, canvas2DContext);
    }

    setVideo() {
        // obtention des accès à la webcam -> mise dans la balise id=video
        var video: any = document.getElementById('video');
        // Get access to the camera!
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            // Not adding `{ audio: true }` since we only want video now
            this.changementDemarre = true;
            navigator.mediaDevices.getUserMedia({ video: true }).then(function (stream) {
                video.srcObject = stream;
                //this.camAutorisee=true;
                video.play();
            }).catch((ex) => {
                console.error('Error fetching users', ex);
                //Si la demande de video a l'utilisateur a echoue, on recupere la video par defaut
                this.cameraIsON = false;
                video.src = this.videoDefaut;
                video.muted = true; //on ne veut pas le son de la video par defaut
                video.play();
                this.changementDemarre = true;
                //this.camAutorisee=false;
            });
            this.video = video;
        }
    }
}
