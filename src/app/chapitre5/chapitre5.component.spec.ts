import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Chapitre5Component } from './chapitre5.component';

describe('Chapitre5Component', () => {
  let component: Chapitre5Component;
  let fixture: ComponentFixture<Chapitre5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Chapitre5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Chapitre5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
