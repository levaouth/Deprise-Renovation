import { Component, InjectionToken } from '@angular/core';
import { TranslateService }   from './translate';
import { routes } from './app.router';
//import "pixi.js";
//import "howler";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
    constructor(public _translate: TranslateService) {
        this.lang = _translate.currentLang;
    }
    lang;
  reload(){
    setTimeout(function(){
          window.location.reload();
    });
  }

}
