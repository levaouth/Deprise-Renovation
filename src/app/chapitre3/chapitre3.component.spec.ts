import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Chapitre3Component } from './chapitre3.component';

describe('Chapitre3Component', () => {
  let component: Chapitre3Component;
  let fixture: ComponentFixture<Chapitre3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Chapitre3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Chapitre3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
