/*code de baptiste a debug */
import { Component, OnInit,trigger, keyframes, animate, transition, style, state, NgZone, ViewChild} from '@angular/core';
import {Observable,Subscription} from 'rxjs/Rx';
import { TranslateService }   from '../translate';
import {HostListener} from '@angular/core';
import ShuffleText from 'shuffle-text';

import {Flatterieaffichee} from '../flatterieaffichee';
import {Flatterie} from '../flatterie';
//import * as howler from "howler";
//import * as audioApi from "web-audio-api";
//import * as angAudio from "angular2-audio-component";
@Component({
  selector: 'app-chapitre3',
  templateUrl: './chapitre3.component.html',
  styleUrls: ['./chapitre3.component.css'],
  providers: [TranslateService],
  animations: [
    trigger('wobble', [   //gestion de l'animation de la phrase "mot d'amour ou de rupture?"
      transition('inactive => active', animate(1000, keyframes([
        style({transform: 'translate3d(0, 800%, 0)', offset: 1})
      ]))),
    ]),
    /*trigger('wobbleVers', [   //gestion de l'animation de la phrase "mot d'amour ou de rupture?"
      transition('inactive => active', animate(1000, keyframes([
        style({transform: 'translate3d(0, 800%, 0)', offset: 1})
      ]))),
    ]),*/
    /*trigger('deplacementVers', [
      transition('inactive => active', animate(1000, keyframes([
        style({transform: 'translate3d(0, 800%, 0)', offset: 1})
      ])))
    ]),*/
    /*trigger('visibilityChanged1', [
      state('hidden', style({ opacity: 0 })),
      state('shown', style({ opacity: 1 })),
      transition('hidden => shown', animate('3000ms')),
    ])*/
    /*trigger('visibilityChanged1', [
      state('hidden', style({ opacity: 0 })),
      state('shown', style({ opacity: 1 })),
      transition('hidden => shown', animate('3000ms')),
    ])*/
  ]
})
export class Chapitre3Component implements OnInit {

  constructor( private _translate: TranslateService ) { }

  //Tableau des sons du chapitre 2 (meme principe que dans le chapitre1)
  //sons : string[] = ["assets/sound/chapitre3/public_fond_sonore_01.mp3"]; // REPLACE_ME

  Phrase : string[] = ["3",
                       "Vingt ans se sont écoulés depuis notre première rencontre.",
                       "Ce matin je me perds dans un mot qu'elle m'a laissé.",
                       "Tout se brouille dans mon esprit.",
                       "Je ne sais comment l'interpréter.",
                       "Mot d'amour ou de rupture ?",
                       "Que puis-je faire?"];

  Poeme : string[] = ["Je sais que c'est pour toi un choc",
                      "\"Je n'ai que de l'amour pour toi\"",
                      "Est un mensonge, et",
                      "\"Dans un couple, il y en a un qui souffre et un qui s'ennuie\"",
                      "Je veux que tous nos amis sachent que",
                      "Je ne veux pas rester avec toi",
                      "Depuis le premier jour, je ne sais pas comment tu peux croire que",
                      "Je t'aime",
                      "Mon amour",
                      "A disparu",
                      "L'indifférence",
                      "Est plus vivace que jamais",
                      "Le charme de notre rencontre",
                      "S'est dissipé à présent",
                      "Et le moindre malentendu",
                      "A vaincu",
                      "Notre amour"];

  //On lance le bruit de foule dès le lancement du chapitre 2
  //sonMoment=this.sons[0];
  phraseParlee="";
  ticks =0; //compteur de secondes ecoulees depuis le lancement du chapitre3, comme dans le chapitre 1
  ticksPhrase:number; //seconds ecoulees depuis l'affichage de la derniere phrase
  ticksAnim: number; //nombre de secondes au début de l'animation (qui permet de savoir quand on autorise le shuffle vers "que puis-je faire?")
  description=this.Phrase[0];

  //tableau dont la valeur i indique si la Phrase[i] est affichee ou non
  //(pratique pour eviter d'afficher la meme phrase sur deux passages de la souris
  // d'affilée par exemple). La premiere valeur est ici a true car le chapitre 2
  //commence directement avec l'affichage d'un 2
  afficherPhrase: boolean[] = [true,
                              false,
                              false,
                              false,
                              false,
                              false,
                              false,
                              false,
                              false,
                              false,
                              false,
                              false,
                              false]

  rienAfficher = false;
  j=1; //j parcours de tableau comme dans le chapitre 1
  //commence ici a 1 car les hover commencer a partir de Phrase[1] dans ce chapitre

  //entier permettant de changer la taille du 2 mais pas des autres textes du chapitre
  fontsize:number=400; //tout de suite initialises car le 2 est le premier texte affiche dans le chapitre
  top:number=10; //pour recentrer le 2 sur la page après l'avoir agrandi


  //liste des phrases que le narrateur dit a la fille qui lui plait
  //qui s'affichent toutes d'un coup apres le "impossible de prononcer qqch de coherent"
  //Les flatteriesAffiches sont stockees dans un json, on a besoin de garder l'id ET la string correspondante
  //L'interface flatterieAffichee a ete declaree dans flatterieaffichee.ts, qui fait partie des imports
  //Ces flatteriesEcran sont celles qui seront affichees dans le fichier .html des que leur attribut
  //css "hidden" sera remplace par "bloc" (c'est fait juste apres le 'impossible de prononcer qqch de coherent')
  //dans la methode 'afficherFlatteries'
  //permet de gerer la position des phrases apres deplacement
  public margintop: Number;
  //display permet d'indiquer qu'on veut afficher la balise au moment voulu (au debut c'est hidden vu qu'il s'agit des flatteries)
  public display: string;
  //wobbleState indique l'etat de l'animation
  //son passage de "inactive" a "active" la declenche
  public wobbleState: string="inactive";

  //besoin de ces deux attributs pour faire fonctionner le timer
  private timer;
  private sub: Subscription;



  //boolean apparition des phrases
    bolleanApparitionPhrase: boolean[]  = [true, false, false];

    carmen = new Audio();
    carmen_reverse = new Audio();

    reverse = false;

    //permet de regler l'espace entre les lignes en fonction du deplacement
    //de la souris lors de l'affichage du poeme
    interligne : number;
    //permet d'afficher le poeme au bon moment de la scene 3
    afficherPoeme : string;

    //affiche le vers
   afficherVers: string;

   public display0: string="hidden";
   public display1: string="hidden";
   visibilityState0: string="hidden";
   visibilityState1: string='hidden';
   //EtatDeplacementVers: string= "inactive";

   //pour afficher le texte correctement, on prend la position du curseur
   positionCurseur: number;
   tailleFenetre: number;

   animationFinie: boolean = false; //autorise le passage à "que puis-je faire"
   //seulement si l'animation est finie

  ngOnInit() {

    this.Phrase = ["3",
                         this._translate.instant("Vingt ans se sont écoulés depuis notre rencontre."),
                         this._translate.instant("Ce matin, je me perds dans un mot qu'elle m'a laissé."),
                         this._translate.instant("Tout se brouille dans mon esprit."),
                         this._translate.instant("Je ne sais comment l'interpréter."),
                         this._translate.instant("Mot d'amour ou de rupture ?"),
                         this._translate.instant("Le fait-elle exprès ?")];

    this.Poeme = [this._translate.instant("Je sais que c'est pour toi un choc"),
                        this._translate.instant("Je n'ai que de l'amour pour toi"),
                        this._translate.instant("Est un mensonge, et"),
                        this._translate.instant("Dans un couple, il y en a un qui souffre et un qui s'ennuie"),
                        this._translate.instant("Je veux que tous nos amis sachent que"),
                        this._translate.instant("Je ne veux pas rester avec toi,"),
                        this._translate.instant("Depuis le premier jour, je ne sais pas comment tu peux croire que"),
                        this._translate.instant("Je t'aime"),
                        this._translate.instant("Mon amour"),
                        this._translate.instant("A disparu"),
                        this._translate.instant("L'indifférence"),
                        this._translate.instant("Est plus vivace que jamais"),
                        this._translate.instant("Le charme de notre rencontre"),
                        this._translate.instant("S'est dissipé à présent"),
                        this._translate.instant("Et le moindre malentendu"),
                        this._translate.instant("A vaincu"),
                        this._translate.instant("Notre amour")];




    //lancement d'un timer des le lancement de la page comme dans le chapitre 1
    this.timer = Observable.timer(0,1000);
    this.sub=this.timer.subscribe((t)=>
      this.tickerFunc(t)
     );

    /*this.carmen_reverse.src = "assets/sound/chapitre3/carmen_reverse.mp3";
    this.carmen_reverse.loop = true;
    this.carmen.src = "assets/sound/chapitre3/carmen.mp3";
    this.carmen.playbackRate = 1;
    this.carmen.play();
    this.carmen.loop = true;*/

  }


  tickerFunc(tick){
    this.ticks = tick;
    //On affiche le 2 pendant 2 secondes, puis la premiere phrase du tableau
    //et on indique dans le tableau de booleens que la phrase 1 est affichee
    //et que la phrase 0 ne l'est plus
    if (this.ticks >= 3 && this.afficherPhrase[0]) {
       //On remet le texte a une taille correcte apres l'agrandissement du numero de chapitre
       this.fontsize=25;
       this.top=45;
       this.description=this.Phrase[1];
       this.afficherPhrase[0]=false;
       this.afficherPhrase[1]=true;
     }

     //Si j=5, ça veut dire qu'on est à la phrase "Mot d'amour ou de rupture ?"
     //on lance alors l'animation
     if (this.j==5){
        //on charge deja alors le poeme pour faire apparaitre
        //progressivement les vers tandis que la phrase
        //se place correctement
        this.afficherPoeme="block";
        this.wobbleState = "active";
       }

       /*console.log(this.ticks);
       console.log("TICKS ANIM "+this.ticksAnim);*/
       if (this.ticks-this.ticksAnim > 30){
         console.log("PHRASE SUIVANTE");
         //s'il s'est écoulé plus de 20 secondes depuis le début de l'animation, affichage de "que puis-je faire?"
         this.animationFinie=true;
         this.phraseSuivante();
       }
     }

   placerCorrectement(){
   //Attention, la hauteur ne change qu'à partir de la phrase "Mot d'amour ou de rupture?" (ici la phrase 5)
     if (this.j==5){
       //console.log('HEAY');
       this.margintop=90; //besoin de rehausser un peu la phrase par rapport a l'endroit ou elle essaie d'aller
       //on peut alors lancer les musiques
       this.carmen_reverse.src = "assets/sound/chapitre3/carmen_reverse.mp3";
       this.carmen_reverse.loop = true;
       this.carmen.src = "assets/sound/chapitre3/carmen.mp3";
       this.carmen.playbackRate = 1;
       this.carmen.play();
       this.carmen.loop = true;
       //lanccement du compteur pour savoir si on peut passer a la phrase suivante
        this.ticksAnim=this.ticks;
       //on affiche le premier vers du poeme

       //this.visibilityState1='shown';
       //this.EtatDeplacementVers="active";
       console.log("CHANGEMENT VISIBLITE "+this.visibilityState1);
     }
   }

   /*placerVersCorrectement(){
      if (this.j==5){
        this.
      }
   }*/


   changementInterdit(i:number) : boolean{
     //pas de changement s'il s'est ecoule moins de deux secondes depuis le dernier affichage de phrase
     if (this.ticks -this.ticksPhrase < 1) {
      //console.log("CHANGEMENT INTERDIT");
       return true;
     } else if (i==5){ //si i vaut 5, on interdit le changement si l'animation n'est pas finie
       if (this.animationFinie){
         return false;
       } else {
         return true;
       }
     } else { //Si i vaut autre chose, on autorise le changement vers la phrase suivante
       return false;
     }
   }

   //Changement de texte au passage de la souris sur celui-ci
  phraseSuivante(){

    //Lors d'un hover, on lance le shuffle SAUF si le texte affiche a l'écran
    //est un 2 ou sur la phrase "impossible de prononcer qqch de coherent"

      //if (!this.afficherPhrase[0] && this.j!=5 && this.description!=this.Phrase[this.Phrase.length-1]){
      if (!this.afficherPhrase[0] && this.description!=this.Phrase[this.Phrase.length-1] && !this.changementInterdit(this.j)){
       var el = document.getElementById("chap3");
       var shuffle = new ShuffleText(el);
       shuffle.start();
     }

        // if (this.afficherPhrase[this.j] && this.j!=5){
          if (this.afficherPhrase[this.j] && !this.changementInterdit(this.j)){
          //On autorise le passage de la phrase suivante en changeant le booleen
          //correspondant, sauf sur la phrase "Impossible de prononcer qqch de coherent"
          //(auquel cas on interdit l'incrémentation de j)

            if ((this.j)+1<this.Phrase.length){
                 this.afficherPhrase[(this.j)+1]=true;
                 this.description=this.Phrase[(this.j)+1]
                 //console.log(this.description);
                 //console.log(this.j)
                 //Etant donne qu'on a change de texte, il faut prevenir le Shuffle
                 //qu'on met a jour le texte a melanger. Pour cela, on arrete le Shuffle
                 //en cours et on en relance un nouveau avec le nouveau contenu de la balise html
                 shuffle.stop();
                 el.innerHTML=this.description;
                 shuffle = new ShuffleText(el)
                 shuffle.start();
             }

         //La phrase i n'étant plus affichee, on met son booleen a false pour
         //eviter de le reafficher lors du hover suivant
         this.afficherPhrase[this.j]=false;
         //on incrémente j pour permettre le changement de phrase au hover suivant
         this.j=(this.j)+1;

         //sauvegarde du nombre de secondes a l'affichage de la phrase
         //pour savoir quand on va autoriser l'affichage de la phrase suivante
         this.ticksPhrase=this.ticks;

       }
  }

  //@HostListener('mousemove', ['$event'])
  changeMusicEtInterligne(event: MouseEvent){

    var body = document.body,
    html = document.documentElement;
    var windowSize = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight );
    //var windowSize=parseInt(document.getElementById("vers0").style.top);

    //var windowSize = document.innerHeight;
    this.tailleFenetre=windowSize;
    var mouseYPosition = event.clientY;
    this.positionCurseur=mouseYPosition;
    /*console.log(mouseYPosition);
    console.log("LA MOITIE DE LA HAUTEUR DE LA FENETRE "+windowSize/2); */
    //si la souris est dans la moitie haute de l'ecran, on met carmen a l'envers
    if(mouseYPosition <= windowSize/2){

    //décommenter le bloc ci-dessous si jamais on a un doute si la position de la moitie de l'ecran est la bonne
    if (mouseYPosition==windowSize/2){
      console.log ("MOITIE DE L'ECRAN");
    }
      if(this.reverse){
        //Si la souris vient d'arriver en haut de l'ecran, on lance carmen en sens normal
        var bufferd = this.carmen_reverse.currentTime;
        this.carmen.currentTime = 46 - bufferd;
        this.carmen.playbackRate = 0.5 + ((mouseYPosition - (windowSize/2))/(windowSize/2))/2;
        this.carmen.play();
        this.reverse = false;
        this.carmen_reverse.pause();
      }
      else{
      //si reverse vaut true, ca veut dire que la souris ne vient pas d'arriver
      //en haut de l'ecran, on s'occupe seulement de la vitesse de lecture de la musique
        this.carmen.playbackRate = 1 - ((mouseYPosition )/(windowSize/2))/2 ;
      }
      //console.log("souris en haut");
      //if (this.interligne > -10){ //on fixe une limite pour l'interligne du haut pour éviter que le poeme ne parte trop haut
        this.interligne=(mouseYPosition-(windowSize/2))*0.01-2; //l'interligne doit etre negatif
        //console.log("L'INTERLIGNE :" + this.interligne);
      //}

    } //si la souris est en bas de l'ecran, on met carmen dans le sens normal
    else if (mouseYPosition > windowSize/2){
        if(!this.reverse){
          var bufferd = this.carmen.currentTime;
          this.carmen_reverse.currentTime = 46 - bufferd;
          this.carmen.playbackRate = 1 - ((mouseYPosition )/(windowSize/2))/2;
          this.carmen_reverse.play();
          this.reverse = true;
          this.carmen.pause();
        }
        else{
          this.carmen.playbackRate = 0.5 + ((mouseYPosition - (windowSize/2))/(windowSize/2))/2;
        }
        //puis quelque soit la valeur de reverse, on regle l'interligne
        //if (this.interligne<15) { //on fixe une valeur maximum a l'interligne
          this.interligne=(mouseYPosition-(windowSize/2))*0.01-2;
        /*  console.log("L'INTERLIGNE :" + this.interligne); */
        //}
    }

    }


}
