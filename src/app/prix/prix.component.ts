import { Component, OnInit } from '@angular/core';
import { TranslateService } from '../translate'

@Component({
  selector: 'app-prix',
  templateUrl: './prix.component.html',
  styleUrls: ['./prix.component.css']
})
export class PrixComponent implements OnInit {

  constructor( private _translate: TranslateService ) { }

  ngOnInit() {
  }

}
