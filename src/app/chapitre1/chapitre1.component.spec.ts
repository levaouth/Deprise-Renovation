import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Chapitre1Component } from './chapitre1.component';

describe('Chapitre1Component', () => {
  let component: Chapitre1Component;
  let fixture: ComponentFixture<Chapitre1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Chapitre1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Chapitre1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
