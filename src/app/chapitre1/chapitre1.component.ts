import { Component, OnInit, trigger, animate, transition, style, state, EventEmitter, ViewEncapsulation } from '@angular/core';
import { Observable, Subscription } from 'rxjs/Rx';
import { TranslateService } from '../translate';

import { HostListener } from '@angular/core';
import ShuffleText from 'shuffle-text';
import { EventListener } from '@angular/core/src/debug/debug_node';
import { Event } from '@angular/router/src/events';
//import { eventNames } from 'cluster';
import { EventManager } from '@angular/platform-browser/src/dom/events/event_manager';
import { AppComponent } from '../app.component';
//import { setInterval } from 'timers';


@Component({
    selector: 'app-chapitre1',
    templateUrl: './chapitre1.component.html',
    styleUrls: ['./chapitre1.component.css'],

    //Test un peu nul d'apparition de "je suis maitre de mon destin" en fondu
    /*animations: [
       trigger('visibilityChanged', [
          state('shown', style({ opacity: 1 })),
          state('hidden', style({ opacity: 0 })),
          transition('hidden => shown', animate('2000ms')),
        ])
      ] */
    providers: [TranslateService],
    encapsulation: ViewEncapsulation.None,
})


export class Chapitre1Component implements OnInit {

    constructor(private _translate: TranslateService) {
    }
    numChapitre = "1";

    description = this._translate.instant("Avez-vous pensé à allumer vos enceintes ?");
    Phrase: string[] = [this._translate.instant("Toute ma vie, j'ai cru avoir devant moi un champ des possibles infini."),
    this._translate.instant("\"L'univers entier m'appartient\", pensais-je."),
    this._translate.instant("J'ai le choix."),
    this._translate.instant("Je suis maître de mon destin."), //click ici
    this._translate.instant("Je peux prendre ce qui me plaît."),
    this._translate.instant("Je deviendrai ce que je veux."),
    this._translate.instant("J'ai tracé mon propre chemin."),
    this._translate.instant("J'ai parcouru de magnifiques paysages."),
    this._translate.instant("Quoi de plus naturel, je les avais choisis."),
    this._translate.instant("Mais depuis un moment, j'ai des doutes."),
    this._translate.instant("Comment avoir prise sur ce qui m'arrive ?"),
    this._translate.instant("Tout s'échappe."),
    this._translate.instant("Me glisse entre les doigts."),
    this._translate.instant("Les objets, les personnes."),
    this._translate.instant("J'ai l'impression de ne plus rien contrôler."),
    this._translate.instant("Depuis quelques temps maintenant,"),
    this._translate.instant("Je n'attends qu'une chose."),
    this._translate.instant("La suite.")
    ] //attente + appui sur n'importe quelle touche

    phraseFin = this._translate.instant("Et le rendez-vous est arrivé.");
    aideUtilisateur = this._translate.instant("(Appuyez sur n'importe quelle touche pour continuer)");

    //tableau des différents paths des sons du chapitre1
    //rappel: ne jamais mettre des string en dur dans le code mais dans des tableaux
    //fichiers txt ou bdd pour eviter de devoir chercher partout en cas de changement
    //de code
    sons: string[] = ["assets/sound/chapitre1/BienvenueAppuyez.mp3",
        "assets/sound/chapitre1/BipEtBravo.mp3",
        "assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous.mp3",
        "assets/sound/chapitre1/BravoVotreRendezVousEstArrive.mp3"
    ]

    notesMusique: string[] = ["assets/sound/chapitre1/noteMusique/n1.mp3",
        "assets/sound/chapitre1/noteMusique/n2.mp3",
        "assets/sound/chapitre1/noteMusique/n3.mp3",
        "/assets/sound/chapitre1/noteMusique/n4.mp3",
        "assets/sound/chapitre1/noteMusique/n5.mp3",
        "assets/sound/chapitre1/noteMusique/n6.mp3",
        "assets/sound/chapitre1/noteMusique/n7.mp3",
        "assets/sound/chapitre1/noteMusique/n8.mp3",
        "assets/sound/chapitre1/noteMusique/n9.mp3",
        "assets/sound/chapitre1/noteMusique/n10.mp3",
        "assets/sound/chapitre1/noteMusique/n11.mp3",
        "assets/sound/chapitre1/noteMusique/n12.mp3",
        "assets/sound/chapitre1/noteMusique/n13.mp3",
        "assets/sound/chapitre1/noteMusique/n14.mp3",
        "assets/sound/chapitre1/noteMusique/n15.mp3",
        "assets/sound/chapitre1/noteMusique/n16.mp3",
        "assets/sound/chapitre1/noteMusique/n17.mp3",
        "assets/sound/chapitre1/noteMusique/n18.mp3",
        "assets/sound/chapitre1/noteMusique/n19.mp3",
        "assets/sound/chapitre1/noteMusique/n20.mp3",
        "assets/sound/chapitre1/noteMusique/n21.mp3",
        "assets/sound/chapitre1/noteMusique/n22.mp3",
    ]
    //tableau de booléens dont l'element i autorise l'affichage de la Phrase[i]
    //s'il est à true et l'interdit sinon (initialisé à false vu qu'au debut on veut
    //juste la description par defaut
    afficherPhrase: boolean[] = [false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false]
    rienAfficher: boolean = true;

    //booleen indiquant si la derniere phrase du chapitre 1 est affichee ou non.
    Fini: boolean = false;
    //permet de connaitre le nombre de secondes ecoulees depuis le lancement de la page
    ticks = 0;
    ticksPhrase: number; //seconds ecoulees depuis l'affichage de la derniere phrase
    //j est un entier de parcours de tableau (erhhh...)
    j = 0;
    // Si il y en a plus, on supprime le premier rond
    nbrMaxRond = 40;

    //permet de compter le nombre de caractere à changer
    effetMelangeCompteur = 0;

    leftSide = "";
    rightSide = "";


    charAuHasard;
    tabChar: string[] = [
        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
    ]

    ttl = 5;

    //entier permettant de changer la taille du 1 mais pas des autres textes du chapitre
    fontsize: number = 25; //initialiser a 25 pour que "Avez-vous pense...encintes?" soit grand aussi
    top: number; //pour recentrer le 1 sur la page après l'avoir agrandi


    //Un changement de la valeur de cet attribut change le style de la souris sur la page
    //On s'en sert pour changer la souris en petite main sur "je suis maitre de mon destin"
    curseur;



    //Vieux souvenirs de la tentative de shuffle "a la main" avant d'installer shuffle-text.js
    /*effetMelangeCompteur = 0; //permet de compter le nombre de caractere à changer
    leftSide="";
    rightSide="";
    charAuHasard;
    tabChar : string[] = [
      "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
      "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"
    ]
    ttl=5;*/

    //Tableau contenant les type de couleur des ronds
    typePointCouleur: string[] = [
        "rondBleu",
        "rondRouge",
        "rondVert",
        "rondJaune",
        "rondViolet",
        "rondRose",
        "rondMarron",
        "rondOrange"
    ];

    //Tableau contenant les type de taille des ronds
    typePointTaille: string[] = [
        "rondPetit",
        "rondMoyen",
        "rondGrand"
    ];
    positionXCurseur = 0;
    positionYCurseur = 0;
    //position du curseur de la souris. Les données à l'initialisation n'ont pas d'importance.


    explosionInterval;
    creationLockedInterval;
    // si on peux creer un point quand on clique
    allowedToCreate = true;
    intervalDejaCree = false;


    sonMoment: string; //path du son actuellement lance sur la page

    noteMusique: string;
    noteMusique2: string;
    noteMusique3: string;
    noteMusique4: string;
    //besoin de ces deux attributs pour faire fonctionner le timer
    private timer;
    private sub: Subscription;

    //On laisse d'abord le message pendant trois secondes, puis on affiche un 1
    //pendant 3 secondes, puis on n'affiche plus rien tant que l'utilisateur
    //n'a pas appuye sur une touche

    ngOnInit() {

        this.description = this._translate.instant("Avez-vous pensé à allumer vos enceintes ?");
        this.Phrase = [this._translate.instant("Toute ma vie, j'ai cru avoir devant moi un champ des possibles infini."),
        this._translate.instant("\"L'univers entier m'appartient\", pensais-je."),
        this._translate.instant("J'ai le choix."),
        this._translate.instant("Je suis maître de mon destin."), //click ici
        this._translate.instant("Je peux prendre ce qui me plaît."),
        this._translate.instant("Je deviendrai ce que je veux."),
        this._translate.instant("J'ai tracé mon propre chemin."),
        this._translate.instant("J'ai parcouru de magnifiques paysages."),
        this._translate.instant("Quoi de plus naturel, je les avais choisis."),
        this._translate.instant("Mais depuis un moment, j'ai des doutes."),
        this._translate.instant("Comment avoir prise sur ce qui m'arrive ?"),
        this._translate.instant("Tout s'échappe."),
        this._translate.instant("Me glisse entre les doigts."),
        this._translate.instant("Les objets, les personnes."),
        this._translate.instant("J'ai l'impression de ne plus rien contrôler."),
        this._translate.instant("Depuis quelques temps maintenant,"),
        this._translate.instant("Je n'attends qu'une chose."),
        this._translate.instant("La suite.")
        ] //attente + appui sur n'importe quelle touche

        this.phraseFin = this._translate.instant("Et le rendez-vous est arrivé.");
        this.aideUtilisateur = this._translate.instant("(Appuyez sur n'importe quelle touche pour continuer)");

        this.sons = [this._translate.instant("assets/sound/chapitre1/BienvenueAppuyez.mp3"),
        this._translate.instant("assets/sound/chapitre1/BipEtBravo.mp3"),
        this._translate.instant("assets/sound/chapitre1/BonjourSiVousVoulezQueVotreRendezVous.mp3"),
        this._translate.instant("assets/sound/chapitre1/BravoVotreRendezVousEstArrive.mp3")
        ];





        this.timer = Observable.timer(0, 1000);
        this.sub = this.timer.subscribe((t) =>
            this.tickerFunc(t)
        );

        //choix de la langue dans ngOnInit ici
        console.log(this._translate.instant('hello world'));
    }



    pointCouleur(event: MouseEvent) {

        //Récupère la position du curseur de la souris en px
        //On soustrait par un nombre pour régler le décalage car event.client récupere les coordonnée de l'écran et non du cadre.

        if (this.j >= 3 && this.j != this.Phrase.length) {

            //  if(this.j <= 5){
            var lecteurHasard = Math.floor(Math.random() * 3);
            var noteHasard = Math.floor(Math.random() * 22);
            if (lecteurHasard > 0 && lecteurHasard <= 1) {
                this.noteMusique = this.notesMusique[noteHasard];
            }
            else if (lecteurHasard > 1 && lecteurHasard <= 2) {
                this.noteMusique2 = this.notesMusique[noteHasard];
            }
            else if (lecteurHasard > 2 && lecteurHasard <= 3) {
                this.noteMusique3 = this.notesMusique[noteHasard];
            }
            else {
                this.noteMusique4 = this.notesMusique[noteHasard];
            }
            //  }

            //les points de couleur ne s'affichent qu'à partir de 'je suis maitre de mon destin'
            //soit le troisieme element du tableau des phrases. La variable de parcours j
            //doit donc etre egale a au moins 3 pour permettre les points de couleur
            var taillePointHasard = Math.floor(Math.random() * 8);
            //Permet de ne changer les probabilités
            var taillePointProba;
            // console.log(event.clientX);
            if (taillePointHasard >= 0 && taillePointHasard < 2) {
                var curseurXPosition = event.clientX - 11;
                var curseurYPosition = event.clientY - 20;
                taillePointProba = 0;
            }
            else if (taillePointHasard >= 2 && taillePointHasard < 4) {
                var curseurXPosition = event.clientX - 22;
                var curseurYPosition = event.clientY - 30;
                taillePointProba = 1;
            }
            else {
                var curseurXPosition = event.clientX - 48;
                var curseurYPosition = event.clientY - 50;
                taillePointProba = 2;
            }

            var typePointHasard = Math.floor(Math.random() * 8);
            //Crée une nouvelle div
            var pointCouleur = document.createElement("div");

            var cadre = document.getElementById("cadre");
            cadre.appendChild(pointCouleur);
            pointCouleur.className = "rond " + this.typePointCouleur[typePointHasard] + " " + this.typePointTaille[taillePointProba];
            pointCouleur.style.top = curseurYPosition.toString() + "px";
            pointCouleur.style.left = curseurXPosition.toString() + "px";

            var ronds = document.getElementsByClassName("rond");
            if (ronds.length >= this.nbrMaxRond) {
                ronds[0].parentNode.removeChild(ronds[0])
            }
            //  pointCouleur.style.transitionDuration = Math.floor(Math.random() * 4).toString() + "s";



            //La div devient enfant du cadre



            //ajout de la balise de la class rond
            //  pointCouleur.innerHTML = "<div _ngcontent-c1 class='rond " + this.typePointCouleur[typePointHasard] + " " +this.typePointTaille[taillePointProba] +"' style='top: "+ curseurYPosition.toString() + "px; left: " +  curseurXPosition.toString() + "px'></div>";

        }

    }
    /*
      getMousePosition(event: MouseEvent){
    
        var newPositionXCurseur = event.clientX;
        var newPositionYCurseur = event.clientY;
    
        this.positionXCurseur = newPositionXCurseur;
        this.positionYCurseur = newPositionYCurseur;
    
      }*/

    changementDePosition(event: MouseEvent) {
        var ronds = document.getElementsByClassName("rond");
        if (this.j <= 9) {
            if (this.j > 5 && this.j < 15 && this.allowedToCreate) {
                this.allowedToCreate = false;
                this.pointCouleur(event);
            }
            // chaque fois la souris deplace un nouveau point genere
            if (this.j > 6) {
                // WHY??
                var positionX = event.clientX - 30;
                var positionY = event.clientY - 100;

                if (ronds.length > 0) {

                    var i = 0;
                    for (i; i < ronds.length; i++) {

                        ronds[i].setAttribute("style", "top:" + positionY + "px; left: " + positionX + "px");

                    }

                }

            }
        }

        //*le booleen allowToCreate permet dans ce cas la definir un intervalle de temps entre la supression de deux point de couleur
        /*if(this.j > 13 && ronds.length > 0){
    
          var randomRond = Math.floor(Math.random() * ronds.length);
        document.getElementById("cadre").removeChild(ronds[randomRond]);
    
    
        }*/
        if (this.j > 15 && ronds.length > 0) {
            var randomRond = Math.floor(Math.random() * ronds.length);
            document.body.style.cursor = "auto";
            for (i = 0; i < ronds.length; i++) {
                document.getElementById("cadre").removeChild(ronds[randomRond]);

                clearInterval(this.explosionInterval);
                this.intervalDejaCree = false;

            }
        }



        // }
    }



    explosion() {

        var ronds = document.getElementsByClassName("rond");
        if (ronds.length > 0) {



            var noteHasard = Math.floor(Math.random() * 22);
            var noteHasard2 = Math.floor(Math.random() * 22);
            var noteHasard3 = Math.floor(Math.random() * 22);
            var noteHasard4 = Math.floor(Math.random() * 22);

            this.noteMusique = this.notesMusique[noteHasard];
            this.noteMusique2 = this.notesMusique[noteHasard2];
            this.noteMusique3 = this.notesMusique[noteHasard3];
            this.noteMusique4 = this.notesMusique[noteHasard4];




            if (ronds[0] != null) {


                var i = 0;
                for (i; i < ronds.length; i++) {
                    var randomTop = (Math.round(Math.random()) * 2 - 1) * (Math.floor(Math.random() * 75)) + 50;
                    var randomLeft = (Math.round(Math.random()) * 2 - 1) * (Math.floor(Math.random() * 75)) + 50;
                    ronds[i].setAttribute("style", "top:" + randomTop + "%; left: " + randomLeft + "%;");

                }
            }
        }
    }


    tickerFunc(tick) {
        //console.log(this.ticks);
        this.ticks = tick;
        //On laisse deux secondes la question des enceintes, puis on affiche le
        //numero du chapitre sans oublier d'agrandir la taille du texte
        if (this.ticks >= 2 && this.ticks < 3) {
            this.description = this.numChapitre;
            this.fontsize = 400;
            this.top = 5;
        }
        else if (this.ticks >= 5) {
            //Une fois que le 1 est affiché, on n'affiche plus rien jusqu'a
            //ce que l'utilisateur appuie sur une touche du clavier
            // auquel cas on met a true un des booléens du tableau, cf ligne 55
            for (var i = 0; i < this.afficherPhrase.length; i++) {
                if (this.afficherPhrase[i]) {
                    this.rienAfficher = false;
                }
            }
            //si aucune des phrases ne peut etre affichée, on ne met rien
            if (this.rienAfficher) {
                if (this.ticks <= 15) {
                    this.description = "";
                    //et si aucune phrase n'a ete affichee, ca veut dire qu'on est entre
                    //le 1 et la première phrase de l'histoire, donc
                    //il faut afficher le message sonore "bienvenue...diese"
                    this.sonMoment = this.sons[0];
                } else {
                    //Si l'utilisateur met trop de temps a "appuyer sur la touche diese"
                    //on lui dit qu'en fait il peut appuyer ou il veut sur le clavier
                    //sans oublier de remettre a une taille normale le texte
                    //le 1 avait agrandi la taille du texte
                    this.fontsize = 25;
                    // top = 20 est seulement pour la notification de appuyer sur la touche, pour les restes, top = 40
                    this.top = 20;
                    this.description = this.aideUtilisateur;
                }

            }
        }
    }

    //Quelques secondes apres l'affichage du 1 (le temps que la voix termine
    //sa phrase "Entrez la touche diese"), on autorise l'affichage de la premiere
    //phrase de l'histoire du personnage
    @HostListener('window:keydown', ['$event'])
    keyboardInput(event: KeyboardEvent) {
        //S'il n'y a rien et qu'on appuie sur une touche du clavier, on lance la
        //phrase 0 "Toute ma vie... infini"
        if (this.ticks >= 5 && this.rienAfficher) {
            this.afficherPhrase[0] = true;
            this.description = this.Phrase[0];
            //on enregstre le nombre de secondes ecoulees pour etre sur qu'il s'est ecoule
            //un minimum de temps avant l'affichage de la phrase suivante, meme si la souris
            //de l'utilisateur etait au milieu de l'ecran au moment ou il a appuye sur une touche du clavier
            this.ticksPhrase = this.ticks;
            //on remet le texte a un affichage normal et centre sur l'ecran
            //(le changement sur le 1 a deregle les positions)
            this.fontsize = 25;
            this.top = 40;
            this.rienAfficher = false;
            //il est également temps de lire la piste audio disant "bravo"
            this.sonMoment = this.sons[1];
        }


        //Si on appuie sur une touche et que toutes les phrases du tableau ont ete
        //affichees, on autorise l'affichage de "le rendez vous est arrivé"
        //à partir de l'affichage a l'écran de "la suite" et après
        if (this.j >= this.Phrase.length - 1) {
            //this.fontsize=25; //on conserve la taille de phraseFin a la fin
            this.description = this.phraseFin;
            //on fait alors reapparaitre la souris pour lui dire implicitemenet que c'est a lui de cliquer
            this.curseur = "pointer";
            //c'est le moment de dire "bravo votre rendez vous est arrive"
            this.sonMoment = this.sons[3];
            this.Fini = true; //evite un reaffichage de l'heure apres la derniere phrase
        }
    }

    //pas de shuffle vers la phrase suivante s'il ne s'est pas ecoule au moins deux secondes
    //depuis la phrase precedente
    changementInterdit(i: number): boolean {
        //on met en place le changement un peu plus long sur les shuffles de la scene 1 pour etre sur qu'on affiche bien la premiere phrase
        if (this.ticks - this.ticksPhrase < 1) {
            //console.log("CHANGEMENT INTERDIT");
            return true;
        } else {
            return false;
        }
    }

    //Changement de texte au passage de la souris sur celui-ci
    phrasesuivante() {
        console.log(this._translate.instant("Toute ma vie, j'ai cru avoir devant moi un champ des possibles infini."));
        //le shuffle ne doit marcher que si une des phrases du tableau est affichee
        //(on ne veut pas que le hover marche sur l'affichage du 1 ou sur le "avez-vous pensé a allumer vos enceintes")
        //donc on verifie qu'une des phrases du tableau est affichee a l'aide du boolean
        //rienAfficher (qui vaut true seulement si aucune phrase du tableau Phrase n'est affichée)
        //On ne lance pas le shuffle si la phrase affichee est la phrase de fin "et le rdv est arrive"
        //ni si la phrase est "je suis maitre de mon destin" (celle ou on doit cliquer dessus)
        var el = document.getElementById("chap1");
        var shuffle = new ShuffleText(el);

        //lancement du shuffle sur le passage de la souris sur le texte,
        //seulement si rienAfficher est a false (va veut dire qu'une phrase du tableau Phrase est affichee)
        //ou si j est différent de 3 (parce que si j=3, ça veut dire que la phrase affichee
        //est "je suis maitre de mon destin" et dans ce cas l'utilisateur doit cliquer sur la phrase
        //pour passer à la suite
        if (!this.rienAfficher && this.description != this.phraseFin && this.j != 3 && this.j != 4 && this.j != 9 && !this.changementInterdit(this.j)) {
            shuffle.start();
        } else if (this.j == 3) {
            //Si j=3, la phrase affichée est 'je suis maitre de mon destin'.
            //On doit indiquer a l"utilisateur qu'il doit cliquer pour continuer
            //en changeant le pointeur de la souris
            this.curseur = "pointer";
            //this.visibilityState="shown";  //Test un peu nul d'apparition de "je suis maitre de mon destin" en fondu
        }
        else if (this.j == 4) {
            this.creationLockedInterval = setInterval(() => {
                this.allowedToCreate = true;
            }, 100);
        }
        else if (this.j == 9 && !this.intervalDejaCree) {
            //lancement des animations dès la phrase suivant "je suis maitre de mon destin"
            this.explosionInterval = setInterval(() => {
                this.explosion();
            }, 1500);
            this.intervalDejaCree = true;
        }




        //Il faut d'abord que la phrase "Toute ma vie..." soit affichée avant
        //d'autoriser le changement de phrase au passage de la souris dessus
        if (this.afficherPhrase[this.j] && this.j != 3 && !this.changementInterdit(this.j)) {
            if ((this.j) + 1 < this.Phrase.length) {

                this.afficherPhrase[(this.j) + 1] = true;
                this.description = this.Phrase[(this.j) + 1];


                //Etant donne qu'on a change de texte, il faut prevenir le Shuffle
                //qu'on met a jour le texte a melanger. Pour cela, on arrete le Shuffle
                //en cours et on en relance un nouveau avec le nouveau contenu de la balise html
                //shuffle.stop();

                el.innerHTML = this.description;

                shuffle = new ShuffleText(el)
                shuffle.start();

                //Tentative ratee de faire un shuffle a la main
                //on la laisse ici si jamais on veut s'en inspirer un jour
                /*  for(this.effetMelangeCompteur =0; this.effetMelangeCompteur<this.Phrase[(this.j)+1].length; this.effetMelangeCompteur++){
  
  
                    if(this.description[this.effetMelangeCompteur]!=this.Phrase[(this.j)+1][this.effetMelangeCompteur]){
                      //si le compteur tombe sur un espace dans la phrase suivante, on ne fait rien
                      if(this.Phrase[(this.j)+1][this.effetMelangeCompteur]==" "){
                        this.leftSide = this.description.slice(0,this.effetMelangeCompteur);
                        this.rightSide = this.description.slice(this.effetMelangeCompteur+1, this.description.length);
                        this.description = this.leftSide.concat(" ".concat(this.rightSide));
                        //this.MettreAJourDescription(this.description);
                        //setTimeout(this.description = this.leftSide.concat(" ".concat(this.rightSide)), 300);
                        //console.log(this.description);
                      }
                      else{
                        //Si le caractère de la phrase suivante n'est pas un espace, on affiche jusqu'à 5 caractères au hasard avant d'afficher le caractère de la phrase suivante
                        this.ttl = 5;
                        while(this.description[this.effetMelangeCompteur]!=this.Phrase[(this.j)+1][this.effetMelangeCompteur]&&this.ttl>0){
                          this.charAuHasard = Math.floor(Math.random() * 10);
                          this.leftSide = this.description.slice(0,this.effetMelangeCompteur);
                          this.rightSide = this.description.slice(this.effetMelangeCompteur+1, this.description.length);
                          this.description = this.leftSide.concat(this.tabChar[this.charAuHasard].concat(this.rightSide));
                          this.ttl--;
                          //this.MettreAJourDescription(this.description);
                          //console.log(this.description);
                          //setTimeout(this.description = this.leftSide.concat(this.tabChar[this.charAuHasard].concat(this.rightSide)),300)
                        }
                        this.leftSide = this.description.slice(0,this.effetMelangeCompteur);
                        this.rightSide = this.description.slice(this.effetMelangeCompteur+1, this.description.length);
                        this.description = this.leftSide.concat(this.Phrase[(this.j)+1][this.effetMelangeCompteur].concat(this.rightSide));
                        //this.MettreAJourDescription(this.description);
                        //console.log(this.description);
                        //setTimeout(this.description = this.leftSide.concat(this.Phrase[(this.j)+1][this.effetMelangeCompteur].concat(this.rightSide)),300)
                      }
                    }
                    this.description = this.description.slice(0, this.Phrase[(this.j)+1].length);
                  }*/
            }

            //La phrase j n'étant plus affichée, on met son booleen correspondant a false pour
            //eviter de le reafficher lors du hover suivant
            this.afficherPhrase[this.j] = false;
            this.j = (this.j) + 1;

            //sauvegarde du nombre de secondes a l'affichage de la phrase
            //pour savoir quand on va autoriser l'affichage de la phrase suivante
            this.ticksPhrase = this.ticks;

            //console.log("changement j :"+this.j)
            //au moment où j atteint la taille du tableau, on sonne le message
            //"si vous voulez que votre rdv..."
            if (this.j + 1 == this.Phrase.length) {
                this.sonMoment = this.sons[2];
            }
        }

        if (this.j >= 11 && (this.j < this.Phrase.length)) {
            this.curseur = "none";
        }

        //Si toutes les phrases du tableau ont ete affichees,
        //mais que la phrase de fin n'a pas encore ete affichee, on affiche l'heure
        if (this.j == this.afficherPhrase.length && !this.Fini) {
            this.description = new Date().toTimeString().slice(0, 8);

            //shuffle.stop();
            el.innerHTML = this.description;
            shuffle = new ShuffleText(el)
            shuffle.start();
            //si l'utilisateur met trop de temps a comprendre qu'il doit appuyer
            //sur le clavier, on affiche un message d'aide
            if (this.ticks >= 55) {
                this.description = this.description + " " + this.aideUtilisateur;
            }
        }

    }

    @HostListener('document:click', ['$event'])
    documentClick(event: Event): void {

        //S'il n'y a rien et qu'on clique, on lance la
        //phrase 0 (même principe que pour la touche du clavier)
        if (this.ticks >= 5 && this.rienAfficher) {
            this.afficherPhrase[0] = true;
            this.description = this.Phrase[0];
            //on enregstre le nombre de secondes ecoulees pour etre sur qu'il s'est ecoule
            //un minimum de temps avant l'affichage de la phrase suivante, meme si la souris
            //de l'utilisateur etait au milieu de l'ecran au moment ou il a appuye sur une touche du clavier
            this.ticksPhrase = this.ticks;
            //on remet le texte a un affichage normal et centre sur l'ecran
            //(le changement sur le 1 a deregle les positions)
            this.fontsize = 25;
            this.top = 40;
            this.rienAfficher = false;
            //il est également temps d'afficher le "bravo"
            this.sonMoment = this.sons[1];
        }


        //si on est a la phrase "Je suis maitre de mon destin" (3ème phrase du tableau),
        //il faut cliquer pour passer a la phrase suivante
        if (this.j == 3) {
            if ((this.j) + 1 < this.Phrase.length) {
                //console.log("PASSAGE PHRASE APRES CLIC");
                this.description = this.Phrase[(this.j) + 1];
                this.afficherPhrase[(this.j) + 1] = true;

                //on n'oublie pas de faire le shuffle vers la phrase suivante
                var el = document.getElementById("chap1");
                el.innerHTML = this.description;
                var shuffle = new ShuffleText(el)
                shuffle.start();
            }
            this.afficherPhrase[this.j] = false;
            this.j = (this.j) + 1;
        }
    }



}



//Vieux souvenirs de la tentative de shuffle "a la main" avant d'installer shuffle-text.js
/*  function delay(ms: number) {
      return new Promise(resolve => setTimeout(resolve, ms));
  } */

/*
// Crée un cadre pour les rendus et définis la taille et la couleur du fond
var renderer = PIXI.autoDetectRenderer(2000, 200,{
  backgroundColor: 0x000000,
 // backgroundColor: 0x060606
});

// Attache le cadre à body

document.body.appendChild(renderer.view);


// Crée un cointainer ou se déroulera la scène
var stage = new PIXI.Container();

// add stage to the canvas
render();

function render(){
    renderer.render(stage);

}

*/
