import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//import { AnimationTargetComponent } from './animation-target/animation-target.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { CreditComponent } from './credit/credit.component';
import { TelechargementComponent } from './telechargement/telechargement.component';
import { PrixComponent } from './prix/prix.component';
import { Chapitre1Component } from './chapitre1/chapitre1.component';
import { Chapitre2Component } from './chapitre2/chapitre2.component';
import { Chapitre3Component } from './chapitre3/chapitre3.component';
import { Chapitre4Component } from './chapitre4/chapitre4.component';
import { Chapitre5Component } from './chapitre5/chapitre5.component';
import { Chapitre6Component } from './chapitre6/chapitre6.component';

import { TRANSLATION_PROVIDERS, TranslatePipe, TranslateService }   from './translate';
import { routes } from './app.router';
import { HomeComponent } from './home/home.component';

import {ChangeDirective} from './directive';
import { MobileComponent } from './mobile/mobile.component'
import { CommonModule } from '@angular/common';
@NgModule({
  declarations: [
    AppComponent,
    CreditComponent,
    TelechargementComponent,
    PrixComponent,
    Chapitre1Component,
    Chapitre2Component,
    Chapitre3Component,
    Chapitre4Component,
    Chapitre5Component,
    Chapitre6Component,
    //AnimationTargetComponent,
    HomeComponent,
    ChangeDirective,
    TranslatePipe,
    MobileComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    routes,
    BrowserAnimationsModule,
    FormsModule
  ],
  providers: [ TRANSLATION_PROVIDERS, TranslateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
