import { Component, OnInit } from '@angular/core';
import { TranslateService } from '../translate';

@Component({
    selector: 'app-credit',
    templateUrl: './credit.component.html',
    styleUrls: ['./credit.component.css']
})
export class CreditComponent implements OnInit {

    constructor(private _translate: TranslateService) {
        this.lang = _translate.currentLang;
    }
    lang;
    ngOnInit() {
        console.log(this._translate.instant("hello world"));
    }

}
