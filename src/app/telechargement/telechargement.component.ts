import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-telechargement',
  templateUrl: './telechargement.component.html',
  styleUrls: ['./telechargement.component.css']
})
export class TelechargementComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  TelechargementFr = 'http://www.utc.fr/~bouchard/works/presentation-deprise.pdf';
  ImageFr = 'assets/img/fr-flag.svg';
  TelechargementAn = 'http://www.utc.fr/~bouchard/works/presentation_Loss-of-Grasp.pdf';
  ImageAn = 'assets/img/en-flag.svg';

}
